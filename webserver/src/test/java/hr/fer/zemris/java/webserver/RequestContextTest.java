package hr.fer.zemris.java.webserver;

import static org.junit.Assert.assertEquals;
import hr.fer.zemris.java.webserver.RequestContext.RCCookie;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

public class RequestContextTest {

    private RequestContext getRequestContext() throws IOException {
        final OutputStream os = Files.newOutputStream(Paths.get("test.txt"));
        final RequestContext rc =
                new RequestContext(os, new HashMap<String, String>(),
                        new HashMap<String, String>(), new ArrayList<RequestContext.RCCookie>());
        rc.setEncoding("UTF-8");
        rc.setMimeType("text/plain");
        rc.setStatusCode(205);
        rc.setStatusText("Idemo dalje");
        rc.addRCCookie(new RCCookie("korisnik", "perica", 3600, null, "/", false));
        rc.addRCCookie(new RCCookie("zgrada", "B4", null, null, "/", false));
        return rc;
    }

    @Test
    public void RCCookie_Test() {
        final RCCookie cookie = new RCCookie("zgrada", "B4", 3600, "127.0.0.1", "/", false);
        assertEquals("zgrada", cookie.getName());
        assertEquals("B4", cookie.getValue());
        assertEquals(new Integer(3600), cookie.getMaxAge());
        assertEquals("127.0.0.1", cookie.getDomain());
        assertEquals("/", cookie.getPath());
    }

    @Test(expected = IllegalArgumentException.class)
    public void constuctor_IllegalOutputStream_IllegalArgumentException() {
        new RequestContext(null, null, null, null);
    }

    @Test(expected = RuntimeException.class)
    public void setMimeType_calledAfterWriteMethod_RuntimeException() throws IOException {
        final RequestContext rc = getRequestContext();
        rc.write("");
        rc.write("test");
        rc.setMimeType("text/html");
    }

    @Test
    public void temporaryParameter_ValidInput_Test() throws IOException {
        final RequestContext rc = getRequestContext();
        rc.setTemporaryParameter("aa", "bb");
        rc.setTemporaryParameter("ab", "bb");
        rc.setTemporaryParameter("cc", "bb");
        assertEquals(rc.getTemporaryParameter("cc"), "bb");
        rc.removeTemporaryParameter("cc");
        final String[] keys = new String[2];
        rc.getTemporaryParameterNames().toArray(keys);
        assertEquals(keys[0], "aa");
        assertEquals(keys[1], "ab");
    }

    @Test
    public void persistentParameter_ValidInput_Test() throws IOException {
        final OutputStream os = Files.newOutputStream(Paths.get("aa"));
        final RequestContext rc = new RequestContext(os, null, null, null);
        rc.setPersistentParameter("aa", "bb");
        rc.setPersistentParameter("ab", "bb");
        rc.setPersistentParameter("cc", "bb");
        assertEquals(rc.getPersistentParameter("cc"), "bb");
        rc.removePersistentParameter("cc");
        final String[] keys = new String[2];
        rc.getPersistentParameterNames().toArray(keys);
        assertEquals(keys[0], "aa");
        assertEquals(keys[1], "ab");
    }

    @Test
    public void write_ValidInput_Test() throws IOException {
        final OutputStream os = Files.newOutputStream(Paths.get("aa"));
        final Map<String, String> params = new HashMap<>();
        params.put("test", "test");
        final RequestContext rc = new RequestContext(os, params, null, null);
        final String[] keys = new String[2];
        rc.getParameterNames().toArray(keys);
        rc.addRCCookie(new RCCookie("korisnik", "perica", 3600, null, "/", false));
        rc.addRCCookie(new RCCookie("zgrada", "B4", 500, "127.0.0.1", "/", true));
        assertEquals("test", keys[0]);
        assertEquals("test", rc.getParameter("test"));
        assertEquals(rc, rc.write("test"));
    }
}

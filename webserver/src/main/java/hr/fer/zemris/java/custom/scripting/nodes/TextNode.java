package hr.fer.zemris.java.custom.scripting.nodes;

/**
 * A node representing a piece of textual data.
 *
 * @author Stipan Mikulić
 * @version 1.0
 */
public class TextNode extends Node {
    /**
     * String where data is stored.
     */
    private final String text;

    /**
     * Constructs an TextNode with given data.
     *
     * @param text data to store
     */
    public TextNode(final String text) {
        super();
        this.text = text;
    }

    /**
     * Returns TextNode data.
     *
     * @return TextNode data
     */
    public String getText() {
        return text;
    }

    @Override
    public void accept(final INodeVisitor visitor) {
        visitor.visitTextNode(this);
    }
}

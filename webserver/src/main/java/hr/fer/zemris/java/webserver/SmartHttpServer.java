package hr.fer.zemris.java.webserver;

import hr.fer.zemris.java.custom.scripting.exec.SmartScriptEngine;
import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParser;
import hr.fer.zemris.java.webserver.RequestContext.RCCookie;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PushbackInputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * <p>
 * This class represents {@link SmartHttpServer}.
 * </p>
 * This {@link SmartHttpServer} can execute given scripts by {@link SmartScriptParser}, display
 * circles filled with random color, display HTML pages with various content.
 * <p>
 * This server could be run from command line. It accepts one command line argument that needs to be
 * path to server properties file.
 * </p>
 *
 *
 * @author Stipan Mikulić
 * @version 1.0
 * @see SmartScriptParser
 * @see RequestContext
 */
public class SmartHttpServer {
    /**
     * Size of buffer for reading files.
     */
    private static final int BUFFER_FOR_READING_FILES = 4096;
    /**
     * Server socket timeout.
     */
    private static final int SERVER_SOCKET_TIMEOUT = 1000;
    /**
     * Five minutes in milliseconds that represents period for cleaning thread.
     */
    private static final long FIVE_MINUTES = 300000;
    /**
     * Milliseconds in one second.
     */
    private static final int MILISECONDS = 1000;
    /**
     * Address on which server listens.
     */
    private final String address;
    /**
     * Port on which server listens.
     */
    private final int port;
    /**
     * Number of worker threads.
     */
    private final int workerThreads;
    /**
     * Server session timeout.
     */
    private final int sessionTimeout;
    /**
     * Map where mime types are stored.
     */
    private final Map<String, String> mimeTypes = new HashMap<>();
    /**
     * Server thread.
     */
    private ServerThread serverThread;
    /**
     * Thread pool for server threads.
     */
    private ExecutorService threadPool;
    /**
     * {@link Path} to root directory from which we serve files.
     */
    private final Path documentRoot;
    /**
     * Flag that determines is server running.
     */
    private boolean serverRuning;
    /**
     * Map where {@link IWebWorker} are stored.
     */
    private final Map<String, IWebWorker> workersMap = new HashMap<>();
    /**
     * Map where server sessions are stored.
     */
    private volatile Map<String, SessionMapEntry> sessions = new HashMap<>();
    /**
     * Random number generator.
     */
    private final Random sessionRandom = new Random();
    /**
     * Timer for cleaning server sessions.
     */
    private final Timer timer;

    /**
     * Creates new instance of {@link SmartHttpServer}.
     *
     * @param configFileName
     *            file where server properties are stored
     */
    public SmartHttpServer(final String configFileName) {
        final Properties serverProperties = new Properties();
        try {
            final FileInputStream in = new FileInputStream(configFileName);
            serverProperties.load(in);
            in.close();
        } catch (final Exception e) {
            System.err.println("Error while reading server properties.");
            System.exit(1);
        }
        address = serverProperties.getProperty("server.address");
        port = Integer.parseInt(serverProperties.getProperty("server.port"));
        workerThreads = Integer.parseInt(serverProperties.getProperty("server.workerThreads"));
        documentRoot = Paths.get(serverProperties.getProperty("server.documentRoot"));
        try {
            getMimeTypes(serverProperties.getProperty("server.mimeConfig"));
            getWorkers(serverProperties.getProperty("server.workers"));
        } catch (final Exception e) {
            System.err.println("Error while reading properties file!");
            System.exit(1);
        }
        sessionTimeout = Integer.parseInt(serverProperties.getProperty("session.timeout"));
        serverRuning = false;
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                final List<String> keys = new ArrayList<String>(sessions.keySet());
                final int size = keys.size();
                for (int i = 0; i < size; i++) {
                    if (System.currentTimeMillis() > sessions.get(keys.get(i)).validUntil) {
                        sessions.remove(keys.get(i));
                    }
                }
            }
        }, 0, FIVE_MINUTES);
    }

    /**
     * This method reads worker properties from file that is given as argument.
     *
     * @param workersPropertiesFile
     *            path to worker properties file
     * @throws Exception
     *             if error while reading properties occurred
     */
    private void getWorkers(final String workersPropertiesFile) throws Exception {
        final Properties workersProperties = new Properties();
        final FileInputStream in = new FileInputStream(workersPropertiesFile);
        workersProperties.load(in);
        in.close();
        for (final String key : workersProperties.stringPropertyNames()) {
            final String fqcn = workersProperties.getProperty(key);
            Class<?> referenceToClass;
            Object newObject = null;
            referenceToClass = this.getClass().getClassLoader().loadClass(fqcn);
            newObject = referenceToClass.newInstance();
            workersMap.put(key, (IWebWorker) newObject);
        }
    }

    /**
     * This method reads mime type properties from file that is given as argument.
     *
     * @param mimeTypesFile
     *            path to mime type properties file
     * @throws Exception
     *             if error while reading properties occurred
     */
    private void getMimeTypes(final String mimeTypesFile) throws Exception {
        final Properties mimeTypeProperties = new Properties();
        try {
            final FileInputStream in = new FileInputStream(mimeTypesFile);
            mimeTypeProperties.load(in);
            in.close();
        } catch (final Exception e) {
            System.err.println("Error while reading server properties.");
            System.exit(1);
        }
        for (final String key : mimeTypeProperties.stringPropertyNames()) {
            mimeTypes.put(key, mimeTypeProperties.getProperty(key));
        }
    }

    /**
     * This method starts {@link SmartHttpServer}.
     */
    protected synchronized void start() {
        serverThread = new ServerThread();
        threadPool = Executors.newFixedThreadPool(workerThreads);
        serverThread.start();
        serverRuning = true;
        System.out.println("Server started!");
    }

    /**
     * This method stops {@link SmartHttpServer}.
     */
    protected synchronized void stop() {
        if (threadPool != null && serverRuning) {
            serverRuning = false;
            timer.cancel();
            serverThread.interrupt();
            threadPool.shutdown();
            System.out.println("Server stopped!");
        }
    }

    /**
     * This method represents entry for each session in {@link SmartHttpServer}.
     *
     * @author Stipan Mikulić
     * @version 1.0
     */
    private static class SessionMapEntry {
        /**
         * Session identifier.
         */
        private String sid;
        /**
         * Millisecond until session is valid.
         */
        private long validUntil;
        /**
         * Persistent parameters for each session.
         */
        private Map<String, String> map;
    }

    /**
     * This class represents server thread.
     *
     * @author Stipan Mikulić
     * @version 1.0
     */
    protected class ServerThread extends Thread {
        /**
         * Server socket.
         */
        private ServerSocket serverSocket = null;

        @Override
        public void interrupt() {
            super.interrupt();
            try {
                serverSocket.close();
            } catch (final IOException ignorable) {
            }
        }

        @Override
        public void run() {
            try {
                serverSocket = new ServerSocket();
                serverSocket.setSoTimeout(SERVER_SOCKET_TIMEOUT);
                serverSocket.bind(new InetSocketAddress(InetAddress.getByName(address), port));
            } catch (final IOException e) {
                System.err.println("Error while creating server socket.");
                System.exit(1);
            }
            while (serverRuning) {
                Socket client = null;
                try {
                    client = serverSocket.accept();
                } catch (final Exception e) {
                    // timeout has occurred
                    continue;
                }
                final ClientWorker cw = new ClientWorker(client);
                threadPool.execute(cw);
            }
        }
    }

    /**
     * <p>
     * This class represents job for each {@link ServerThread}.
     * </p>
     *
     * @author Stipan Mikulić
     * @version 1.0
     */
    private class ClientWorker implements Runnable {
        /**
         * Length of SID.
         */
        private static final int SID_LENGTH = 20;
        /**
         * Number of letters in alphabet.
         */
        private static final int NUMBER_OF_LETTERS = 26;
        /**
         * ASCII code for letter A.
         */
        private static final int NUMBER_OF_FIRST_LETTER = 65;
        /**
         * Size of pushback buffer.
         */
        private static final int PUSHBACK_BUFFER_SIZE = 4;
        /**
         * Client socket.
         */
        private final Socket csocket;
        /**
         * Input stream that provides method for push back read bytes.
         */
        private PushbackInputStream istream;
        /**
         * Output stream for client.
         */
        private OutputStream ostream;
        /**
         * Version of protocol.
         */
        private String version;
        /**
         * Request method for HTTP protocol.
         */
        private String method;
        /**
         * Map where URL parameters are stored.
         */
        private final Map<String, String> params = new HashMap<>();
        /**
         * Persistent parameters for each client.
         */
        private Map<String, String> permPrams = new HashMap<>();
        /**
         * List where cookies are stored.
         */
        private final List<RCCookie> outputCookies = new ArrayList<>();
        /**
         * Session identifier.
         */
        private String sid;

        /**
         * Creates new instance of {@link ClientWorker} class.
         *
         * @param csocket
         *            socket for this client
         */
        public ClientWorker(final Socket csocket) {
            super();
            this.csocket = csocket;
        }

        @Override
        public void run() {
            try {
                istream = new PushbackInputStream(csocket.getInputStream(), PUSHBACK_BUFFER_SIZE);
                ostream = csocket.getOutputStream();
            } catch (final IOException e) {
                System.err.println("Error while getting streams!");
                return;
            }

            List<String> request = null;
            try {
                request = readRequest();
            } catch (final IOException e) {
                System.err.println("Error while reading request!");
                return;
            }
            checkSession(request);

            final RequestContext rc = new RequestContext(ostream, params, permPrams, outputCookies);
            if (request.size() < 1) {
                sendError(rc, 400, "Invalid header");
                return;
            }

            final String[] firstLine = request.isEmpty() ? null : request.get(0).split(" ");
            if (firstLine == null || firstLine.length != 3) {
                sendError(rc, 400, "Bad request");
                return;
            }
            method = firstLine[0].toUpperCase();
            if (!method.equals("GET")) {
                sendError(rc, 405, "Method Not Allowed");
                return;
            }

            version = firstLine[2].toUpperCase().trim();
            if (!version.equals("HTTP/1.1") && !version.equals("HTTP/1.0")) {
                sendError(rc, 505, "HTTP Version Not Supported");
                return;
            }
            // Extract path and parameters from requested path
            final int questionMarkIndex = firstLine[1].indexOf("?");
            String path = firstLine[1];
            String paramString = null;
            if (questionMarkIndex != -1) {
                path = firstLine[1].substring(0, questionMarkIndex);
                paramString = firstLine[1].substring(questionMarkIndex + 1);
                parseParameters(paramString);
            }
            final Path requestedPath = documentRoot.resolve(path.substring(1)).normalize();

            // if requestedPath is not below documentRoot, status 403 forbidden
            if (!requestedPath.startsWith(documentRoot)) {
                sendError(rc, 403, "Forbidden");
                return;
            }
            // if convention based web worker is requested, process it
            if (path.startsWith("/ext/")) {
                processConvBased(path, rc);
                return;
            }
            // if web worker is requested then process it
            final IWebWorker worker = workersMap.get(path);
            if (worker != null) {
                worker.processRequest(rc);
                try {
                    ostream.flush();
                    ostream.close();
                } catch (final IOException e) {
                    System.err.println("Error while closing output stream!");
                    return;
                }
                return;
            }

            // get file extension from requested file
            final File requestedFile = requestedPath.toFile();
            String fileExtension = null;
            if (!requestedFile.exists() || !requestedFile.isFile() || !requestedFile.canRead()) {
                sendError(rc, 403, "Error with file: " + requestedFile.toString());
                return;
            } else {
                fileExtension = getExtension(requestedFile);
            }
            // if file is executable script, process it
            if (fileExtension.toLowerCase().equals("smscr")) {
                if (!processScript(requestedPath, rc)) {
                    return;
                }
            } else { // process basic request
                if (!processBasicRequest(requestedFile, rc)) {
                    return;
                }
            }
            // flush and close output stream
            try {
                ostream.flush();
                ostream.close();
                csocket.close();
            } catch (final IOException e) {
                System.err.println("Error while writing to request context output stream!");
                return;
            }
        }

        /**
         * This method checks is current client already send queries to this {@link SmartHttpServer}
         * .
         *
         * @param headers
         *            {@link List} that contains header lines
         */
        private synchronized void checkSession(final List<String> headers) {
            String sidCandidate = null;
            for (final String header : headers) {
                if (!header.startsWith("Cookie:")) {
                    continue;
                } else {
                    final String cookiesString = header.substring("Cookie:".length()).trim();
                    final String[] cookies = cookiesString.split(";");
                    for (final String cookie : cookies) {
                        // get cookie name
                        final String cookieName =
                                cookie.trim().substring(0, cookie.indexOf("=")).trim();
                        // get cookie value without quotes
                        final String cookieValue =
                                cookie.trim().substring(cookie.indexOf("=") + 2,
                                        cookie.length() - 1);
                        if (cookieName.equals("sid")) {
                            sidCandidate = cookieValue;
                            break;
                        }
                    }
                }
            }
            SessionMapEntry session;
            if (sidCandidate == null) {
                session = makeSessionEntry();
            } else {
                session = sessions.get(sidCandidate);
                if (session == null) {
                    session = makeSessionEntry();
                } else if (session.validUntil < System.currentTimeMillis()) {
                    session = makeSessionEntry();
                    sessions.remove(sidCandidate);
                } else {
                    session.validUntil = System.currentTimeMillis() + MILISECONDS * sessionTimeout;
                }
            }
            sid = session.sid;
            permPrams = session.map;
        }

        /**
         * This method creates new {@link SessionMapEntry} with new generated SID.
         *
         * @return created {@link SessionMapEntry}
         */
        private synchronized SessionMapEntry makeSessionEntry() {
            final SessionMapEntry session = new SessionMapEntry();
            sid = generateSID();
            session.sid = sid;
            session.map = new ConcurrentHashMap<String, String>();
            session.validUntil = System.currentTimeMillis() + MILISECONDS * sessionTimeout;
            sessions.put(session.sid, session);
            outputCookies.add(new RCCookie("sid", session.sid, null, "127.0.0.1", "/", true));
            return session;
        }

        /**
         * This method generates random SID that contains 20 upper case letters.
         *
         * @return generated SID
         */
        private synchronized String generateSID() {
            final StringBuilder newSID = new StringBuilder();
            for (int i = 0; i < SID_LENGTH; i++) {
                final int intOfLetter =
                        Math.abs(sessionRandom.nextInt()) % NUMBER_OF_LETTERS
                                + NUMBER_OF_FIRST_LETTER;
                newSID.append(Character.toChars(intOfLetter));
            }
            return newSID.toString();
        }

        /**
         * This method processes basic request for this server.
         *
         * @param requestedFile
         *            file that is requested
         * @param rc
         *            {@link RequestContext} object
         * @return <code>true</code> if no exception has occurred, <code>false</code> otherwise
         */
        private boolean processBasicRequest(final File requestedFile, final RequestContext rc) {
            String mimeType = mimeTypes.get(getExtension(requestedFile));
            if (mimeType == null) {
                mimeType = "application/octet-stream";
            }
            rc.setMimeType(mimeType);

            FileInputStream cis = null;
            try {
                cis = new FileInputStream(requestedFile);
            } catch (final FileNotFoundException e1) {
                System.err.println("File (" + requestedFile.toString() + ") not found!");
                return false;
            }

            final byte[] bytes = new byte[BUFFER_FOR_READING_FILES];
            int readBytes = 0;
            try {
                while ((readBytes = cis.read(bytes)) > 0) {
                    rc.write(Arrays.copyOfRange(bytes, 0, readBytes));
                }
                cis.close();
            } catch (final IOException e) {
                System.err.println("Error while reading file: " + requestedFile.toString());
                return false;
            }
            return true;
        }

        /**
         * This method gets extension for given file.
         *
         * @param requestedFile
         *            file from which extension is get
         * @return searched extension
         */
        private String getExtension(final File requestedFile) {
            final String reqFileString = requestedFile.toString();
            return reqFileString.substring(reqFileString.lastIndexOf(".") + 1);
        }

        /**
         * This method processes scripts with extension "smscr".
         *
         * @param requestedPath
         *            path to required script
         * @param rc
         *            {@link RequestContext} object
         * @return <code>true</code> if no exception has occurred, <code>false</code> otherwise
         */
        private boolean processScript(final Path requestedPath, final RequestContext rc) {
            String documentBody = null;
            try {
                documentBody =
                        new String(Files.readAllBytes(requestedPath), StandardCharsets.UTF_8);
            } catch (final IOException e) {
                System.err.println("Error while reading file!");
                return false;
            }
            new SmartScriptEngine(new SmartScriptParser(documentBody).getDocumentNode(), rc)
                    .execute();
            try {
                csocket.close();
            } catch (final IOException e) {
                System.err.println("Error while closing socket!");
                return false;
            }
            return true;
        }

        /**
         * This method processes worker request that are required by convention-over-configuration
         * approach to {@link IWebWorker} classes.
         *
         * @param path
         *            path that represents {@link IWebWorker} class
         * @param rc
         *            {@link RequestContext} object
         */
        private void processConvBased(final String path, final RequestContext rc) {
            String fqcn = "hr.fer.zemris.java.webserver.workers.";
            fqcn += path.substring("/ext/".length());
            Class<?> referenceToClass;
            try {
                referenceToClass = this.getClass().getClassLoader().loadClass(fqcn);
                final Object newObject = referenceToClass.newInstance();
                final IWebWorker iww = (IWebWorker) newObject;
                iww.processRequest(rc);
                ostream.flush();
                ostream.close();
            } catch (final Exception e) {
                System.err.println("Error while processing worker: "
                        + path.substring("/ext/".length()));
                return;
            }
        }

        /**
         * This method displays error message to client.
         *
         * @param rc
         *            {@link RequestContext} object
         * @param statusCode
         *            code status for occurred error
         * @param statusText
         *            text status for occurred error
         */
        private void sendError(final RequestContext rc, final int statusCode,
                final String statusText) {
            try {
                // @formatter:off
                rc.write(("<html>"
                        + "<head>" + "<title>Error</title>" + "</head>"
                        + "<body><h1>" + statusCode + ".   " + statusText + "</h1></b></body>"
                        + "<html>")
                        .getBytes(StandardCharsets.US_ASCII));
                // @formatter:on
                ostream.flush();
                ostream.close();
            } catch (final IOException e) {
                System.err.println("Error while writing to request context output stream!");
                return;
            }
        }

        /**
         * This method parses parameters that are given in URL.
         *
         * @param paramString
         *            uncut parameters from URL
         */
        private void parseParameters(final String paramString) {
            final String[] parameters = paramString.split("&");
            for (final String param : parameters) {
                final String[] keyAndValue = param.split("=");
                params.put(keyAndValue[0], keyAndValue[1]);
            }
        }

        /**
         * This method cuts header lines from given request.
         *
         * @return {@link List} that contains header lines
         * @throws IOException
         *             if error while reading request occurred
         */
        private List<String> readRequest() throws IOException {
            final int carrigeReturn = 13;
            final int lineFeed = 10;
            final int maskForBytes = 0xFF;
            final List<String> lines = new ArrayList<>();
            StringBuilder line = new StringBuilder();
            while (true) {
                final int b1 = istream.read();
                line.append((char) (b1 & maskForBytes));
                if (b1 == carrigeReturn) {
                    final int b2 = istream.read();
                    line.append((char) (b2 & maskForBytes));
                    if (b2 == lineFeed) {
                        lines.add(line.toString());
                        line = new StringBuilder();
                        final int b3 = istream.read();
                        final int b4 = istream.read();
                        if (b3 == carrigeReturn && b4 == lineFeed) {
                            line.append((char) (b3 & maskForBytes)).append(
                                    (char) (b4 & maskForBytes));
                            lines.add(line.toString());
                            return lines;
                        } else {
                            istream.unread(b4);
                            istream.unread(b3);
                        }
                    }
                }
            }
        }
    }

    /**
     * This method is called when program starts.
     *
     * @param args
     *            command line arguments
     */
    public static void main(final String[] args) {
        if (args.length != 1) {
            System.err.println("Server properties file has been expected!");
            return;
        }
        final SmartHttpServer httpServer = new SmartHttpServer(args[0]);
        final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        boolean serverStarted = false;
        while (true) {
            if (!serverStarted) {
                System.out.println("Input command " + "(start - starts server,"
                        + " stop - stops server):");
            } else {
                System.out.println("Server is running. Input command " + "(stop - stops server):");
            }
            String input = null;
            try {
                input = reader.readLine().trim();
            } catch (final IOException e) {
                System.err.println("Error while reading input!");
                return;
            }
            if (!serverStarted && input.toLowerCase().equals("start")) {
                serverStarted = true;
                httpServer.start();
            } else if (serverStarted && input.toLowerCase().equals("stop")) {
                httpServer.stop();
                return;
            }
        }
    }
}

package hr.fer.zemris.java.webserver.workers;

import hr.fer.zemris.java.webserver.IWebWorker;
import hr.fer.zemris.java.webserver.RequestContext;

import java.io.IOException;

/**
 * <p>
 * This class represents {@link IWebWorker} that creates a HTML page with parameter given in URL
 * listed.
 * </p>
 * Parameters are listed in key = value form.
 *
 * @author Stipan Mikulić
 * @version 1.0
 */
public class EchoParams implements IWebWorker {

    @Override
    public void processRequest(final RequestContext context) {
        try {
            context.write("Parameters: <br>");
            for (final String name : context.getParameterNames()) {
                context.write(name).write(" = ").write(context.getParameter(name) + "<br>");
            }
        } catch (final IOException e) {
            System.err.println("Error while writing to request context output stream!");
            return;
        }
    }
}

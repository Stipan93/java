package hr.fer.zemris.java.custom.collections;

/**
 * The ObjectStack class represents a last-in-first-out (LIFO) stack of objects. Usual stack methods
 * are provided.
 *
 * @author Stipan Mikulić
 * @version 1.0
 */
public class ObjectStack {
    /**
     * Array where elements of stack are stored.
     */
    private final ArrayBackedIndexedCollection collection;

    /**
     * Constructs an empty ObjectStack with an initial capacity of 16.
     */
    public ObjectStack() {
        this.collection = new ArrayBackedIndexedCollection();
    }

    /**
     * Initialize new empty ObjectStack with capacity initialCapacity.
     *
     * @param initialCapacity Initial capacity of ObjectStack
     * @throws IllegalArgumentException if initial capacity is less than 1.
     */
    public ObjectStack(final int initialCapacity) throws IllegalArgumentException {
        this.collection = new ArrayBackedIndexedCollection(initialCapacity);
    }

    /**
     * Returns true if ObjectStack contains no elements.
     *
     * @return true if ObjectStack contains no elements.
     */
    public boolean isEmpty() {
        return collection.isEmpty();
    }

    /**
     * Returns number of elements in ObjectStack.
     *
     * @return the number of elements in ObjectStack.
     */
    public int size() {
        return collection.size();
    }

    /**
     * Pushes an item onto the top of ObjectStack.
     *
     * @param value element to be pushed to ObjectStack
     * @throws IllegalArgumentException if value is null
     */
    public void push(final Object value) throws IllegalArgumentException {
        collection.add(value);
    }

    /**
     * Removes the object at the top of ObjectStack and returns that object as the value of this
     * function.
     *
     * @return The object at the top of ObjectStack
     * @throws IndexOutOfBoundsException if the index argument is negative or not less than the
     *         length of array.
     * @throws EmptyStackException if ObjectStack is empty
     */
    public Object pop() throws EmptyStackException {
        if (collection.isEmpty()) {
            throw new EmptyStackException();
        }
        final Object temporary = collection.get(collection.size() - 1);
        collection.remove(collection.size() - 1);
        return temporary;
    }

    /**
     * Returns, but not removes, object at the top of ObjectStack.
     *
     * @return The object at the top of ObjectStack
     * @throws EmptyStackException if ObjectStack is empty
     */
    public Object peek() throws EmptyStackException {
        if (collection.isEmpty()) {
            throw new EmptyStackException();
        }
        return collection.get(collection.size() - 1);
    }

    /**
     * Deletes all elements in ObjectStack.
     */
    public void clear() {
        collection.clear();
    }
}

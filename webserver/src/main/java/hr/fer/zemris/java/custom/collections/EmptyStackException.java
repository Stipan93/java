package hr.fer.zemris.java.custom.collections;

/**
 * Exception that is thrown to indicate that user is trying to get element from an empty stack.
 *
 * @author Stipan Mikulić
 * @version 1.0
 */
public class EmptyStackException extends RuntimeException {
    /**
     * Serial version ID.
     */
    private static final long serialVersionUID = -2537015940318072500L;

    /**
     * Constructs an EmptyStackException with no detail message.
     */
    public EmptyStackException() {
        super();
    }

    /**
     * Constructs an EmptyStackException with the specified detail message.
     *
     * @param message specified detail message
     */
    public EmptyStackException(final String message) {
        super(message);
    }

    /**
     * Constructs a new exception with the specified cause and a detail message which typically
     * contains the class and detail message of cause.
     *
     * @param cause specified cause of exception
     */
    public EmptyStackException(final Throwable cause) {
        super(cause);
    }

    /**
     * Constructs a new exception with the specified detail message and cause.
     *
     * @param message specified detail message
     * @param cause specified cause of exception
     */
    public EmptyStackException(final String message, final Throwable cause) {
        super(message, cause);
    }

}

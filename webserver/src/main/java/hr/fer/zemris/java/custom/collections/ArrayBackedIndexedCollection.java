package hr.fer.zemris.java.custom.collections;

/**
 * Implementation of array collection. General contract of this collection is: duplicate elements
 * are allowed; null references are not allowed. Collection provides default constructor and one
 * constructor with one argument (initial capacity of array). Also methods like add, remove,
 * isEmpty, clear, size, get, insert, indexOf and contains are provided.
 *
 * @author Stipan Mikulić
 * @version 1.0
 */
public class ArrayBackedIndexedCollection {
    /**
     * Default initial capacity.
     */
    private static final int DEFAULT_CAPACITY = 16;
    /**
     * Number of elements stored in array.
     */
    private int size;
    /**
     * Capacity of array.
     */
    private int capacity;
    /**
     * Array where elements are stored.
     */
    private Object[] elements;

    /**
     * Initialize new empty ArrayBackedIndexedCollection with capacity initialCapacity.
     *
     * @param initialCapacity Initial capacity of ArrayBackedIndexedCollection
     * @throws IllegalArgumentException if initial capacity is less than 1.
     */
    public ArrayBackedIndexedCollection(final int initialCapacity) throws IllegalArgumentException {
        if (initialCapacity < 1) {
            throw new IllegalArgumentException("Initial capacity must be greater than 1!");
        }
        size = 0;
        capacity = initialCapacity;
        elements = new Object[initialCapacity];
    }

    /**
     * Constructs an empty ArrayBackedIndexedCollection with an initial capacity of 16.
     */
    public ArrayBackedIndexedCollection() {
        this(DEFAULT_CAPACITY);
    }

    /**
     * Returns true if ArrayBackedIndexedCollection contains no elements.
     *
     * @return true if ArrayBackedIndexedCollection contains no elements.
     */
    public boolean isEmpty() {
        if (size > 0) {
            return false;
        }
        return true;
    }

    /**
     * Returns number of elements in ArrayBackedIndexedCollection.
     *
     * @return the number of elements in ArrayBackedIndexedCollection.
     */
    public int size() {
        return size;
    }

    /**
     * Appends the specified element to the end of ArrayBackedIndexedCollection.
     *
     * @param value element to be appended to ArrayBackedIndexedCollection
     * @throws IllegalArgumentException if value is null
     */
    public void add(final Object value) throws IllegalArgumentException {
        if (value == null) {
            throw new IllegalArgumentException("Cannot append null!");
        }
        if (size == capacity) {
            capacity *= 2;
            final Object[] temporary = new Object[capacity];
            for (int i = 0; i < size; i++) {
                temporary[i] = elements[i];
            }
            elements = temporary;
        }
        elements[size++] = value;
    }

    /**
     * Returns the element at the specified position in ArrayBackedIndexedCollection.
     *
     * @param index index of the element to return
     * @return the element at the specified position in ArrayBackedIndexedCollection
     * @throws IndexOutOfBoundsException if the index argument is negative or not less than the
     *         length of array.
     */
    public Object get(final int index) throws IndexOutOfBoundsException {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException();
        }
        return elements[index];
    }

    /**
     * Removes the element at the specified position in ArrayBackedIndexedCollection.
     *
     * @param index index of the element to remove
     * @throws IndexOutOfBoundsException if the index argument is negative or not less than the
     *         length of array.
     */
    public void remove(final int index) throws IndexOutOfBoundsException {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException();
        }
        for (int i = index; i < size - 1; i++) {
            elements[i] = elements[i + 1];
        }
        elements[size - 1] = null;
        size--;
    }

    /**
     * Inserts element at specified position in ArrayBackedIndexedCollection.
     *
     * @param value element to be inserted to ArrayBackedIndexedCollection
     * @param position index at which to insert the element
     */
    public void insert(final Object value, final int position) {
        Object temporary;
        add(value);
        for (int i = size - 1; i > position; i--) {
            temporary = elements[i];
            elements[i] = elements[i - 1];
            elements[i - 1] = temporary;
        }
    }

    /**
     * Returns the index of the first occurrence of the specified element in
     * ArrayBackedIndexedCollection, or -1 if ArrayBackedIndexedCollection does not contain the
     * element.
     *
     * @param value element to search for
     * @return the index of the first occurrence of the specified element in
     *         ArrayBackedIndexedCollection, or -1 if ArrayBackedIndexedCollection does not contain
     *         the element
     */
    public int indexOf(final Object value) {
        for (int i = 0; i < size; i++) {
            if (elements[i].equals(value)) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Returns true if ArrayBackedIndexedCollection contains specified element, otherwise false.
     *
     * @param value element that is searched for
     * @return returns true if ArrayBackedIndexedCollection contains specified element, otherwise
     *         false
     */
    public boolean contains(final Object value) {
        for (int i = 0; i < size; i++) {
            if (elements[i].equals(value)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Deletes all elements in ArrayBackedIndexedCollection.
     */
    public void clear() {
        for (int i = 0; i < size; i++) {
            elements[i] = null;
        }
        size = 0;
    }
}

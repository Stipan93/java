package hr.fer.zemris.java.webserver;

/**
 * <p>
 * This interface makes contract between class that implements it and {@link SmartHttpServer} in a
 * manner that class provides method {@link #processRequest(RequestContext)} for processing current
 * request.
 * </p>
 *
 * @author Stipan Mikulić
 * @version 1.0
 */
public interface IWebWorker {
    /**
     * This method creates a content for client using given {@link RequestContext} object.
     *
     * @param context
     *            {@link RequestContext} object
     */
    void processRequest(RequestContext context);
}

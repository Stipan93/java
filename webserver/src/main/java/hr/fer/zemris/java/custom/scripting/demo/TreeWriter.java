package hr.fer.zemris.java.custom.scripting.demo;

import hr.fer.zemris.java.custom.scripting.nodes.DocumentNode;
import hr.fer.zemris.java.custom.scripting.nodes.EchoNode;
import hr.fer.zemris.java.custom.scripting.nodes.ForLoopNode;
import hr.fer.zemris.java.custom.scripting.nodes.INodeVisitor;
import hr.fer.zemris.java.custom.scripting.nodes.TextNode;
import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParser;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * <p>
 * This class is demonstration program for {@link SmartScriptParser} and {@link INodeVisitor}.
 * </p>
 * This program expects one command line argument. Expected argument must be file name which
 * contains code that needs to be parsed and then printed on standard output.
 *
 * @author Stipan Mikulić
 * @version 1.0
 * @see SmartScriptParser
 * @see INodeVisitor
 */
public final class TreeWriter {
    /**
     * This constructor prevents instantiation.
     */
    private TreeWriter() {
        // Prevent instantiation
    }

    /**
     * <p>
     * This class implements {@link INodeVisitor}.
     * </p>
     * It implements method for visiting nodes in a manner that it reconstructs nodes into document
     * and prints it on standard output.
     *
     * @author Stipan Mikulić
     * @version 1.0
     * @see INodeVisitor
     */
    private static class WriterVisitor implements INodeVisitor {
        /**
         * End tag for every for-loop tag.
         */
        private static final String END_TAG = "{$END$}";

        @Override
        public void visitTextNode(final TextNode node) {
            System.out.print(node.getText());
        }

        @Override
        public void visitForLoopNode(final ForLoopNode node) {
            System.out.print(node.toString());
            for (int i = 0, size = node.numberOfChildren(); i < size; i++) {
                node.getChild(i).accept(this);
            }
            System.out.print(END_TAG);
        }

        @Override
        public void visitEchoNode(final EchoNode node) {
            System.out.print(node.toString());
        }

        @Override
        public void visitDocumentNode(final DocumentNode node) {
            for (int i = 0, size = node.numberOfChildren(); i < size; i++) {
                node.getChild(i).accept(this);
            }
        }
    }

    /**
     * This method is called when program starts.
     *
     * @param args command line arguments
     */
    public static void main(final String[] args) {
        if (args.length != 1) {
            System.err.println("Program expects file name as argument.");
            System.exit(1);
        } else {
            String docBody = null;
            try {
                docBody =
                        new String(Files.readAllBytes(Paths.get(args[0])), StandardCharsets.UTF_8);
            } catch (final IOException e) {
                System.err.println("Error while reading file!");
                System.exit(1);
            }
            final SmartScriptParser p = new SmartScriptParser(docBody);
            final WriterVisitor visitor = new WriterVisitor();
            p.getDocumentNode().accept(visitor);
        }
    }
}

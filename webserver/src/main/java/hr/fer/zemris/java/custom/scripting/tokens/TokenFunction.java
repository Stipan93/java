package hr.fer.zemris.java.custom.scripting.tokens;

/**
 * Represents a function.
 *
 * @author Stipan Mikulić
 * @version 1.0
 */
public class TokenFunction extends Token {
    /**
     * {@link TokenFunction} name.
     */
    private final String name;

    /**
     * Creates {@link TokenFunction} with given name.
     *
     * @param name {@link TokenFunction} name
     */
    public TokenFunction(final String name) {
        super();
        this.name = name;
    }

    /**
     * Returns {@link TokenFunction} name.
     *
     * @return {@link TokenFunction} name
     */
    public String getName() {
        return name;
    }

    /**
     * Returns {@link String} representation of {@link TokenFunction}.
     *
     * @return {@link TokenFunction} name
     */
    @Override
    public String asText() {
        return this.name;
    }

    @Override
    public String toString() {
        return this.name;
    }

}

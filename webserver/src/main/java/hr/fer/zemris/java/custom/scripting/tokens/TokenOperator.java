package hr.fer.zemris.java.custom.scripting.tokens;

/**
 * Represents a operator.
 *
 * @author Stipan Mikulić
 * @version 1.0
 */
public class TokenOperator extends Token {
    /**
     * {@link TokenOperator} symbol.
     */
    private final String symbol;

    /**
     * Creates {@link TokenOperator} with given symbol.
     *
     * @param symbol {@link TokenOperator} symbol
     */
    public TokenOperator(final String symbol) {
        super();
        this.symbol = symbol;
    }

    /**
     * Returns {@link TokenOperator} symbol.
     *
     * @return {@link TokenOperator} symbol
     */
    public String getSymbol() {
        return this.symbol;
    }

    /**
     * Returns {@link String} representation of {@link TokenOperator}.
     *
     * @return {@link TokenOperator} symbol
     */
    @Override
    public String asText() {
        return this.symbol;
    }

    @Override
    public String toString() {
        return this.symbol;
    }

}

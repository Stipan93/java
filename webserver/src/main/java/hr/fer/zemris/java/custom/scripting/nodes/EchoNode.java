package hr.fer.zemris.java.custom.scripting.nodes;

import hr.fer.zemris.java.custom.scripting.tokens.Token;
import hr.fer.zemris.java.custom.scripting.tokens.TokenConstantDouble;
import hr.fer.zemris.java.custom.scripting.tokens.TokenConstantInteger;
import hr.fer.zemris.java.custom.scripting.tokens.TokenFunction;
import hr.fer.zemris.java.custom.scripting.tokens.TokenOperator;
import hr.fer.zemris.java.custom.scripting.tokens.TokenString;
import hr.fer.zemris.java.custom.scripting.tokens.TokenVariable;

/**
 * A node representing a command which generates some textual output dynamically.
 *
 * @author Stipan Mikulić
 * @version 1.0
 */
public class EchoNode extends Node {
    /**
     * Sequence that represents empty echo tag.
     */
    public static final String EMPTY_ECHO_TAG = "\"--$--&emptyEchoTag&--$--\"";
    /**
     * Array of token arguments. Possible arguments are: {@link TokenString}, {@link TokenVariable},
     * {@link TokenFunction}, {@link TokenOperator} {@link TokenConstantDouble},
     * {@link TokenConstantInteger}
     */
    private final Token[] tokens;

    /**
     * Creates {@link EchoNode} with given arguments.
     *
     * @param tokens arguments of {@link EchoNode}
     */
    public EchoNode(final Token[] tokens) {
        super();
        this.tokens = tokens;
    }

    /**
     * Array of echo node Tokens.
     *
     * @return array of {@link EchoNode} Tokens
     */
    public Token[] getTokens() {
        return tokens;
    }

    /**
     * String representation of {@link EchoNode}.
     *
     * @return {@link EchoNode} text representation
     */
    @Override
    public String toString() {
        String result = "";

        // if echo tag is empty return empty tag
        if (getTokens()[0].toString().equals(EMPTY_ECHO_TAG)) {
            return "{$=  $}";
        }

        // concatenate all tokens that echo tag contains
        result += "{$=";
        for (final Token token : getTokens()) {
            result += " " + token.toString();
        }
        result += " $}";
        return result;
    }

    @Override
    public void accept(final INodeVisitor visitor) {
        visitor.visitEchoNode(this);
    }

}

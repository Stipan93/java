package hr.fer.zemris.java.custom.scripting.tokens;

/**
 * Represents a variable.
 *
 * @author Stipan Mikulić
 * @version 1.0
 */
public class TokenVariable extends Token {
    /**
     * {@link TokenVariable} name.
     */
    private final String name;

    /**
     * Creates {@link TokenVariable} with given name.
     *
     * @param name {@link TokenVariable} name
     */
    public TokenVariable(final String name) {
        super();
        this.name = name;
    }

    /**
     * Returns {@link TokenVariable} name.
     *
     * @return {@link TokenVariable} name
     */
    public String getName() {
        return this.name;
    }

    /**
     * Returns {@link String} representation of {@link TokenVariable}.
     *
     * @return {@link TokenVariable} name
     */
    @Override
    public String asText() {
        return this.name;
    }
}

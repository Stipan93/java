package hr.fer.zemris.java.custom.scripting.tokens;

/**
 * Represents a string.
 *
 * @author Stipan Mikulić
 * @version 1.0
 */
public class TokenString extends Token {
    /** Escape character. */
    private static final String ESCAPED_CHAR = "\\";

    /** Escaped escape character. */
    private static final String ESCAPED_ESCAPE_CHAR = "\\\\";

    /** Temporarily replacement for escaped escape character. */
    private static final String ESCAPED_ESC_CHAR_REPL = "%-dblEscReplacment-%";
    /**
     * {@link TokenString} value.
     */
    private final String value;

    /**
     * Creates {@link TokenString} with given value.
     *
     * @param value {@link TokenString} value
     */
    public TokenString(final String value) {
        super();
        this.value = value;
    }

    /**
     * Returns {@link TokenString} value.
     *
     * @return {@link TokenString} value
     */
    public String getValue() {
        return value.substring(1, value.length() - 1);
    }

    /**
     * Returns {@link TokenString} original representation where special characters does not exist.
     *
     * @return {@link TokenString} original representation
     */
    @Override
    public String asText() {
        return value;
    }

    @Override
    public String toString() {
        String result = getValue();
        result = result.substring(1, result.length() - 1);
        result = result.replace(ESCAPED_CHAR, ESCAPED_ESC_CHAR_REPL);
        result = result.replace("\r", "\\r");
        result = result.replace("\"", "\\\"");
        result = result.replace("\t", "\\t");
        result = result.replace("\n", "\\n");
        result = result.replace(ESCAPED_ESC_CHAR_REPL, ESCAPED_ESCAPE_CHAR);
        result = "\"" + result + "\"";
        return result;
    }

}

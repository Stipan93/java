package hr.fer.zemris.java.custom.scripting.parser;

/**
 * This class represents {@link Exception} that is thrown when error while parsing file has
 * occurred.
 *
 * @author Stipan Mikulić
 * @version 1.0
 */
public class SmartScriptParserException extends RuntimeException {
    /**
     * Serial version ID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Constructs an EmptyStackException with no detail message.
     */
    public SmartScriptParserException() {
        super();
    }

    /**
     * Constructs an EmptyStackException with the specified detail message.
     *
     * @param message
     *            specified detail message
     */
    public SmartScriptParserException(final String message) {
        super(message);
    }

    /**
     * Constructs a new exception with the specified cause and a detail message which typically
     * contains the class and detail message of cause.
     *
     * @param cause
     *            specified cause of exception
     */
    public SmartScriptParserException(final Throwable cause) {
        super(cause);
    }

    /**
     * Constructs a new exception with the specified detail message and cause.
     *
     * @param message
     *            specified detail message
     * @param cause
     *            specified cause of exception
     */
    public SmartScriptParserException(final String message, final Throwable cause) {
        super(message, cause);
    }
}

package hr.fer.zemris.java.custom.scripting.nodes;

import hr.fer.zemris.java.custom.scripting.tokens.Token;
import hr.fer.zemris.java.custom.scripting.tokens.TokenConstantDouble;
import hr.fer.zemris.java.custom.scripting.tokens.TokenConstantInteger;
import hr.fer.zemris.java.custom.scripting.tokens.TokenString;
import hr.fer.zemris.java.custom.scripting.tokens.TokenVariable;

/**
 * <p>
 * A node representing a single for-loop construct. Accepts three or four arguments.
 * </p>
 * First argument must be {@link TokenVariable}. Other possible arguments are: {@link TokenVariable}
 * , {@link TokenConstantDouble} {@link TokenConstantInteger}, {@link TokenString} which can be
 * parsed into number.
 *
 * @author Stipan Mikulić
 * @version 1.0
 */
public class ForLoopNode extends Node {
    /**
     * First argument, for-loop variable.
     */
    private final TokenVariable variable;
    /**
     * {@link Token} which is start state.
     */
    private final Token startExpression;
    /**
     * {@link Token} which is end state.
     */
    private final Token endExpression;
    /**
     * {@link Token} which represents step between states.
     */
    private final Token stepExpression;

    /**
     * Creates {@link ForLoopNode} with three or four arguments. StepExpression can be null.
     *
     * @param variable First argument, for-loop variable
     * @param startExpression {@link Token} which is start state
     * @param endExpression {@link Token} which is end state
     * @param stepExpression {@link Token} which represents step between states. It can be null.
     */
    public ForLoopNode(final TokenVariable variable, final Token startExpression,
            final Token endExpression, final Token stepExpression) {
        super();
        this.variable = variable;
        this.startExpression = startExpression;
        this.endExpression = endExpression;
        this.stepExpression = stepExpression;
    }

    /**
     * @return for-loop variable.
     */
    public TokenVariable getVariable() {
        return variable;
    }

    /**
     * Start {@link ForLoopNode} variable.
     *
     * @return {@link Token} which is start state
     */
    public Token getStartExpression() {
        return startExpression;
    }

    /**
     * End {@link ForLoopNode} variable.
     *
     * @return {@link Token} which is end state
     */
    public Token getEndExpression() {
        return endExpression;
    }

    /**
     * Step {@link ForLoopNode} variable.
     *
     * @return {@link Token} which represents step between states
     */
    public Token getStepExpression() {
        return stepExpression;
    }

    /**
     * String representation of {@link ForLoopNode}.
     *
     * @return {@link ForLoopNode} string representation
     */
    @Override
    public String toString() {
        String result = "";
        result +=
                "{$ FOR " + getVariable().toString() + " " + getStartExpression().toString() + " "
                        + getEndExpression().toString();

        // if step expression exists, concatenate it to result
        if (getStepExpression() == null) {
            result += " $}";
        } else {
            result += " " + getStepExpression().toString() + " $}";
        }
        return result;
    }

    @Override
    public void accept(final INodeVisitor visitor) {
        visitor.visitForLoopNode(this);
    }
}

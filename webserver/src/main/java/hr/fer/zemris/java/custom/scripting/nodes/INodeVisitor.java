package hr.fer.zemris.java.custom.scripting.nodes;

import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParser;

/**
 * <p>
 * This interface represents Visitor object that provides methods for visiting each {@link Node} in
 * {@link SmartScriptParser}.
 * </p>
 * Supported nodes are: {@link TextNode}, {@link ForLoopNode}, {@link DocumentNode} and
 * {@link EchoNode}.
 *
 * @author Stipan Mikulić
 * @version 1.0
 */
public interface INodeVisitor {
    /**
     * This method represents visitor for {@link TextNode}.
     *
     * @param node {@link TextNode} that is visited
     */
    void visitTextNode(TextNode node);

    /**
     * This method represents visitor for {@link ForLoopNode}.
     *
     * @param node {@link ForLoopNode} that is visited
     */
    void visitForLoopNode(ForLoopNode node);

    /**
     * This method represents visitor for {@link EchoNode}.
     *
     * @param node {@link EchoNode} that is visited
     */
    void visitEchoNode(EchoNode node);

    /**
     * This method represents visitor for {@link DocumentNode}.
     *
     * @param node {@link DocumentNode} that is visited
     */
    void visitDocumentNode(DocumentNode node);
}

package hr.fer.zemris.java.webserver.workers;

import hr.fer.zemris.java.webserver.IWebWorker;
import hr.fer.zemris.java.webserver.RequestContext;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

/**
 * <p>
 * This class represents {@link IWebWorker} that creates a HTML page with draw circle.
 * </p>
 * Circle is filled with random color.
 *
 * @author Stipan Mikulić
 * @version 1.0
 */
public class CircleWorker implements IWebWorker {
    /**
     * Default image dimension.
     */
    private static final int IMAGE_DIMENSION = 200;

    @Override
    public void processRequest(final RequestContext context) {
        final BufferedImage bim =
                new BufferedImage(IMAGE_DIMENSION, IMAGE_DIMENSION, BufferedImage.TYPE_3BYTE_BGR);
        final Graphics2D g2d = bim.createGraphics();
        g2d.setColor(Color.WHITE);
        g2d.fillRect(0, 0, IMAGE_DIMENSION, IMAGE_DIMENSION);
        final Color circleColor =
                new Color((float) Math.random(), (float) Math.random(), (float) Math.random());
        g2d.setColor(circleColor);
        g2d.fillOval(0, 0, IMAGE_DIMENSION, IMAGE_DIMENSION);
        g2d.dispose();
        final ByteArrayOutputStream bos = new ByteArrayOutputStream();
        context.setMimeType("image/png");
        try {
            ImageIO.write(bim, "png", bos);
            context.write(bos.toByteArray());
        } catch (final IOException e) {
            System.err.println("Error while writing to request context output stream!");
            return;
        }
    }

}

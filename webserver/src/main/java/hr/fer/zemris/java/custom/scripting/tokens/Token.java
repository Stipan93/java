package hr.fer.zemris.java.custom.scripting.tokens;

/**
 * Base class for all tokens.
 *
 * @author Stipan Mikulić
 * @version 1.0
 */
public class Token {

    /**
     * Returns empty {@link String} for this base class.
     *
     * @return empty {@link String}
     */
    public String asText() {
        return "";
    }
}

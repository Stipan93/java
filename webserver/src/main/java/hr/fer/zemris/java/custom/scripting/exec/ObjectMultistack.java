package hr.fer.zemris.java.custom.scripting.exec;

import java.util.EmptyStackException;
import java.util.HashMap;
import java.util.Map;

/**
 * This class represents a map where key is a String and value is a stack of MultistackEntrys.
 * Instead of default map methods, here are methods that usually are implemented in stack.
 *
 * @author Stipan Mikulić
 * @version 1.0
 */
public class ObjectMultistack {
    /**
     * Map for {@link MultistackEntry} mappings.
     */
    private final Map<String, MultistackEntry> map;

    /**
     * This class represents entry of ObjectMultistack.
     */
    public static class MultistackEntry {
        /**
         * Value of one specific entry.
         */
        private ValueWrapper value;
        /**
         * Pointer to next multistackEntry.
         */
        private final MultistackEntry next;

        /**
         * Creates a new MultistackEntry.
         *
         * @param value Value of new MultistackEntry
         * @param next Pointer to next entry
         */
        public MultistackEntry(final ValueWrapper value, final MultistackEntry next) {
            super();
            this.value = value;
            this.next = next;
        }

        /**
         * Returns {@link ValueWrapper} value.
         *
         * @return {@link ValueWrapper} value
         */
        public ValueWrapper getValue() {
            return value;
        }

        /**
         * Sets value of {@link ValueWrapper}.
         *
         * @param value {@link ValueWrapper} to set
         */
        public void setValue(final ValueWrapper value) {
            this.value = value;
        }
    }

    /**
     * Creates a new {@link ObjectMultistack}.
     */
    public ObjectMultistack() {
        map = new HashMap<String, MultistackEntry>();
    }

    /**
     * Method pushes new ultistackEntry with value of valueWrapper on specific stack.
     *
     * @param name Stack to push on
     * @param valueWrapper Value to push
     *
     */
    public void push(final String name, final ValueWrapper valueWrapper) {
        final MultistackEntry entry = new MultistackEntry(valueWrapper, map.get(name));
        map.put(name, entry);
    }

    /**
     * Method returns and removes element from top of the stack.
     *
     * @param name name of the stack
     * @return Value which is popped from stack.
     * @throws EmptyStackException if stack is empty
     */
    public ValueWrapper pop(final String name) throws EmptyStackException {
        final ValueWrapper retValue = peek(name);

        final MultistackEntry newTop = map.get(name);
        map.put(name, newTop.next);
        return retValue;
    }

    /**
     * Method returns element from top of the stack.
     *
     * @param name name of the stack to peek from
     * @return element from top of the stack
     * @throws EmptyStackException if stack is empty
     */
    public ValueWrapper peek(final String name) throws EmptyStackException {
        if (isEmpty(name)) {
            throw new EmptyStackException();
        }
        final MultistackEntry entry = map.get(name);
        return entry.value;
    }

    /**
     * Method returns true if specific stack is empty (specified by name).
     *
     * @param name name of the stack (i.e. key of a map)
     * @return <code>true</code> if empty, <code>false</code> otherwise.
     */
    public boolean isEmpty(final String name) {
        if (map.containsKey(name)) {
            if (map.get(name) != null) {
                return false;
            }
        }
        return true;
    }
}

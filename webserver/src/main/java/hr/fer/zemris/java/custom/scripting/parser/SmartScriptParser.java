package hr.fer.zemris.java.custom.scripting.parser;

import hr.fer.zemris.java.custom.collections.ObjectStack;
import hr.fer.zemris.java.custom.scripting.nodes.DocumentNode;
import hr.fer.zemris.java.custom.scripting.nodes.EchoNode;
import hr.fer.zemris.java.custom.scripting.nodes.ForLoopNode;
import hr.fer.zemris.java.custom.scripting.nodes.Node;
import hr.fer.zemris.java.custom.scripting.nodes.TextNode;
import hr.fer.zemris.java.custom.scripting.tokens.Token;
import hr.fer.zemris.java.custom.scripting.tokens.TokenConstantDouble;
import hr.fer.zemris.java.custom.scripting.tokens.TokenConstantInteger;
import hr.fer.zemris.java.custom.scripting.tokens.TokenFunction;
import hr.fer.zemris.java.custom.scripting.tokens.TokenOperator;
import hr.fer.zemris.java.custom.scripting.tokens.TokenString;
import hr.fer.zemris.java.custom.scripting.tokens.TokenVariable;

/**
 * This class represents parser for documents that contains four types of Nodes:
 * {@link DocumentNode}, {@link TextNode}, {@link ForLoopNode} and {@link EchoNode}. Every
 * {@link ForLoopNode} has its end tag. Parser separates document into Nodes and constructs document
 * tree with {@link DocumentNode} as root.
 *
 * @author Stipan Mikulić
 * @version 1.0
 */
public class SmartScriptParser {
    /** Sequence that represents empty echo tag. */
    public static final String EMPTY_ECHO_TAG = "\"--$$--&emptyEchoTag&--$$--\"";

    /** Sequence that signify where to split tags to get tokens. */
    private static final String SPLIT_SEQUENCE = "--&split&--";

    /** Escape character. */
    public static final String ESCAPE_CHAR = "\\";

    /** Escaped escape character. */
    public static final String ESCAPED_ESCAPE_CHAR = "\\\\";

    /** Temporarily replacement for escaped escape character. */
    public static final String ESCAPED_ESCAPE_CHAR_REPLACEMENT = "%%-doubleEscaeReplacment-%%";

    /** Temporarily replacement for escaped tag start sequence. */
    private static final String ESCAPED_START_TAG_REPLACEMENT = "&&-escapedTagStartReplacement-&&";

    /** Regex that represents variable name validation. */
    private static final String VARIABLE_NAME_VALIDATOR = "[a-zA-Z]\\w*";

    /** Regex that represents operator validation. */
    private static final String OPERATOR_VALIDATOR = "^[*|/|+|\\-|%]$";

    /** Regex that represents string validation. */
    private static final String STRING_VALIDATOR = "^\".*\"$";

    /** Regex that represents integer validation. */
    private static final String INTEGER_CONSTANT_VALIDATOR = "^[-+]?\\d+$";

    /** Regex that represents double validation. */
    private static final String DOUBLE_CONSTANT_VALIDATOR = "^[-+]?\\d+\\.\\d+$";

    /** Regex that represents function name validation. */
    private static final String FUNCTION_NAME_VALIDATOR = "@[a-zA-Z]\\w*";

    /** Tag start sequence. */
    private static final String TAG_START = "{$";

    /** Escaped tag start sequence. */
    private static final String ESCAPED_TAG_START = "\\{$";

    /** Tag end sequence. */
    private static final String TAG_END = "$}";
    /**
     * Number of for loops (must be equal to number of end tags).
     */
    private int numberOfForLoops;

    /**
     * Root of document tree.
     */
    private final DocumentNode documentNode;

    /**
     * Creates parser and parses given document content into document tree with {@link DocumentNode}
     * as a root.
     *
     * @param body
     *            document content
     * @throws SmartScriptParserException
     *             if document cannot be parsed
     */
    public SmartScriptParser(final String body) throws SmartScriptParserException {
        numberOfForLoops = 0;
        documentNode = parseDocument(body);
    }

    /**
     * Method that parses document content and returns root of tree.
     *
     * @param docContent
     *            document content
     * @return {@link DocumentNode}, root of document tree
     * @throws SmartScriptParserException
     *             if document cannot be parsed
     */
    private DocumentNode parseDocument(final String docContent) throws SmartScriptParserException {
        // replace some redundant content with special sequences so we could
        // efficiently parse document
        String content = optimizeContent(docContent);
        String text;
        String tag;
        final int startIndex = 0;

        final DocumentNode docNode = new DocumentNode();
        final ObjectStack stack = new ObjectStack();
        stack.push(docNode);
        // while there is some content, parse it
        while (!content.isEmpty()) {
            // if document contains tag start then parse text before tag and
            // parse tag.
            // if there is not tag start parse all text to the end of document
            if (content.contains(TAG_START)) {
                // cut string to the first occurrence of tag start
                text = content.substring(startIndex, content.indexOf(TAG_START));
                final String findTagEnd =
                        content.substring(content.indexOf(TAG_START) + TAG_START.length());

                // if there is text before tag process it
                if (!text.isEmpty()) {
                    processText(text, stack);
                }

                // if there is tag end process tag, otherwise throw exception
                if (findTagEnd.contains(TAG_END)) {
                    tag = findTagEnd.substring(startIndex, findTagEnd.indexOf(TAG_END));
                    processTag(tag, stack);
                    content = findTagEnd.substring(findTagEnd.indexOf(TAG_END) + TAG_END.length());
                } else {
                    throw new SmartScriptParserException(
                            "Unable to parse document! Invalid end of tag.");
                }
            } else {
                text = content.substring(startIndex);
                if (!text.isEmpty()) {
                    processText(text, stack);
                }
                content = content.substring(text.length());
            }
        }
        if (numberOfForLoops != 0) {
            throw new SmartScriptParserException("Unable to parse document!"
                    + " Number of for-loop tags is " + "not equal to number od end tags.");
        }
        return docNode;
    }

    /**
     * This method splits tag into array of string.
     *
     * @param tag
     *            given tag
     * @return array of string
     */
    private String[] splitTag(final String tag) {
        String[] tokeni = null;
        final StringBuilder buffer = new StringBuilder();
        char previous = 0;
        boolean inString = false;
        // empty echo tag
        if (tag.isEmpty()) {
            tokeni = new String[1];
            tokeni[0] = EMPTY_ECHO_TAG;
            return tokeni;
        }
        for (final char c : tag.toCharArray()) {
            // process strings
            if (c == '\"' && previous != '\\') {
                if (inString) {
                    inString = false;
                } else {
                    buffer.append(SPLIT_SEQUENCE);
                    inString = true;
                }
            }
            // if current char is not part of string
            if (!inString) {
                // process numbers, operators, functions and variables
                if (shouldISplit(c, previous)) {
                    buffer.append(SPLIT_SEQUENCE);
                }
            }
            buffer.append(c);
            previous = c;
        }
        // split tokens for current tag
        final String str = buffer.toString();
        if (str.length() >= SPLIT_SEQUENCE.length()) {
            if (str.substring(0, SPLIT_SEQUENCE.length()).equals(SPLIT_SEQUENCE)) {
                tokeni = str.substring(SPLIT_SEQUENCE.length()).split(SPLIT_SEQUENCE);
            } else {
                tokeni = buffer.toString().split(SPLIT_SEQUENCE);
            }
        } else {
            tokeni = buffer.toString().split(SPLIT_SEQUENCE);
        }
        return tokeni;
    }

    /**
     * This method decides does tag needs to be separated at current position.
     *
     * @param ch
     *            current position
     * @param previous
     *            previous position
     * @return true if separation is needed, otherwise false
     */
    private boolean shouldISplit(final char ch, final char previous) {
        // @formatter:off
        return previous == ' '
                && ch != ' '
                && ch != '\"'
                && ch != '@'
                || ch == '@'
                || (Character.isDigit(ch) && (previous != '-' && (previous == '\"'
                || Character.toString(previous).matches(OPERATOR_VALIDATOR) || previous == ' ')))
                || Character.toString(ch).matches(OPERATOR_VALIDATOR);
        // @formatter:on
    }

    /**
     * This method processes text node. Adds {@link TextNode} to its parent in document tree.
     *
     * @param text
     *            text for {@link TextNode}
     * @param stack
     *            stack which helps us to build a tree document
     */
    private void processText(final String text, final ObjectStack stack) {
        String newText = text;
        // replace special sequences that were added at parsing start in
        // original sequences
        newText = newText.replace(ESCAPED_ESCAPE_CHAR_REPLACEMENT, ESCAPED_ESCAPE_CHAR);
        newText = newText.replace(ESCAPED_START_TAG_REPLACEMENT, ESCAPED_TAG_START);
        // get current node parent from stack
        final Node parent = ((Node) stack.peek());
        parent.addChildNode(new TextNode(newText));
    }

    /**
     * This method processes tag node. Adds {@link EchoNode} or {@link ForLoopNode} to its parent in
     * document tree.
     *
     * @param tagToProcess
     *            tag that needs to be processed
     * @param stack
     *            stack which helps us to build a tree document
     */
    private void processTag(final String tagToProcess, final ObjectStack stack) {
        String tag = tagToProcess.trim();
        // replace special sequences that were added at parsing start in
        // original sequences
        tag = tag.replace(ESCAPED_ESCAPE_CHAR_REPLACEMENT, ESCAPED_ESCAPE_CHAR);
        final Node parent = ((Node) stack.peek());
        // if current tag is end tag then pop Node from stack
        if (tag.toLowerCase().equals("end")) {
            numberOfForLoops--;
            stack.pop();
        } else if (isForTag(tag.toLowerCase())) { // if its ForLoopTag, process
            numberOfForLoops++;
            final ForLoopNode forLoop = processForLoopTag(tag);
            parent.addChildNode(forLoop);
            stack.push(forLoop);
        } else if (tag.charAt(0) == '=') { // if its EchoTag, process it
            final EchoNode echo = processEchoTag(tag);
            parent.addChildNode(echo);
        } else { // otherwise throw exception
            throw new SmartScriptParserException("Invalid tag name!");
        }

    }

    /**
     * This method checks is {@link ForLoopNode} token valid.
     *
     * @param token
     *            token to check
     * @return true if token is invalid, otherwise false
     */
    private boolean validForTokens(final Token token) {
        return !(token instanceof TokenVariable) && !(token instanceof TokenString)
                && !(token instanceof TokenConstantInteger)
                && !(token instanceof TokenConstantDouble);
    }

    /**
     * This method processes and creates {@link ForLoopNode}. {@link ForLoopNode} can have three or
     * four arguments.
     *
     * @param arguments
     *            {@link ForLoopNode} arguments as one {@link String}
     * @return created {@link ForLoopNode}
     */
    private ForLoopNode processForLoopTag(final String arguments) {
        final String args =
                arguments.substring(arguments.toLowerCase().indexOf("for ") + "for ".length());
        // cut tag name
        final String[] stringTokens = splitTag(args);
        final Token[] tokens = makeTokens(stringTokens); // make tokens for this
        // Node
        if (!(tokens[0] instanceof TokenVariable)) {
            // if first argument is not variable, throw exception
            throw new SmartScriptParserException("First argument must be variable");
        }
        for (final Token token : tokens) { // check other token arguments in
                                           // Node
            if (validForTokens(token)) {
                // if token is invalid throw exception
                throw new SmartScriptParserException("Invalid token in for-loop node!");
            }
            if (token instanceof TokenString) {
                // if TokenString cannot be parsed into number throw exception
                try {
                    Double.parseDouble(((TokenString) token).getValue().substring(1,
                            ((TokenString) token).getValue().length() - 1));
                } catch (final NumberFormatException e) {
                    throw new SmartScriptParserException(
                            "Unable to parse document! Cannot parse string to double.");
                }
            }
        }
        ForLoopNode forLoop = null;
        // create ForLoopNode for 3 or 4 arguments
        if (tokens.length == 4) {
            forLoop = new ForLoopNode((TokenVariable) tokens[0], tokens[1], tokens[2], tokens[3]);
        } else if (tokens.length == 3) {
            forLoop =
                    new ForLoopNode((TokenVariable) tokens[0], tokens[1], tokens[2],
                            new TokenConstantInteger(1));
        } else {
            throw new SmartScriptParserException("Unable to parse document! Invalid forLoop node.");
        }
        return forLoop;
    }

    /**
     * This method processes and creates {@link EchoNode}.
     *
     * @param arguments
     *            {@link EchoNode} arguments as one {@link String}
     * @return created {@link EchoNode}
     */
    private EchoNode processEchoTag(final String arguments) {
        // cut tag name
        final String args = arguments.substring(arguments.indexOf("= ") + "= ".length());
        final String[] stringTokens = splitTag(args.trim());

        final Token[] tokens = makeTokens(stringTokens);
        final EchoNode echo = new EchoNode(tokens);
        return echo;
    }

    /**
     * Replaces some redundant content with special sequences in order to efficiently parse
     * document.
     *
     * @param content
     *            document content
     * @return document content with replaced sequences
     */
    private String optimizeContent(final String content) {
        String newContent = content;
        newContent = newContent.trim();
        newContent = newContent.replace(ESCAPED_ESCAPE_CHAR, ESCAPED_ESCAPE_CHAR_REPLACEMENT);
        newContent = newContent.replace(ESCAPED_TAG_START, ESCAPED_START_TAG_REPLACEMENT);
        return newContent;
    }

    /**
     * This method checks is given tag {@link ForLoopNode}.
     *
     * @param tag
     *            given {@link Node}
     * @return true if tag is {@link ForLoopNode}, otherwise false
     */
    private boolean isForTag(final String tag) {
        return tag.length() >= "for ".length() && tag.substring(0, "for ".length()).equals("for ");
    }

    /**
     * Creates and returns Tokens from given Strings.
     *
     * @param stringTokens
     *            array of strings from which we need to create Tokens
     * @return array of created Tokens
     * @throws SmartScriptParserException
     *             if there are invalid tokens that cannot be parsed
     */
    private Token[] makeTokens(final String[] stringTokens) throws SmartScriptParserException {
        final Token[] tokens = new Token[stringTokens.length];
        int counter = 0;

        // from every string make Token, if there is not valid Token for current
        // String throw exception
        for (String token : stringTokens) {
            token = token.trim();
            if (isString(token)) {
                token = editString(token);
                tokens[counter] = new TokenString(token);
            } else if (isOperator(token)) {
                tokens[counter] = new TokenOperator(token);
            } else if (isFunction(token)) {
                tokens[counter] = new TokenFunction(token);
            } else if (isVariable(token)) {
                tokens[counter] = new TokenVariable(token);
            } else if (isIntegerConstant(token)) {
                tokens[counter] = new TokenConstantInteger(Integer.parseInt(token));
            } else if (isDoubleConstant(token)) {
                tokens[counter] = new TokenConstantDouble(Double.parseDouble(token));
            } else {
                throw new SmartScriptParserException("Unable to parse document! Invalid token!");
            }
            counter++;
        }
        return tokens;
    }

    /**
     * Replaces special character sequences with their original meanings.
     *
     * @param token
     *            {@link String} which special characters need to be replaced
     * @return new edited {@link String}
     */
    private String editString(final String token) {
        String newToken = token;
        newToken = newToken.replace(ESCAPED_ESCAPE_CHAR, ESCAPED_ESCAPE_CHAR_REPLACEMENT);
        newToken = newToken.replace("\\r", "\r");
        newToken = newToken.replace("\\\"", "\"");
        newToken = newToken.replace("\\t", "\t");
        newToken = newToken.replace("\\n", "\n");
        newToken = newToken.replace(ESCAPED_ESCAPE_CHAR_REPLACEMENT, ESCAPE_CHAR);
        return newToken;
    }

    /**
     * Checks does given {@link String} matches {@link TokenConstantInteger} format.
     *
     * @param token
     *            given {@link String}
     * @return true if {@link String} matches {@link TokenConstantInteger} format, otherwise false
     */
    private boolean isIntegerConstant(final String token) {
        return token.matches(INTEGER_CONSTANT_VALIDATOR);
    }

    /**
     * Checks does given {@link String} matches {@link TokenConstantDouble} format.
     *
     * @param token
     *            given {@link String}
     * @return true if {@link String} matches {@link TokenConstantDouble} format, otherwise false
     */
    private boolean isDoubleConstant(final String token) {
        return token.matches(DOUBLE_CONSTANT_VALIDATOR);
    }

    /**
     * Checks does given {@link String} matches {@link TokenVariable} format.
     *
     * @param token
     *            given {@link String}
     * @return true if {@link String} matches {@link TokenVariable} format, otherwise false
     */
    private boolean isVariable(final String token) {
        return token.matches(VARIABLE_NAME_VALIDATOR);
    }

    /**
     * Checks does given {@link String} matches {@link TokenFunction} format.
     *
     * @param token
     *            given {@link String}
     * @return true if {@link String} matches {@link TokenFunction} format, otherwise false
     */
    private boolean isFunction(final String token) {
        return token.matches(FUNCTION_NAME_VALIDATOR);
    }

    /**
     * Checks does given {@link String} matches {@link TokenOperator} format.
     *
     * @param token
     *            given {@link String}
     * @return true if {@link String} matches {@link TokenOperator} format, otherwise false
     */
    private boolean isOperator(final String token) {
        return token.matches(OPERATOR_VALIDATOR);
    }

    /**
     * Checks does given {@link String} matches {@link TokenString} format.
     *
     * @param token
     *            given {@link String}
     * @return true if {@link String} matches {@link TokenString} format, otherwise false
     */
    private boolean isString(final String token) {
        return token.matches(STRING_VALIDATOR);
    }

    /**
     * Returns root of document tree.
     *
     * @return {@link DocumentNode} generated by {@link SmartScriptParser}.
     */
    public DocumentNode getDocumentNode() {
        return documentNode;
    }
}

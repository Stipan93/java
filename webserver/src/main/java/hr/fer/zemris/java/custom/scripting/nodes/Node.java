package hr.fer.zemris.java.custom.scripting.nodes;

import hr.fer.zemris.java.custom.collections.ArrayBackedIndexedCollection;

/**
 * Base class for all graph nodes.
 *
 * @author Stipan Mikulić
 * @version 1.0
 */
public abstract class Node {
    /**
     * Array where children of current Node are stored.
     */
    private ArrayBackedIndexedCollection children = null;

    /**
     * Adds Node to array of children. Constructs array when adding first child.
     *
     * @param child Node to add
     */
    public void addChildNode(final Node child) {
        if (children == null) {
            children = new ArrayBackedIndexedCollection();
        }
        children.add(child);
    }

    /**
     * Returns child Node at specified position.
     *
     * @param index specified position in array
     * @return child Node at given index
     * @throws IllegalArgumentException if this Node does not have children
     * @throws IndexOutOfBoundsException if given index is out of bounds
     */
    public Node getChild(final int index) throws IllegalArgumentException,
            IndexOutOfBoundsException {
        if (this.children == null) {
            throw new IllegalArgumentException("Node does not have children!");
        }
        if ((index < 0) || (index >= this.children.size())) {
            throw new IndexOutOfBoundsException("Index must be in interval [0,ArraySize - 1]!");
        }
        return (Node) this.children.get(index);
    }

    /**
     * Returns number of children of this Node.
     *
     * @return number of children
     */
    public int numberOfChildren() {
        if (children != null) {
            return this.children.size();
        } else {
            return 0;
        }
    }

    /**
     * This method delegates printing to {@link INodeVisitor} implementations method this object.
     *
     * @param visitor {@link INodeVisitor} implementation
     */
    public abstract void accept(INodeVisitor visitor);

}

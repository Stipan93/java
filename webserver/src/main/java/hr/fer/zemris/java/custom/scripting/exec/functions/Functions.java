package hr.fer.zemris.java.custom.scripting.exec.functions;

import hr.fer.zemris.java.custom.scripting.exec.SmartScriptEngine;
import hr.fer.zemris.java.webserver.RequestContext;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class contains functions that {@link SmartScriptEngine} supports.
 * </p>
 * Every {@link IFunction} is mapped by its name as key. Functions can be retrieved by method
 * {@link #getFunction(String)}.
 *
 * @author Stipan Mikulić
 * @version 1.0
 */
public class Functions {
    /**
     * Map where functions are stored.
     */
    private static Map<String, IFunction> funcitons = new HashMap<>();
    /**
     * {@link RequestContext} object.
     */
    private final RequestContext requestContext;

    /**
     * Creates new {@link Functions} object.
     *
     * @param requestContext
     *            {@link RequestContext} object
     */
    public Functions(final RequestContext requestContext) {
        this.requestContext = requestContext;
        initFunctions();
    }

    /**
     * This method initialize all supported functions for {@link SmartScriptEngine}.
     */
    private void initFunctions() {
        funcitons.put("@sin", (stack) -> {
            final Object arg = stack.pop();
            if (arg instanceof String) {
                stack.push(Math.sin(Math.toRadians(Double.parseDouble((String) arg))));
            } else if (arg instanceof Integer) {
                stack.push(Math.sin(Math.toRadians((Integer) arg)));
            } else if (arg instanceof Double) {
                stack.push(Math.sin(Math.toRadians((Double) arg)));
            }

        });
        funcitons.put("@decfmt", (stack) -> {
            final Object format = stack.pop();
            if (!(format instanceof String)) {
                throw new IllegalArgumentException("Format for decimal formatter must be string!");
            }
            final DecimalFormat formatter = new DecimalFormat((String) format);
            Object arg = stack.pop();
            if (arg instanceof String) {
                try {
                    arg = Integer.parseInt((String) arg);
                } catch (final Exception e) {
                    arg = Double.parseDouble((String) arg);
                }
            }
            stack.push(formatter.format(arg));
        });
        funcitons.put("@dup", (stack) -> {
            final Object dupValue = stack.pop();
            stack.push(dupValue);
            stack.push(dupValue);
        });
        funcitons.put("@swap", (stack) -> {
            final Object first = stack.pop();
            final Object second = stack.pop();
            stack.push(first);
            stack.push(second);
        });
        funcitons.put("@setMimeType", (stack) -> {
            final Object arg = stack.pop();
            if (arg instanceof String) {
                requestContext.setMimeType((String) arg);
            } else {
                throw new IllegalArgumentException("Mime type must be string!");
            }
        });
        funcitons.put("@paramGet", (stack) -> {
            final Object defValue = stack.pop();
            final Object name = stack.pop();
            if (!(name instanceof String)) {
                throw new IllegalArgumentException("Parameters key must be string!");
            }
            final String value = requestContext.getParameter((String) name);
            if (value == null) {
                stack.push(defValue);
            } else {
                stack.push(value);
            }
        });
        funcitons.put("@pparamGet", (stack) -> {
            final Object defValue = stack.pop();
            final Object name = stack.pop();
            if (!(name instanceof String)) {
                throw new IllegalArgumentException("Parameters key must be string!");
            }
            final String value = requestContext.getPersistentParameter((String) name);
            if (value == null) {
                stack.push(defValue);
            } else {
                stack.push(value);
            }
        });
        funcitons.put("@pparamSet", (stack) -> {
            final Object name = stack.pop();
            final Object value = stack.pop();
            if (!(name instanceof String)) {
                throw new IllegalArgumentException("Persistent parameters key must be string!");
            }
            if (value instanceof String) {
                requestContext.setPersistentParameter((String) name, (String) value);
            } else {
                requestContext.setPersistentParameter((String) name, String.valueOf(value));
            }
        });
        funcitons.put("@pparamDel", (stack) -> {
            final Object name = stack.pop();
            if (name instanceof String) {
                requestContext.removePersistentParameter((String) name);
            } else {
                throw new IllegalArgumentException("Persistent parameters key must be string!");
            }
        });
        funcitons.put("@tparamGet", (stack) -> {
            final Object defValue = stack.pop();
            final Object name = stack.pop();
            if (!(name instanceof String)) {
                throw new IllegalArgumentException("Parameters key must be string!");
            }
            final String value = requestContext.getTemporaryParameter((String) name);
            if (value == null) {
                stack.push(defValue);
            } else {
                stack.push(value);
            }
        });
        funcitons.put("@tparamSet", (stack) -> {
            final Object name = stack.pop();
            final Object value = stack.pop();
            if (!(name instanceof String)) {
                throw new IllegalArgumentException("Temporary parameters key must be string!");
            }
            if (value instanceof String) {
                requestContext.setTemporaryParameter((String) name, (String) value);
            } else {
                requestContext.setTemporaryParameter((String) name, String.valueOf(value));
            }
        });
        funcitons.put("@tparamDel", (stack) -> {
            final Object name = stack.pop();
            if (name instanceof String) {
                requestContext.removeTemporaryParameter((String) name);
            } else {
                throw new IllegalArgumentException("Persistent parameters key must be string!");
            }
        });

    }

    /**
     * This method returns function that is mapped with given name.
     *
     * @param name
     *            key for function mapping
     * @return {@link IFunction} with given key or <code>null</code> if mapping for the key does not
     *         exists.
     */
    public IFunction getFunction(final String name) {
        return funcitons.get(name);
    }
}

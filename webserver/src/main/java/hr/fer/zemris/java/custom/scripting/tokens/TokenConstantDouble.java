package hr.fer.zemris.java.custom.scripting.tokens;

/**
 * Represents a double constant.
 *
 * @author Stipan Mikulić
 * @version 1.0
 */
public class TokenConstantDouble extends Token {
    /**
     * {@link TokenConstantDouble} value.
     */
    private final double value;

    /**
     * Creates {@link TokenConstantDouble} with given value.
     *
     * @param value {@link TokenConstantDouble} value
     */
    public TokenConstantDouble(final double value) {
        super();
        this.value = value;
    }

    /**
     * Returns {@link TokenConstantDouble} value.
     *
     * @return {@link TokenConstantDouble} value
     */
    public double getValue() {
        return value;
    }

    /**
     * Returns textual representation for {@link TokenConstantDouble} value.
     *
     * @return {@link TokenConstantDouble} value
     */
    @Override
    public String asText() {
        return String.valueOf(this.value);
    }

    @Override
    public String toString() {
        return String.valueOf(this.value);
    }
}

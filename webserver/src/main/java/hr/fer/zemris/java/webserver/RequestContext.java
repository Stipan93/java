package hr.fer.zemris.java.webserver;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * <p>
 * This class represents request that needs to be send to {@link SmartHttpServer}.
 * </p>
 * It provides methods for setting needed header attributes. If attributes are not explicitly set,
 * default values are used.
 *
 * @author Stipan Mikulić
 * @version 1.0
 */
public class RequestContext {
    /**
     * Default encoding for header.
     */
    private static final String DEFAULT_ENCODING_FOR_HEADER = "ISO_8859_1";
    /**
     * Default encoding.
     */
    private static final String DEFAULT_ENCODING = "UTF-8";
    /**
     * Default status code.
     */
    private static final int DEFAULT_STATUS_CODE = 200;
    /**
     * Default status text.
     */
    private static final String DEFAULT_STATUS_TEXT = "OK";
    /**
     * Default mime type.
     */
    private static final String DEFAULT_MIME_TYPE = "text/html";
    /**
     * Default value for {@link #headerGenerated}.
     */
    private static final boolean DEFAULT_HEADER_GENERATED = false;

    /**
     * <p>
     * This class represents {@link RCCookie}.
     * </p>
     *
     * @author Stipan Mikulić
     * @version 1.0
     */
    public static class RCCookie {
        /**
         * Cookie name.
         */
        private final String name;
        /**
         * Cookie value.
         */
        private final String value;
        /**
         * Cookie domain.
         */
        private final String domain;
        /**
         * Cookie path.
         */
        private final String path;
        /**
         * Cookie max age.
         */
        private final Integer maxAge;
        /**
         * Represents http only cookie.
         */
        private final String cookieType;

        /**
         * Creates new {@link RCCookie}.
         *
         * @param name
         *            cookie name
         * @param value
         *            cookie value
         * @param domain
         *            cookie domain
         * @param path
         *            cookie path
         * @param maxAge
         *            cookie max age
         * @param type
         *            determines cookie type
         */
        public RCCookie(final String name, final String value, final Integer maxAge,
                final String domain, final String path, final boolean type) {
            super();
            this.name = name;
            this.value = value;
            this.domain = domain;
            this.path = path;
            this.maxAge = maxAge;
            if (type) {
                cookieType = "HttpOnly";
            } else {
                cookieType = "";
            }
        }

        /**
         * @return cookie name
         */
        public final String getName() {
            return name;
        }

        /**
         * @return cookie name
         */
        public final String getValue() {
            return value;
        }

        /**
         *
         * @return cookie domain
         */
        public final String getDomain() {
            return domain;
        }

        /**
         * @return cookie path
         */
        public final String getPath() {
            return path;
        }

        /**
         * @return cookie max age
         */
        public final Integer getMaxAge() {
            return maxAge;
        }
    }

    /**
     * {@link OutputStream} where methods {@link RequestContext#write(byte[])} and
     * {@link RequestContext#write(String)} writes.
     */
    private final OutputStream outputStream;
    /**
     * Charset which is used while writing to {@link OutputStream}.
     */
    private Charset charset;
    /**
     * Encoding for creating {@link Charset}.
     */
    private String encoding;
    /**
     * Status code.
     */
    private int statusCode;
    /**
     * Status text.
     */
    private String statusText;
    /**
     * Mime type.
     */
    private String mimeType;
    /**
     * Map of parameters.
     */
    private Map<String, String> parameters;
    /**
     * Map of temporary parameters.
     */
    private final Map<String, String> temporaryParameters;
    /**
     * Map of persistent parameters.
     */
    private final Map<String, String> persistentParameters;
    /**
     * List of {@link RCCookie} objects that header contains.
     */
    private final List<RCCookie> outputCookies;
    /**
     * Flag that determines is header generated.
     */
    private boolean headerGenerated;

    /**
     * Creates new {@link RequestContext}.
     *
     * @param outputStream
     *            {@link RequestContext} output stream
     * @param parameters
     *            map of parameters
     * @param persistentParameters
     *            map of persistent parameters
     * @param outputCookies
     *            list of {@link RCCookie} objects for header
     */
    public RequestContext(final OutputStream outputStream, final Map<String, String> parameters,
            final Map<String, String> persistentParameters, final List<RCCookie> outputCookies) {
        if (outputStream == null) {
            throw new IllegalArgumentException("Output streamn cannot be null!");
        }
        encoding = DEFAULT_ENCODING;
        statusCode = DEFAULT_STATUS_CODE;
        statusText = DEFAULT_STATUS_TEXT;
        mimeType = DEFAULT_MIME_TYPE;
        headerGenerated = DEFAULT_HEADER_GENERATED;
        this.outputStream = outputStream;
        temporaryParameters = new HashMap<String, String>();
        if (parameters == null) {
            this.parameters = Collections.unmodifiableMap(new HashMap<>());
        } else {
            this.parameters = Collections.unmodifiableMap(parameters);
        }
        if (persistentParameters == null) {
            this.persistentParameters = new HashMap<>();
        } else {
            this.persistentParameters = persistentParameters;
        }
        if (outputCookies == null) {
            this.outputCookies = new ArrayList<>();
        } else {
            this.outputCookies = outputCookies;
        }
    }

    /**
     * This method sets {@link #encoding} to given value.
     *
     * @param encoding
     *            {@link String} to set
     * @throws RuntimeException
     *             if header is generated
     */
    public final void setEncoding(final String encoding) throws RuntimeException {
        headerGeneratedValidation();
        this.encoding = encoding;
    }

    /**
     * This method sets {@link #statusCode} to given value.
     *
     * @param statusCode
     *            {@code int} to set
     * @throws RuntimeException
     *             if header is generated
     */
    public final void setStatusCode(final int statusCode) throws RuntimeException {
        headerGeneratedValidation();
        this.statusCode = statusCode;
    }

    /**
     * This method sets {@link #statusText} to given value.
     *
     * @param statusText
     *            {@link String} to set
     * @throws RuntimeException
     *             if header is generated
     */
    public final void setStatusText(final String statusText) throws RuntimeException {
        headerGeneratedValidation();
        this.statusText = statusText;
    }

    /**
     * This method sets {@link #mimeType} to given value.
     *
     * @param mimeType
     *            {@link String} to set
     * @throws RuntimeException
     *             if header is generated
     */
    public final void setMimeType(final String mimeType) throws RuntimeException {
        headerGeneratedValidation();
        this.mimeType = mimeType;
    }

    /**
     * This method add new {@link RCCookie}.
     *
     * @param rcCookie
     *            {@link RCCookie} to add.
     */
    public final void addRCCookie(final RCCookie rcCookie) {
        headerGeneratedValidation();
        outputCookies.add(rcCookie);
    }

    /**
     * This method throws {@link RuntimeException} if header is already generated.
     *
     * @throws RuntimeException
     *             if header is generated
     */
    private void headerGeneratedValidation() throws RuntimeException {
        if (headerGenerated) {
            throw new RuntimeException();
        }
    }

    /**
     * This method retrieves value from parameters map (or null if no association exists).
     *
     * @param name
     *            key to get from parameters map
     * @return retrieved value from map
     */
    public final String getParameter(final String name) {
        return parameters.get(name);
    }

    /**
     * This method retrieves names of all parameters in {@link #parameters} map.
     *
     * @return read-only {@link Set} with names of all parameters
     */
    public final Set<String> getParameterNames() {
        return Collections.unmodifiableSet(parameters.keySet());
    }

    /**
     * This method retrieves value from {@link #persistentParameters} map (or null if no association
     * exists).
     *
     * @param name
     *            key to get from parameters map
     * @return retrieved value from map
     */
    public final String getPersistentParameter(final String name) {
        return persistentParameters.get(name);
    }

    /**
     * This method retrieves names of all parameters in {@link #persistentParameters} map.
     *
     * @return read-only {@link Set} with names of all persistent parameters
     */
    public final Set<String> getPersistentParameterNames() {
        return Collections.unmodifiableSet(persistentParameters.keySet());
    }

    /**
     * This method stores a value to {@link #persistentParameters} map.
     *
     * @param name
     *            key for entry
     * @param value
     *            value for entry
     */
    public final void setPersistentParameter(final String name, final String value) {
        persistentParameters.put(name, value);
    }

    /**
     * This method removes a value from {@link #persistentParameters} map.
     *
     * @param name
     *            key for mapping to remove
     */
    public final void removePersistentParameter(final String name) {
        persistentParameters.remove(name);
    }

    /**
     * This method retrieves value from {@link #temporaryParameters} map (or null if no association
     * exists).
     *
     * @param name
     *            key to get from parameters map
     * @return retrieved value from map
     */
    public final String getTemporaryParameter(final String name) {
        return temporaryParameters.get(name);
    }

    /**
     * This method retrieves names of all parameters in {@link #temporaryParameters} map.
     *
     * @return read-only {@link Set} with names of all temporary parameters
     */
    public final Set<String> getTemporaryParameterNames() {
        return Collections.unmodifiableSet(temporaryParameters.keySet());
    }

    /**
     * This method stores a value to {@link #temporaryParameters} map.
     *
     * @param name
     *            key for entry
     * @param value
     *            value for entry
     */
    public final void setTemporaryParameter(final String name, final String value) {
        temporaryParameters.put(name, value);
    }

    /**
     * This method removes a value from {@link #temporaryParameters} map.
     *
     * @param name
     *            key for mapping to remove
     */
    public final void removeTemporaryParameter(final String name) {
        temporaryParameters.remove(name);
    }

    /**
     * This method writes given byte array to {@link OutputStream}.
     *
     * @param data
     *            array to write
     * @return reference to {@link RequestContext} object (i.e. this)
     * @throws IOException
     *             if error while writing to stream occurred.
     */
    public final RequestContext write(final byte[] data) throws IOException {
        if (!headerGenerated) {
            outputStream.write(generateHeader().getBytes(
                    Charset.forName(DEFAULT_ENCODING_FOR_HEADER)));
            charset = Charset.forName(encoding);
            headerGenerated = true;
        }
        outputStream.write(data);
        outputStream.flush();
        return this;
    }

    /**
     * This method writes given {@link String} to {@link OutputStream}.
     *
     * @param text
     *            {@link String} to write
     * @return reference to {@link RequestContext} object (i.e. this)
     * @throws IOException
     *             if error while writing to stream occurred.
     */
    public final RequestContext write(final String text) throws IOException {
        if (!headerGenerated) {
            charset = Charset.forName(encoding);
        }
        return write(text.getBytes(charset));
    }

    /**
     * This method generates header.
     *
     * @return {@link String} representation of created header
     */
    private String generateHeader() {
        charset = Charset.forName(encoding);
        final StringBuilder header = new StringBuilder();
        header.append("HTTP/1.1 " + statusCode + " " + statusText + "\r\n");
        header.append("Content-Type: " + mimeType);
        if (mimeType.startsWith("text/")) {
            header.append("; charset=" + encoding);
        }
        header.append("\r\n");
        apppendCookies(header);
        header.append("\r\n");
        return header.toString();
    }

    /**
     * This method appends descriptions of {@link RCCookie} objects to header.
     *
     * @param header
     *            {@link StringBuilder} where cookie description is appended.
     */
    private void apppendCookies(final StringBuilder header) {
        for (final RCCookie cookie : outputCookies) {
            header.append("Set-Cookie: ");
            header.append(cookie.getName() + "=\"" + cookie.getValue() + "\"");
            if (cookie.getDomain() != null) {
                header.append("; Domain=" + cookie.getDomain());
            }
            if (cookie.getPath() != null) {
                header.append("; Path=" + cookie.getPath());
            }
            if (cookie.getMaxAge() != null) {
                header.append("; Max-Age=" + cookie.getMaxAge());
            }
            if (!cookie.cookieType.equals("")) {
                header.append("; " + cookie.cookieType);
            }
            header.append("\r\n");
        }
    }
}

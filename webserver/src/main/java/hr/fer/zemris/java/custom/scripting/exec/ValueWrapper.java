package hr.fer.zemris.java.custom.scripting.exec;


/**
 * This class wraps {@link Object} value. Valid objects for wrapped value are {@link String},
 * {@link Double} and {@link Integer}.
 *
 * @author Stipan Mikulić
 * @version 1.0
 */
public class ValueWrapper {
    /**
     * This enum class represents type of operation which needs to be done. Example: ( * ) is
     * operator
     *
     * <pre>
     *  Integer * Integer = Integer
     *  Integer * Double = Double
     *  Double * Integer = Double
     *  Double * Double = Double
     * </pre>
     *
     *
     * @author Stipan Mikulić
     * @version 1.0
     */
    private enum OperationType {
        /**
         * This enum defines Double as operation type.
         */
        DOUBLE,
        /**
         * This enum defines Integer as operation type.
         */
        INTEGER;
    }

    /**
     * Regex for checking double format.
     */
    private static String dblRegex = "[-+]?[0-9]*\\.[0-9]+([eE][-+]?[0-9]+)?";
    /**
     * Stored value for {@link ValueWrapper} object.
     */
    private Object value;

    /**
     * Creates new valueWrapper with {@link Object} given as argument.
     *
     * @param value given {@link Object}
     */
    public ValueWrapper(final Object value) {
        super();
        this.value = value;
    }

    /**
     * Sets {@link ValueWrapper} value.
     *
     * @param value {@link Object} value to set
     */
    public void setValue(final Object value) {
        this.value = value;
    }

    /**
     * Returns {@link ValueWrapper} value.
     *
     * @return {@link Object} value
     */
    public Object getValue() {
        return value;
    }

    /**
     * This method increment stored value for value given as argument.
     *
     * @param incValue given increment
     */
    public void increment(final Object incValue) {
        final OperationType operationType = checkOperation(incValue);
        if (operationType == OperationType.DOUBLE) {
            value =
                    new Double((Double) getTypeForOperation(operationType)
                            + (Double) getRightType(incValue, operationType));
        } else {
            value =
                    new Integer((Integer) getTypeForOperation(operationType)
                            + (Integer) getRightType(incValue, operationType));
        }
    }

    /**
     * This method decrement stored value for value given as argument.
     *
     * @param decValue given decrement
     */
    public void decrement(final Object decValue) {
        value = substract(decValue);
    }

    /**
     * This method subtract given value from stored value.
     *
     * @param decValue given subtraction
     * @return new {@link Object} which is result of subtraction
     */
    private Object substract(final Object decValue) {
        final OperationType operationType = checkOperation(decValue);
        if (operationType == OperationType.DOUBLE) {
            return new Double((Double) getTypeForOperation(operationType)
                    - (Double) getRightType(decValue, operationType));
        } else {
            return new Integer((Integer) getTypeForOperation(operationType)
                    - (Integer) getRightType(decValue, operationType));
        }
    }

    /**
     * This method multiply stored value with value given as argument.
     *
     * @param mulValue value to multiply with
     */
    public void multiply(final Object mulValue) {
        final OperationType operationType = checkOperation(mulValue);
        if (operationType == OperationType.DOUBLE) {
            value =
                    new Double((Double) getTypeForOperation(operationType)
                            * (Double) getRightType(mulValue, operationType));
        } else {
            value =
                    new Integer((Integer) getTypeForOperation(operationType)
                            * (Integer) getRightType(mulValue, operationType));
        }
    }

    /**
     * This method divide stored value with value given as argument.
     *
     * @param divValue divisor, value to divide with
     * @throws ArithmeticException when zero division is tried
     */
    public void divide(final Object divValue) {
        final OperationType operationType = checkOperation(divValue);

        if (operationType == OperationType.DOUBLE) {
            final Double divisor = (Double) getRightType(divValue, operationType);
            if (divisor == 0) {
                throw new ArithmeticException("Cannot divide by zero!");
            }
            value = new Double((Double) getTypeForOperation(operationType) / divisor);
        } else {
            final Integer divisor = (Integer) getRightType(divValue, operationType);
            if (divisor == 0) {
                throw new ArithmeticException("Cannot divide by zero!");
            }
            value = new Integer((Integer) getTypeForOperation(operationType) / divisor);
        }
    }

    /**
     * This method perform numerical comparison between currently stored value in
     * {@link ValueWrapper} and given argument.
     *
     * @param withValue Value to compare
     * @return Integer less than zero if currently stored value is smaller than argument, an integer
     *         greater than zero if currently stored value is larger than argument or an integer 0
     *         if they are equal.
     */
    public int numCompare(final Object withValue) {
        if (withValue == null && value == null) {
            return 0;
        }
        if (withValue == null && value != null) {
            return (Integer) getRightType(value, OperationType.INTEGER);
        } else if (withValue != null && value == null) {
            return new Integer(0) - (Integer) getRightType(withValue, OperationType.INTEGER);
        }
        Object result = substract(withValue);
        if (result instanceof Double) {
            result = ((Double) result).intValue();
        }
        return (Integer) result;
    }

    /**
     * Method returns right type for given operation.
     *
     * @param operationType OperationType that determines operation
     * @return new instance of {@link Integer} or {@link Double} depending on {@link OperationType}
     */
    private Object getTypeForOperation(final OperationType operationType) {
        if (operationType == OperationType.DOUBLE) {
            if (value instanceof String) {
                return Double.parseDouble((String) value);
            }
            return ((Double) value).doubleValue();
        } else {
            if (value instanceof String) {
                return Integer.parseInt((String) value);
            }
            return ((Integer) value).intValue();
        }
    }

    /**
     * This method determines which operation should be done.
     *
     * @param opValue second operand
     * @return {@link OperationType#DOUBLE} if operation on {@link Double} values is needed,
     *         {@link OperationType#INTEGER} otherwise
     */
    private OperationType checkOperation(final Object opValue) {
        if (value instanceof Double || opValue instanceof Double) {
            if (value instanceof Integer) {
                value = ((Integer) value).doubleValue();
            }
            return OperationType.DOUBLE;
        } else {
            return OperationType.INTEGER;
        }
    }

    /**
     * This method gets right type for stored value for operation with given second operand and
     * OperationType.
     *
     * @param value second operand
     * @param operationType OperationType which determines needed type of operation
     * @return {@link Object} that is valid type for operations
     * @throws RuntimeException If arguments are invalid
     */
    private Object getRightType(final Object value, final OperationType operationType) {
        Object newValue;
        if (value == null) {
            newValue = new Integer(0);
        } else {
            if (value instanceof String) {
                if (((String) value).matches(dblRegex)) {
                    newValue = Double.parseDouble((String) value);
                } else {
                    Integer integerValue;
                    try {
                        integerValue = Integer.parseInt((String) value);
                    } catch (final NumberFormatException e) {
                        throw new RuntimeException("Invalid argument type!");
                    }
                    newValue = new Integer(integerValue);
                }
            } else if (value instanceof Double) {
                newValue = new Double(((Double) value).doubleValue());
            } else if (value instanceof Integer) {
                newValue = new Integer(((Integer) value).intValue());
            } else {
                throw new RuntimeException("Invalid argument type!");
            }
        }
        return returnRightType(newValue, operationType);
    }

    /**
     * This method determines which type should be result of operation ( {@link Integer} or
     * {@link Double} ).
     *
     * @param newValue value for which type is determined
     * @param operationType {@link OperationType} which determines operation
     * @return new instance of Double or Integer
     */
    private Object returnRightType(final Object newValue, final OperationType operationType) {
        if (operationType == OperationType.DOUBLE) {
            if (newValue instanceof Double) {
                return newValue;
            } else {
                return new Double(((Integer) newValue).doubleValue());
            }
        } else {
            if (newValue instanceof Integer) {
                return newValue;
            } else {
                return new Integer(((Double) newValue).intValue());
            }
        }
    }
}

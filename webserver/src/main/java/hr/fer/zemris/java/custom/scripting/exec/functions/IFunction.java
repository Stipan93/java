package hr.fer.zemris.java.custom.scripting.exec.functions;

import hr.fer.zemris.java.custom.scripting.exec.SmartScriptEngine;

import java.util.Stack;

/**
 * <p>
 * This interface makes contract with classes which implements it and it provides method which needs
 * to implement algorithm for specific function that {@link SmartScriptEngine} supports.
 * </p>
 * Concrete strategy class needs to implement method {@link #accept(Stack)} in its appropriate way.
 *
 *
 * @author Stipan Mikulić
 * @version 1.0
 */
public interface IFunction {
    /**
     * This method executes defined {@link IFunction}.
     *
     * @param stack
     *            {@link Stack} where result and operands are stored
     */
    void accept(Stack<Object> stack);
}

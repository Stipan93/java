package hr.fer.zemris.java.custom.scripting.tokens;

/**
 * Represents a integer constant.
 *
 * @author Stipan Mikulić
 * @version 1.0
 */
public class TokenConstantInteger extends Token {
    /**
     * {@link TokenConstantInteger} value.
     */
    private final int value;

    /**
     * Creates {@link TokenConstantInteger} with given value.
     *
     * @param value {@link TokenConstantInteger} value
     */
    public TokenConstantInteger(final int value) {
        super();
        this.value = value;
    }

    /**
     * Returns {@link TokenConstantInteger} value.
     *
     * @return {@link TokenConstantInteger} value
     */
    public int getValue() {
        return value;
    }

    /**
     * Returns {@link String} representation of {@link TokenConstantInteger}.
     *
     * @return {@link TokenConstantInteger} value
     */
    @Override
    public String asText() {
        return String.valueOf(this.value);
    }

    @Override
    public String toString() {
        return String.valueOf(this.value);
    }

}

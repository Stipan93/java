package hr.fer.zemris.java.custom.scripting.exec;

import hr.fer.zemris.java.custom.scripting.exec.functions.Functions;
import hr.fer.zemris.java.custom.scripting.nodes.DocumentNode;
import hr.fer.zemris.java.custom.scripting.nodes.EchoNode;
import hr.fer.zemris.java.custom.scripting.nodes.ForLoopNode;
import hr.fer.zemris.java.custom.scripting.nodes.INodeVisitor;
import hr.fer.zemris.java.custom.scripting.nodes.Node;
import hr.fer.zemris.java.custom.scripting.nodes.TextNode;
import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParser;
import hr.fer.zemris.java.custom.scripting.tokens.Token;
import hr.fer.zemris.java.custom.scripting.tokens.TokenConstantDouble;
import hr.fer.zemris.java.custom.scripting.tokens.TokenConstantInteger;
import hr.fer.zemris.java.custom.scripting.tokens.TokenFunction;
import hr.fer.zemris.java.custom.scripting.tokens.TokenOperator;
import hr.fer.zemris.java.custom.scripting.tokens.TokenString;
import hr.fer.zemris.java.custom.scripting.tokens.TokenVariable;
import hr.fer.zemris.java.webserver.RequestContext;

import java.io.IOException;
import java.util.Stack;

/**
 * <p>
 * This class represents engine for execution of documents parsed by {@link SmartScriptParser}.
 * </p>
 *
 * @author Stipan Mikulić
 * @version 1.0
 * @see SmartScriptParser
 * @see RequestContext
 */
public class SmartScriptEngine {
    /**
     * Root node of parsed document.
     */
    private final DocumentNode documentNode;
    /**
     * {@link RequestContext} object.
     */
    private final RequestContext requestContext;
    /**
     * {@link Functions} object that contains all supported functions.
     */
    private final Functions functions;
    /**
     * {@link ObjectMultistack} for storing variables and their values.
     */
    private final ObjectMultistack multistack = new ObjectMultistack();
    /**
     * <p>
     * This anonymous class implements {@link INodeVisitor} methods.
     * </p>
     * It implements methods for visiting nodes in a manner that it simulates execution of program
     * that has given document structure.
     *
     * @see INodeVisitor
     */
    private final INodeVisitor visitor = new INodeVisitor() {

        @Override
        public void visitTextNode(final TextNode node) {
            try {
                requestContext.write(node.getText());
            } catch (final IOException e) {
                System.err.println("Error while writing textNode!");
                System.exit(1);
            }
        }

        @Override
        public void visitForLoopNode(final ForLoopNode node) {
            final String variableName = node.getVariable().getName();

            final Object startValue = getRightInstance(node.getStartExpression().asText());
            multistack.push(variableName, new ValueWrapper(startValue));
            final Object stepValue = getRightInstance(node.getStepExpression().asText());
            final Object finalValue = getRightInstance(node.getEndExpression().asText());
            while (conditionTrue(multistack.peek(variableName).getValue(), finalValue)) {
                for (int i = 0, size = node.numberOfChildren(); i < size; i++) {
                    node.getChild(i).accept(visitor);
                }
                multistack.peek(variableName).increment(stepValue);
            }
            multistack.pop(variableName);
        }

        /**
         * <p>
         * This method returns appropriate object for value defined in {@link ForLoopNode}.
         * </p>
         * If value can be interpreted as {@link Integer} then method returns {@link Integer}
         * object, otherwise {@link Double}.
         *
         *
         * @param asText
         *            {@link String} representation of value
         * @return appropriate object for given value
         */
        private Object getRightInstance(final String asText) {
            try {
                return Integer.parseInt(asText);
            } catch (final Exception e) {
                return Double.parseDouble(asText);
            }
        }

        /**
         * This method checks is {@link ForLoopNode} condition satisfied.
         *
         * @param tempValue
         *            current value for every iteration in {@link ForLoopNode}
         * @param finalValue
         *            end value for {@link ForLoopNode}
         * @return <code>true</code> if condition is satisfied, <code>false</code> otherwise
         */
        private boolean conditionTrue(final Object tempValue, final Object finalValue) {
            final Double arg1;
            if ((tempValue instanceof Integer)) {
                arg1 = ((Integer) tempValue).doubleValue();
            } else {
                arg1 = (Double) tempValue;
            }

            final Double arg2;
            if ((finalValue instanceof Integer)) {
                arg2 = ((Integer) finalValue).doubleValue();
            } else {
                arg2 = (Double) finalValue;
            }
            if (arg1 <= arg2) {
                return true;
            }
            return false;
        }

        @Override
        public void visitEchoNode(final EchoNode node) {
            final Stack<Object> tempStack = new Stack<Object>();
            for (final Token token : node.getTokens()) {
                if (token instanceof TokenConstantDouble) {
                    tempStack.push(((TokenConstantDouble) token).getValue());
                } else if (token instanceof TokenConstantInteger) {
                    tempStack.push(((TokenConstantInteger) token).getValue());
                } else if (token instanceof TokenString) {
                    tempStack.push(((TokenString) token).getValue());
                } else if (token instanceof TokenFunction) {
                    functions.getFunction(((TokenFunction) token).getName()).accept(tempStack);
                } else if (token instanceof TokenOperator) {
                    processTokenOperator(tempStack, (TokenOperator) token);
                } else if (token instanceof TokenVariable) {
                    final Object value = multistack.peek(token.asText()).getValue();
                    tempStack.push(value);
                }
            }
            printStack(tempStack);
        }

        /**
         * This method prints objects left on {@link Stack} on {@link RequestContext} output stream.
         *
         * @param tempStack
         *            {@link Stack} to print
         */
        private void printStack(final Stack<Object> tempStack) {
            final Stack<Object> helperStack = new Stack<>();
            while (!tempStack.isEmpty()) {
                helperStack.push(tempStack.pop());
            }
            while (!helperStack.isEmpty()) {
                final Object value = helperStack.pop();
                String print;
                if (value instanceof String) {
                    print = (String) value;
                } else {
                    print = String.valueOf(value);
                }
                try {
                    requestContext.write(print);
                } catch (final IOException e) {
                    System.err.println("Error while writing " + "to "
                            + "request context output stream!");
                    System.exit(1);
                }
            }
        }

        /**
         * This method process operations and defines actions that need to be made when
         * {@link TokenOperator} is selected.
         *
         * @param tmpStack
         *            {@link Stack} where values of {@link EchoNode} are stored
         * @param token
         *            {@link TokenOperator} that defines operation
         */
        private void processTokenOperator(final Stack<Object> tmpStack, final TokenOperator token) {
            final ValueWrapper secondOperand;
            secondOperand = new ValueWrapper(tmpStack.pop());
            final ValueWrapper firstOperand = new ValueWrapper(tmpStack.pop());
            final String operator = token.getSymbol();
            if (operator.equals("+")) {
                firstOperand.increment(secondOperand.getValue());
            } else if (operator.equals("-")) {
                firstOperand.decrement(secondOperand.getValue());
            } else if (operator.equals("*")) {
                firstOperand.multiply(secondOperand.getValue());
            } else if (operator.equals("/")) {
                firstOperand.divide(secondOperand.getValue());
            } else {
                throw new IllegalArgumentException("Invalid operator!");
            }
            tmpStack.push(firstOperand.getValue());
        }

        @Override
        public void visitDocumentNode(final DocumentNode node) {
            for (int i = 0, size = node.numberOfChildren(); i < size; i++) {
                node.getChild(i).accept(visitor);
            }
        }
    };

    /**
     * Creates new {@link SmartScriptEngine} that can execute document stored in
     * {@link DocumentNode}.
     *
     * @param documentNode
     *            root {@link Node} of parsed document
     * @param requestContext
     *            {@link RequestContext} object
     */
    public SmartScriptEngine(final DocumentNode documentNode, final RequestContext requestContext) {
        this.documentNode = documentNode;
        this.requestContext = requestContext;
        functions = new Functions(requestContext);
    }

    /**
     * This method executes document stored in engines {@link DocumentNode}.
     */
    public void execute() {
        documentNode.accept(visitor);
    }
}

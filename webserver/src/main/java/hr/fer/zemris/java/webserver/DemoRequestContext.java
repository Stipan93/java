package hr.fer.zemris.java.webserver;

import hr.fer.zemris.java.webserver.RequestContext.RCCookie;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * This class represents demonstration example for {@link RequestContext} usage.
 *
 * @author Stipan Mikulić
 * @version 1.0
 */
public final class DemoRequestContext {
    /**
     * This constructor prevent instantiating.
     */
    private DemoRequestContext() {
        // prevent instantiating
        // checkstyle warning
    }

    /**
     * This method is called when program starts.
     *
     * @param args
     *            command line arguments
     * @throws IOException
     *             if error while writing to output stream occurred
     */
    public static void main(final String[] args) throws IOException {
        demo1("primjer1.txt", "ISO-8859-2");
        demo1("primjer2.txt", "UTF-8");
        demo2("primjer3.txt", "UTF-8");
    }

    /**
     * This method adds appropriate content to {@link RequestContext} object.
     *
     * @param filePath
     *            {@link String} that represents path of file
     * @param encoding
     *            {@link String} that represents encoding for {@link RequestContext}
     * @throws IOException
     *             if error while writing to output stream occurred
     */
    private static void demo1(final String filePath, final String encoding) throws IOException {
        final int statusCode = 205;
        final OutputStream os = Files.newOutputStream(Paths.get(filePath));
        final RequestContext rc =
                new RequestContext(os, new HashMap<String, String>(),
                        new HashMap<String, String>(), new ArrayList<RequestContext.RCCookie>());
        rc.setEncoding(encoding);
        rc.setMimeType("text/plain");
        rc.setStatusCode(statusCode);
        rc.setStatusText("Idemo dalje");
        // Only at this point will header be created and written...
        rc.write("Čevapčići i Šiščevapčići.");
        os.close();
    }

    /**
     * This method adds appropriate content to {@link RequestContext} object.
     *
     * @param filePath
     *            {@link String} that represents path of file
     * @param encoding
     *            {@link String} that represents encoding for {@link RequestContext}
     * @throws IOException
     *             if error while writing to output stream occurred
     */
    private static void demo2(final String filePath, final String encoding) throws IOException {
        final int statusCode = 205;
        final int maxAge = 3600;
        final OutputStream os = Files.newOutputStream(Paths.get(filePath));
        final RequestContext rc =
                new RequestContext(os, new HashMap<String, String>(),
                        new HashMap<String, String>(), new ArrayList<RequestContext.RCCookie>());
        rc.setEncoding(encoding);
        rc.setMimeType("text/plain");
        rc.setStatusCode(statusCode);
        rc.setStatusText("Idemo dalje");
        rc.addRCCookie(new RCCookie("korisnik", "perica", maxAge, "127.0.0.1", "/", false));
        rc.addRCCookie(new RCCookie("zgrada", "B4", null, null, "/", false));
        // Only at this point will header be created and written...
        rc.write("Čevapčići i Šiščevapčići.");
        os.close();
    }
}

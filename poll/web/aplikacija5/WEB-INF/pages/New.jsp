<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
</head>
<body>
	<c:choose>
		<c:when test="${sessionScope.containsKey('current.user.id')}">
			<form action="saveEntry" method="post">
				Title <input type="text" name="title"
					value='<c:out value="${entry.title}"/>' size="30"><br>
				Text <br> ​
				<textarea id="text" name="text" rows="20" cols="40"><c:out
						value="${entry.text}" /></textarea>
				<br> <input type="submit" name="newEntry" value="Submit">
				<input type="submit" name="newEntry" value="Cancel">

			</form>

		</c:when>
		<c:otherwise>
			<h1>Error occurred</h1>
			<p>Access denied!</p>

			<p>
				<a href="/aplikacija5/servleti/main">Home</a>
			</p>
		</c:otherwise>
	</c:choose>
	
</body>
</html>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
	<head>
		<title>Error</title>
	</head>

	<body>
		<h1>Error occurred</h1>
		<p><c:out value="${message}"/></p>

		<p><a href="/aplikacija5/servleti/main">Home</a></p>
	</body>
</html>
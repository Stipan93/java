<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<title>Registration</title>

<style type="text/css">
.error {
	font-weight: bold;
	font-size: 0.9em;
	color: #FF0000;
}
</style>
</head>

<body>
	<h1>Novi kontakt</h1>

	<form action="save" method="post">

		First Name <input type="text" name="firstName"
			value='<c:out value="${record.firstName}"/>' size="30"><br>
		<c:if test="${record.hasError('firstName')}">
			<div class="error">
				<c:out value="${record.getError('firstName')}" />
			</div>
		</c:if>

		Last Name <input type="text" name="lastName"
			value='<c:out value="${record.lastName}"/>' size="50"><br>
		<c:if test="${record.hasError('lastName')}">
			<div class="error">
				<c:out value="${record.getError('lastName')}" />
			</div>
		</c:if>

		Nick <input type="text" name="nick"
			value='<c:out value="${record.nick}"/>' size="50"><br>
		<c:if test="${record.hasError('nick')}">
			<div class="error">
				<c:out value="${record.getError('nick')}" />
			</div>
		</c:if>

		EMail <input type="text" name="email"
			value='<c:out value="${record.email}"/>' size="100"><br>
		<c:if test="${record.hasError('email')}">
			<div class="error">
				<c:out value="${record.getError('email')}" />
			</div>
		</c:if>

		Password <input type="password" name="password"
			value='<c:out value="${record.password}"/>' size="100"><br>
		<c:if test="${record.hasError('password')}">
			<div class="error">
				<c:out value="${record.getError('password')}" />
			</div>
		</c:if>

		<c:if test="${record == null}">
			<div class="error">
				<c:out value="User with given nick alerady exists!" />
			</div>
		</c:if>

		<input type="submit" name="register" value="Submit"> <input
			type="submit" name="register" value="Cancel">

	</form>

</body>
</html>
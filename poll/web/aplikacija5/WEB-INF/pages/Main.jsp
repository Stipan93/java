<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<title>Login</title>

<style type="text/css">
.error {
	font-weight: bold;
	font-size: 0.9em;
	color: #FF0000;
}
</style>
<c:choose>
	<c:when test="${sessionScope.containsKey('current.user.id')}">
		<p>Logged in as: ${sessionScope.get('current.user.fn')}
			${sessionScope.get('current.user.ln')}</p>
		<a href="/aplikacija5/servleti/logout">Logout</a>
		<br>
	</c:when>
	<c:otherwise>
		<p>Not loged in!</p>
	</c:otherwise>
</c:choose>

</head>
<body>
	<c:if test="${!sessionScope.containsKey('current.user.id')}">
		<form action="/aplikacija5/servleti/login" method="post">

			Nick <input type="text" name="nick"
				value='<c:out value="${record.nick}"/>' size="30"><br>
			<c:if test="${record.hasError('nick')}">
				<div class="error">
					<c:out value="${record.getError('nick')}" />
				</div>
			</c:if>

			<c:if test="${record == null}">
				<div class="error">
					<c:out value="User with given nick don't exists!" />
				</div>
			</c:if>

			Password <input type="password" name="password"
				value='<c:out value="${record.password}"/>' size="30"><br>
			<c:if test="${record.hasError('password')}">
				<div class="error">
					<c:out value="${record.getError('password')}" />
				</div>
			</c:if>

			<c:if test="${badPassword}">
				<div class="error">
					<c:out value="Invalid password!" />
				</div>
			</c:if>

			<input type="submit" name="login" value="Submit">

		</form>
		<a href="/aplikacija5/servleti/register">Register</a>
		<br>
	</c:if>

	<p>Authors:</p>
	<c:forEach var="author" items="${ authors }">
		<a href="/aplikacija5/servleti/author/${ author.nick }">${ author.nick }</a>
		<br>
	</c:forEach>
</body>
</html>
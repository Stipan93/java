<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<title>Login</title>
</head>
<body>
	<p>Entries:</p>
	<c:forEach var="entry" items="${ blogEntries }">
		<a href="${ entry.id }">${ entry.title }</a>
		<br>
	</c:forEach>
    <hr>
	<c:if test="${sessionScope.containsKey('current.user.nick')}">
		<a href="/aplikacija5/servleti/author/${sessionScope.get('current.user.nick')}/new">New</a>
	</c:if>
	
	<p><a href="/aplikacija5/servleti/main">Home</a></p>
</body>
</html>
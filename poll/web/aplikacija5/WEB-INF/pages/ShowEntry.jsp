<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
</head>
<body>
	<p>Blog entry:</p>
	<p>${ blogEntry.id }</p>
	<h1>${ blogEntry.title }</h1>
	<p>${ blogEntry.text }</p>
	<c:if test="${sessionScope.containsKey('current.user.id')}">
		<form
			action="/aplikacija5/servleti/author/${sessionScope.get('current.user.nick')}/edit"
			method="post">
			<input type="hidden" name="entryId" value="${ blogEntry.id }">
			<input type="submit" name="editEntry" value="Edit">
		</form>
	</c:if>
	<p>
        <a href="/aplikacija5/servleti/author/${sessionScope.get('current.user.nick')}">Back to entries</a>
    </p>
	<hr>
	<p>Comments:</p>
	<hr>
	<c:forEach var="comment" items="${ comments }">
		<p>
			<b>User email: ${ comment.usersEMail }</b>
		</p>
		<p>${ comment.message }</p>
		<p>
			<b>Posted on - ${ comment.postedOn }</b>
		</p>
		<hr>
	</c:forEach>
	<form action="addcomment" method="post">
		Enter comment <br> 
		<input type="hidden" name="entryId" value="${ blogEntry.id }">​
		<textarea id="text" name="message" rows="10" cols="30"><c:out
				value="${entry.text}" /></textarea>
		<br> <input type=submit name="submitComment" value="Submit">

	</form>
	<p>
		<a href="/aplikacija5/servleti/main">Home</a>
	</p>
</body>
</html>
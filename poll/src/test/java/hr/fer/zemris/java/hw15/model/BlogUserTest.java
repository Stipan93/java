package hr.fer.zemris.java.hw15.model;

import org.junit.Assert;
import org.junit.Test;

public class BlogUserTest {

    @Test
    public void BasicTest() {
        final BlogUser user = new BlogUser();
        user.setId(new Long(2));
        user.setFirstName("Ante");
        user.setLastName("Antic");
        user.setNick("Antisa");
        user.setEmail("ante.antic@fer.hr");
        Assert.assertTrue(2 == user.getId());
        Assert.assertEquals("Ante", user.getFirstName());
        Assert.assertEquals("Antic", user.getLastName());
        Assert.assertEquals("Antisa", user.getNick());
        Assert.assertEquals("ante.antic@fer.hr", user.getEmail());
    }

}

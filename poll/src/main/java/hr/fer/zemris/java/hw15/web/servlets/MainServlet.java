package hr.fer.zemris.java.hw15.web.servlets;

import hr.fer.zemris.java.hw15.dao.DAOProvider;
import hr.fer.zemris.java.hw15.model.BlogUser;
import hr.fer.zemris.java.hw15.model.FormularForm;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This class represents {@link HttpServlet} that prepares data for main page for this blog.
 *
 * @author Stipan Mikulić
 * @version 1.0
 */
@WebServlet("/servleti/main")
public class MainServlet extends HttpServlet {

    /**
     * Default serial ID.
     */
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp)
            throws ServletException, IOException {
        final BlogUser user = new BlogUser();
        final FormularForm form = new FormularForm();
        form.fillFromRecord(user);

        final List<BlogUser> authors = DAOProvider.getDAO().getBlogUsers();
        req.setAttribute("record", form);
        req.getSession().setAttribute("authors", authors);
        req.getRequestDispatcher("/WEB-INF/pages/Main.jsp").forward(req, resp);
    }

}

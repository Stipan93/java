package hr.fer.zemris.java.hw15.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

/**
 * This class represents blog's user.
 *
 * @author Stipan Mikulić
 * @version 1.0
 */
@Entity
@Table(name = "blog_user")
@Cacheable(true)
public class BlogUser {
    /**
     * User's id.
     */
    private Long id;
    /**
     * User's first name.
     */
    private String firstName;
    /**
     * User's last name.
     */
    private String lastName;
    /**
     * User's nick.
     */
    private String nick;
    /**
     * User's email.
     */
    private String email;
    /**
     * User's password hash.
     */
    private String passwordHash;
    /**
     * User's blog entries.
     */
    private List<BlogEntry> entries = new ArrayList<>();


    /**
     * User's blog entries getter.
     *
     * @return {@link List} of {@link BlogEntry} objects.
     */
    @OneToMany(mappedBy = "creator", fetch = FetchType.LAZY, cascade = CascadeType.PERSIST,
            orphanRemoval = true)
    @OrderBy("id")
    public List<BlogEntry> getEntries() {
        return entries;
    }

    /**
     * User's blog entries setter.
     *
     * @param entries
     *            {@link BlogEntry} objects to set.
     */
    public void setEntries(final List<BlogEntry> entries) {
        this.entries = entries;
    }

    /**
     * User's id getter.
     *
     * @return user's id
     */
    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    /**
     * User's id setter.
     *
     * @param id
     *            id to set
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * User's first name getter.
     *
     * @return user's first name
     */
    @Column(nullable = false, length = 50)
    public String getFirstName() {
        return firstName;
    }

    /**
     * User's first name setter.
     *
     * @param firstName
     *            first name to set
     */
    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    /**
     * User's last name getter.
     *
     * @return user's last name
     */
    @Column(nullable = false, length = 50)
    public String getLastName() {
        return lastName;
    }

    /**
     * User's last name setter.
     *
     * @param lastName
     *            last name to set
     */
    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    /**
     * User's nick getter.
     *
     * @return user's nick
     */
    @Column(unique = true, length = 50)
    public String getNick() {
        return nick;
    }

    /**
     * User's nick setter.
     *
     * @param nick
     *            nick to set
     */
    public void setNick(final String nick) {
        this.nick = nick;
    }

    /**
     * User's email getter.
     *
     * @return user's email
     */
    @Column(nullable = false, length = 50)
    public String getEmail() {
        return email;
    }

    /**
     * User's email setter.
     *
     * @param email
     *            email to set
     */
    public void setEmail(final String email) {
        this.email = email;
    }

    /**
     * User's password hash getter.
     *
     * @return user's password hash
     */
    @Column(nullable = false, length = 100)
    public String getPasswordHash() {
        return passwordHash;
    }

    /**
     * User's password hash setter.
     *
     * @param passwordHash
     *            password hash to set
     */
    public void setPasswordHash(final String passwordHash) {
        this.passwordHash = passwordHash;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((nick == null) ? 0 : nick.hashCode());
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BlogUser other = (BlogUser) obj;
        if (id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!id.equals(other.id)) {
            return false;
        }
        if (nick == null) {
            if (other.nick != null) {
                return false;
            }
        } else if (!nick.equals(other.nick)) {
            return false;
        }
        return true;
    }


}

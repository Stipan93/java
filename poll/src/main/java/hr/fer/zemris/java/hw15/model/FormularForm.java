package hr.fer.zemris.java.hw15.model;


import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

/**
 * This class represents storage for information that user enters in registration form.
 *
 * @author Stipan Mikulić
 * @version 1.0
 */
public class FormularForm {

    /**
     * User's is.
     */
    private String id;
    /**
     * User's last name.
     */
    private String lastName;
    /**
     * User's first name.
     */
    private String firstName;
    /**
     * User's nick.
     */
    private String nick;
    /**
     * User's email.
     */
    private String email;
    /**
     * User's password.
     */
    private String password;

    /**
     * Storage for errors that occurred while filling registration form.
     */
    private final Map<String, String> errors = new HashMap<>();

    /**
     * Creates new {@link FormularForm}.
     */
    public FormularForm() {

    }

    /**
     * This method gets error that is mapped by given {@link String} argument.
     *
     * @param name
     *            name of error
     * @return message about error
     */
    public String getError(final String name) {
        return errors.get(name);
    }

    /**
     * Checks are there any errors in form.
     *
     * @return <code>true</code> if there is no errors, <code>false</code> otherwise
     */
    public boolean hasErrors() {
        return !errors.isEmpty();
    }

    /**
     * Checks is there specified error in form.
     *
     * @param name
     *            name of error
     * @return <code>true</code> if there is specified error, <code>false</code> otherwise
     */
    public boolean hasError(final String name) {
        return errors.containsKey(name);
    }

    /**
     * This method gets data from {@link HttpServletRequest} .
     *
     * @param req
     *            {@link HttpServletRequest} object
     */
    public void fillFromHttpRequest(final HttpServletRequest req) {
        id = prepare(req.getParameter("id"));
        firstName = prepare(req.getParameter("firstName"));
        lastName = prepare(req.getParameter("lastName"));
        nick = prepare(req.getParameter("nick"));
        email = prepare(req.getParameter("email"));
        password = prepare(req.getParameter("password"));
    }

    /**
     * This method gets data from {@link BlogUser}.
     *
     * @param user
     *            {@link BlogUser} object
     */
    public void fillFromRecord(final BlogUser user) {
        if (user.getId() == null) {
            id = "";
        } else {
            id = user.getId().toString();
        }

        firstName = user.getFirstName();
        lastName = user.getLastName();
        nick = user.getNick();
        email = user.getEmail();
        password = user.getPasswordHash();
    }

    /**
     * This method gives data to given {@link BlogUser}.
     *
     * @param user
     *            {@link BlogUser} object
     */
    public void fillRecord(final BlogUser user) {
        if (id.isEmpty()) {
            user.setId(null);
        } else {
            user.setId(Long.valueOf(id));
        }

        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setNick(nick);
        user.setEmail(email);
        user.setPasswordHash(password);
    }

    /**
     * This method validate filled form.
     */
    public void validate() {
        errors.clear();

        if (!id.isEmpty()) {
            try {
                Long.parseLong(id);
            } catch (final NumberFormatException ex) {
                errors.put("id", "Invalid ID.");
            }
        }

        if (firstName.isEmpty()) {
            errors.put("firstName", "First name is required!");
        } else if (firstName.matches(".*\\d+.*")) {
            errors.put("firstName", "First name cannot contain digits!");
        }

        if (lastName.isEmpty()) {
            errors.put("lastName", "Last name is required!");
        } else if (lastName.matches(".*\\d+.*")) {
            errors.put("lastName", "Last name cannot contain digits!");
        }

        if (nick.isEmpty()) {
            errors.put("nick", "Nick is required!");
        }

        if (email.isEmpty()) {
            errors.put("email", "EMail is required!");
        } else {
            final int l = email.length();
            final int p = email.indexOf('@');
            if (l < 3 || p == -1 || p == 0 || p == l - 1) {
                errors.put("email", "Invalid EMail.");
            }
        }

        if (password.isEmpty()) {
            errors.put("password", "Password is required!");
        } else {
            final int len = password.length();
            if (len < 6) {
                errors.put("password", "Password must have at least 6 characters.");
            }
        }
    }

    /**
     * This method prepares data for print.
     *
     * @param s
     *            {@link String} to prepare
     * @return prepared {@link String}
     */
    private String prepare(final String s) {
        if (s == null) {
            return "";
        }
        return s.trim();
    }

    /**
     * User's id getter.
     *
     * @return user's id
     */
    public String getId() {
        return id;
    }

    /**
     * User's id setter.
     *
     * @param id
     *            id to set
     */
    public void setId(final String id) {
        this.id = id;
    }

    /**
     * User's last name getter.
     *
     * @return user's last name
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * User's last name setter.
     *
     * @param lastName
     *            last name to set
     */
    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    /**
     * User's first name getter.
     *
     * @return user's first name
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * User's first name setter.
     *
     * @param firstName
     *            first name to set
     */
    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    /**
     * User's nick getter.
     *
     * @return user's nick
     */
    public String getNick() {
        return nick;
    }

    /**
     * User's nick setter.
     *
     * @param nick
     *            nick to set
     */
    public void setNick(final String nick) {
        this.nick = nick;
    }

    /**
     * User's email getter.
     *
     * @return user's email
     */
    public String getEmail() {
        return email;
    }

    /**
     * User's email setter.
     *
     * @param email
     *            email to set
     */
    public void setEmail(final String email) {
        this.email = email;
    }

    /**
     * User's password hash getter.
     *
     * @return user's password hash
     */
    public String getPassword() {
        return password;
    }

    /**
     * User's password hash setter.
     *
     * @param password
     *            password hash to set
     */
    public void setPassword(final String password) {
        this.password = password;
    }

}

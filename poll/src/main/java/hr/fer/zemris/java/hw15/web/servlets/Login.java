package hr.fer.zemris.java.hw15.web.servlets;

import hr.fer.zemris.java.hw15.dao.DAOProvider;
import hr.fer.zemris.java.hw15.model.BlogUser;
import hr.fer.zemris.java.hw15.model.CryptoUtil;
import hr.fer.zemris.java.hw15.model.FormularForm;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This class represents {@link HttpServlet} that process login data.
 *
 * @author Stipan Mikulić
 * @version 1.0
 */
@WebServlet("/servleti/login")
public class Login extends HttpServlet {
    /**
     * Default serial ID.
     */
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp)
            throws ServletException, IOException {
        obradi(req, resp);
    }

    @Override
    protected void doPost(final HttpServletRequest req, final HttpServletResponse resp)
            throws ServletException, IOException {
        obradi(req, resp);
    }

    /**
     * This method process given data from login process.
     *
     * @param req
     *            an HttpServletRequest object that contains the request the client has made of the
     *            servlet
     * @param resp
     *            an HttpServletResponse object that contains the response the servlet sends to the
     *            client
     * @throws ServletException
     *             if the request for the POST could not be handled
     * @throws IOException
     *             if an input or output error is detected when the servlet handles the request
     */
    protected void obradi(final HttpServletRequest req, final HttpServletResponse resp)
            throws ServletException, IOException {

        req.setCharacterEncoding("UTF-8");
        final String method = req.getParameter("login");
        if (!"Submit".equals(method)) {
            req.getRequestDispatcher("/WEB-INF/pages/Main.jsp").forward(req, resp);
            return;
        }
        final FormularForm form = new FormularForm();

        final String nick = req.getParameter("nick");
        String password = null;
        try {
            password = CryptoUtil.getDigest(req.getParameter("password"));
        } catch (final NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        final BlogUser user = DAOProvider.getDAO().getBlogUser(nick);

        if (user == null) {
            req.setAttribute("record", null);
        } else {
            form.fillFromRecord(user);
            form.validate();
            if (user.getPasswordHash().equals(password)) {
                req.getSession().setAttribute("current.user.id", user.getId());
                req.getSession().setAttribute("current.user.fn", user.getFirstName());
                req.getSession().setAttribute("current.user.ln", user.getLastName());
                req.getSession().setAttribute("current.user.nick", user.getNick());
                req.setAttribute("badPassword", false);
                req.setAttribute("record", form);
            } else {
                form.setPassword("");
                req.setAttribute("record", form);
                req.setAttribute("badPassword", true);
            }
        }
        req.getRequestDispatcher("/WEB-INF/pages/Main.jsp").forward(req, resp);
    }
}

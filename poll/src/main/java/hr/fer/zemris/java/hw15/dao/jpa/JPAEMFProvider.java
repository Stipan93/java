package hr.fer.zemris.java.hw15.dao.jpa;

import javax.persistence.EntityManagerFactory;

/**
 * This class represents entity manager factory provider. It provides methods for getting and
 * setting {@link EntityManagerFactory}.
 *
 * @author Stipan Mikulić
 * @version 1.0
 */
public class JPAEMFProvider {

    /**
     * {@link EntityManagerFactory} object.
     */
    public static EntityManagerFactory emf;

    /**
     * {@link EntityManagerFactory} object getter.
     *
     * @return {@link EntityManagerFactory} object
     */
    public static EntityManagerFactory getEmf() {
        return emf;
    }

    /**
     * {@link EntityManagerFactory} setter.
     *
     * @param emf
     *            {@link EntityManagerFactory} object to set
     */
    public static void setEmf(final EntityManagerFactory emf) {
        JPAEMFProvider.emf = emf;
    }
}

package hr.fer.zemris.java.hw15.dao;

import hr.fer.zemris.java.hw15.dao.jpa.JPADAOImpl;

/**
 * Singleton class that provides method for getting {@link DAO}.
 *
 * @author Stipan Mikulić
 * @version 1.0
 */
public class DAOProvider {
    /**
     * Storage for {@link DAO} object.
     */
    private static DAO dao = new JPADAOImpl();

    /**
     * {@link DAO} getter.
     *
     * @return {@link DAO} object
     */
    public static DAO getDAO() {
        return dao;
    }

}

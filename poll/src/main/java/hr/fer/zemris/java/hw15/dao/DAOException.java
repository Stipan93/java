package hr.fer.zemris.java.hw15.dao;

/**
 * {@link Exception} that is thrown if error while getting data via {@link DAO} occurred.
 *
 * @author Stipan Mikulić
 * @version 1.0
 */
public class DAOException extends RuntimeException {

    /**
     * Default serial ID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Exception with message and cause.
     *
     * @param message
     *            message for {@link DAOException}
     * @param cause
     *            cause of {@link DAOException}
     */
    public DAOException(final String message, final Throwable cause) {
        super(message, cause);
    }

    /**
     * Exception with message.
     *
     * @param message
     *            message for {@link DAOException}
     */
    public DAOException(final String message) {
        super(message);
    }
}

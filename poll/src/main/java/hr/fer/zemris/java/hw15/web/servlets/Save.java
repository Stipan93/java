package hr.fer.zemris.java.hw15.web.servlets;


import hr.fer.zemris.java.hw15.dao.DAOProvider;
import hr.fer.zemris.java.hw15.dao.jpa.JPAEMProvider;
import hr.fer.zemris.java.hw15.model.BlogUser;
import hr.fer.zemris.java.hw15.model.CryptoUtil;
import hr.fer.zemris.java.hw15.model.FormularForm;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import javax.persistence.EntityManager;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This class represents {@link HttpServlet} that process data from user registration and stores it
 * in database.
 *
 * @author Stipan Mikulić
 * @version 1.0
 */
@WebServlet("/servleti/save")
public class Save extends HttpServlet {

    /**
     * Default serial ID.
     */
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp)
            throws ServletException, IOException {
        obradi(req, resp);
    }

    @Override
    protected void doPost(final HttpServletRequest req, final HttpServletResponse resp)
            throws ServletException, IOException {
        obradi(req, resp);
    }

    /**
     * This method process given data from registration.
     *
     * @param req
     *            an HttpServletRequest object that contains the request the client has made of the
     *            servlet
     * @param resp
     *            an HttpServletResponse object that contains the response the servlet sends to the
     *            client
     * @throws ServletException
     *             if the request for the POST could not be handled
     * @throws IOException
     *             if an input or output error is detected when the servlet handles the request
     */
    protected void obradi(final HttpServletRequest req, final HttpServletResponse resp)
            throws ServletException, IOException {

        req.setCharacterEncoding("UTF-8");

        final String method = req.getParameter("register");
        if (!"Submit".equals(method)) {
            resp.sendRedirect(req.getServletContext().getContextPath() + "/servleti/main");
            return;
        }

        final FormularForm f = new FormularForm();
        f.fillFromHttpRequest(req);
        f.validate();

        if (f.hasErrors()) {
            req.setAttribute("record", f);
            req.getRequestDispatcher("/WEB-INF/pages/Registration.jsp").forward(req, resp);
            return;
        }

        BlogUser user = DAOProvider.getDAO().getBlogUser(f.getNick());
        if (user == null) {
            final EntityManager em = JPAEMProvider.getEntityManager();

            user = new BlogUser();
            f.fillRecord(user);
            try {
                user.setPasswordHash(CryptoUtil.getDigest(f.getPassword()));
            } catch (final NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
            em.persist(user);

            req.getSession().setAttribute("current.user.id", user.getId());
            req.getSession().setAttribute("current.user.fn", user.getFirstName());
            req.getSession().setAttribute("current.user.ln", user.getLastName());
            req.getSession().setAttribute("current.user.nick", user.getNick());

            resp.sendRedirect(req.getServletContext().getContextPath() + "/servleti/main");
        } else {
            req.setAttribute("record", null);
            req.getRequestDispatcher("/WEB-INF/pages/Registration.jsp").forward(req, resp);
        }
    }
}

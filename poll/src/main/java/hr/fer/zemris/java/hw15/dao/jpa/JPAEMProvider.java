package hr.fer.zemris.java.hw15.dao.jpa;

import hr.fer.zemris.java.hw15.dao.DAOException;

import javax.persistence.EntityManager;

/**
 * This class represents JPA {@link EntityManager} provider.
 *
 * @author Stipan Mikulić
 * @version 1.0
 */
public class JPAEMProvider {

    /**
     * Storage for {@link LocalData} objects which contains {@link EntityManager} that provides
     * connection to database.
     */
    private static ThreadLocal<LocalData> locals = new ThreadLocal<>();

    /**
     * {@link EntityManager} getter.
     *
     * @return {@link EntityManager} object
     */
    public static EntityManager getEntityManager() {
        LocalData ldata = locals.get();
        if (ldata == null) {
            ldata = new LocalData();
            ldata.em = JPAEMFProvider.getEmf().createEntityManager();
            ldata.em.getTransaction().begin();
            locals.set(ldata);
        }
        return ldata.em;
    }

    /**
     * This method closes connection to database and commits transaction.
     *
     * @throws DAOException
     *             if error while committing transaction occurred or while closing
     *             {@link EntityManager}.
     */
    public static void close() throws DAOException {
        final LocalData ldata = locals.get();
        if (ldata == null) {
            return;
        }
        DAOException dex = null;
        try {
            ldata.em.getTransaction().commit();
        } catch (final Exception ex) {
            dex = new DAOException("Unable to commit transaction.", ex);
        }
        try {
            ldata.em.close();
        } catch (final Exception ex) {
            if (dex != null) {
                dex = new DAOException("Unable to close entity manager.", ex);
            }
        }
        locals.remove();
        if (dex != null) {
            throw dex;
        }
    }

    /**
     * This class represents storage for {@link EntityManager} that provides connection to database.
     *
     * @author Stipan Mikulić
     * @version 1.0
     */
    private static class LocalData {
        /**
         * {@link EntityManager} object.
         */
        EntityManager em;
    }

}

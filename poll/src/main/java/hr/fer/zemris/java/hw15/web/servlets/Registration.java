package hr.fer.zemris.java.hw15.web.servlets;

import hr.fer.zemris.java.hw15.model.BlogUser;
import hr.fer.zemris.java.hw15.model.FormularForm;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * This class represents {@link HttpServlet} that prepares objects for user registration.
 *
 * @author Stipan Mikulić
 * @version 1.0
 */
@WebServlet("/servleti/register")
public class Registration extends HttpServlet {

    /**
     * Default serial ID.
     */
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp)
            throws ServletException, IOException {
        final BlogUser user = new BlogUser();
        final FormularForm form = new FormularForm();
        form.fillFromRecord(user);

        req.setAttribute("record", form);
        req.getRequestDispatcher("/WEB-INF/pages/Registration.jsp").forward(req, resp);
    }

}

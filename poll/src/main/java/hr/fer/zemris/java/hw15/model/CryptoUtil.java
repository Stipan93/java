package hr.fer.zemris.java.hw15.model;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.xml.bind.DatatypeConverter;

/**
 * This class provides method for password encryption on blog.
 *
 * @author Stipan Mikulić
 * @version 1.0
 */
public class CryptoUtil {
    /**
     * Returns digest for {@link String} given as argument.
     *
     * @param password
     *            password for which digest is calculated
     * @return calculated digest
     * @throws NoSuchAlgorithmException
     *             if no Provider supports a MessageDigestSpi implementation for the specified
     *             algorithm.
     */
    public static String getDigest(final String password) throws IOException,
            NoSuchAlgorithmException {
        final byte[] passwordBytes = password.getBytes(StandardCharsets.UTF_8);
        final MessageDigest digestAlgorithm = MessageDigest.getInstance("SHA-1");
        digestAlgorithm.update(passwordBytes, 0, passwordBytes.length);
        return DatatypeConverter.printHexBinary(digestAlgorithm.digest()).toLowerCase();
    }
}

package hr.fer.zemris.java.hw15.dao.jpa;

import hr.fer.zemris.java.hw15.dao.DAO;
import hr.fer.zemris.java.hw15.dao.DAOException;
import hr.fer.zemris.java.hw15.model.BlogComment;
import hr.fer.zemris.java.hw15.model.BlogEntry;
import hr.fer.zemris.java.hw15.model.BlogUser;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 * This class implements {@link DAO} interface and provides methods for getting data from database.
 *
 * @author Stipan Mikulić
 * @version 1.0
 */
public class JPADAOImpl implements DAO {

    @Override
    public BlogEntry getBlogEntry(final Long id) throws DAOException {
        final BlogEntry blogEntry = JPAEMProvider.getEntityManager().find(BlogEntry.class, id);
        return blogEntry;
    }


    @Override
    public BlogUser getBlogUser(final String nick) throws DAOException {
        @SuppressWarnings("unchecked")
        final List<BlogUser> users =
                JPAEMProvider.getEntityManager()
                        .createQuery("SELECT b FROM BlogUser as b WHERE b.nick=:n")
                        .setParameter("n", nick).getResultList();
        return users.isEmpty() ? null : users.get(0);
    }


    @Override
    public List<BlogUser> getBlogUsers() throws DAOException {
        final EntityManager em = JPAEMProvider.getEntityManager();
        final CriteriaBuilder builder = em.getCriteriaBuilder();
        final CriteriaQuery<BlogUser> query = builder.createQuery(BlogUser.class);
        final Root<BlogUser> blogUsersRoot = query.from(BlogUser.class);
        query.select(blogUsersRoot);
        return em.createQuery(query).getResultList();
    }

    @Override
    public List<BlogEntry> getBlogEntries(final BlogUser creator) throws DAOException {
        final EntityManager em = JPAEMProvider.getEntityManager();
        @SuppressWarnings("unchecked")
        final List<BlogEntry> entries =
                em.createQuery("SELECT b FROM BlogEntry as b WHERE b.creator=:c")
                        .setParameter("c", creator).getResultList();
        return entries;
    }

    @Override
    public List<BlogComment> getBlogComments(final BlogEntry blogEntry) {
        final EntityManager em = JPAEMProvider.getEntityManager();
        @SuppressWarnings("unchecked")
        final List<BlogComment> comments =
                em.createQuery("SELECT c FROM BlogComment as c WHERE c.blogEntry=:be")
                        .setParameter("be", blogEntry).getResultList();
        return comments;
    }
}

package hr.fer.zemris.java.hw15.model;

import java.util.Date;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * This class represents blog's comment.
 *
 * @author Stipan Mikulić
 * @version 1.0
 */
@Entity
@Table(name = "blog_comments")
@Cacheable(true)
public class BlogComment {
    /**
     * Comment id.
     */
    private Long id;
    /**
     * {@link BlogEntry} that is parent for this {@link BlogComment}.
     */
    private BlogEntry blogEntry;
    /**
     * EMail of user who write this {@link BlogComment}.
     */
    private String usersEMail;
    /**
     * Text of {@link BlogComment}.
     */
    private String message;
    /**
     * {@link Date} when {@link BlogComment} was posted.
     */
    private Date postedOn;

    /**
     * Comment's id getter.
     *
     * @return comment's id.
     */
    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    /**
     * Comment's id setter.
     *
     * @param id
     *            id to set.
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Comment's parent {@link BlogEntry} getter.
     *
     * @return comment's parent {@link BlogEntry}
     */
    @ManyToOne
    @JoinColumn(nullable = false)
    public BlogEntry getBlogEntry() {
        return blogEntry;
    }

    /**
     * Comment's {@link BlogEntry} parent setter.
     *
     * @param blogEntry
     *            parent to set
     */
    public void setBlogEntry(final BlogEntry blogEntry) {
        this.blogEntry = blogEntry;
    }

    /**
     * User's email getter.
     *
     * @return user's email
     */
    @Column(nullable = false, length = 100)
    public String getUsersEMail() {
        return usersEMail;
    }

    /**
     * User's email setter.
     *
     * @param usersEMail
     *            user's email to set
     */
    public void setUsersEMail(final String usersEMail) {
        this.usersEMail = usersEMail;
    }

    /**
     * Comment's text getter.
     *
     * @return comment's text
     */
    @Column(nullable = false, length = 4096)
    public String getMessage() {
        return message;
    }

    /**
     * Comment's text setter.
     *
     * @param message
     *            text to set
     */
    public void setMessage(final String message) {
        this.message = message;
    }

    /**
     * Comment's post {@link Date} getter.
     *
     * @return comment's post {@link Date}
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    public Date getPostedOn() {
        return postedOn;
    }

    /**
     * Comment's post {@link Date} setter.
     *
     * @param postedOn
     *            {@link Date} when comment was posted
     */
    public void setPostedOn(final Date postedOn) {
        this.postedOn = postedOn;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BlogComment other = (BlogComment) obj;
        if (id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!id.equals(other.id)) {
            return false;
        }
        return true;
    }
}

package hr.fer.zemris.java.hw15.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * This class represents blog entry.
 *
 * @author Stipan Mikulić
 * @version 1.0
 */
//@formatter:off
@NamedQueries({
    @NamedQuery(name = "BlogEntry.upit1",
        query = "select b from BlogComment as b where b.blogEntry=:be and b.postedOn>:when",
        hints = {@QueryHint(name = "org.hibernate.cacheable", value = "true")}
    )
})
// @formatter:on
@Entity
@Table(name = "blog_entries")
@Cacheable(true)
public class BlogEntry {

    /**
     * Blog entry id.
     */
    private Long id;
    /**
     * Blog entry comments.
     */
    private List<BlogComment> comments = new ArrayList<>();
    /**
     * Date when entry was created.
     */
    private Date createdAt;
    /**
     * Date when entry was last time modified.
     */
    private Date lastModifiedAt;
    /**
     * Entry's title.
     */
    private String title;
    /**
     * Entry's text.
     */
    private String text;
    /**
     * {@link BlogUser} that is creator of this entry.
     */
    private BlogUser creator;

    /**
     * Entry's id getter.
     *
     * @return entry's id
     */
    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    /**
     * Entry's id setter.
     *
     * @param id
     *            id to set
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Entry's comments getter.
     *
     * @return entry's {@link List} of comments
     */
    @OneToMany(mappedBy = "blogEntry", fetch = FetchType.LAZY, cascade = CascadeType.PERSIST,
            orphanRemoval = true)
    @OrderBy("postedOn")
    public List<BlogComment> getComments() {
        return comments;
    }

    /**
     * Entry's comments setter.
     *
     * @param comments
     *            {@link List} of comments to set
     */
    public void setComments(final List<BlogComment> comments) {
        this.comments = comments;
    }

    /**
     * Entry's creation {@link Date} getter.
     *
     * @return entry's creation {@link Date}
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    public Date getCreatedAt() {
        return createdAt;
    }

    /**
     * Entry's creation {@link Date} setter.
     *
     * @param createdAt
     *            {@link Date} to set
     */
    public void setCreatedAt(final Date createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * Entry's last modification {@link Date} getter.
     *
     * @return entry's last modification {@link Date}
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = true)
    public Date getLastModifiedAt() {
        return lastModifiedAt;
    }

    /**
     * Entry's last modification {@link Date} setter.
     *
     * @param lastModifiedAt
     *            {@link Date} to set
     */
    public void setLastModifiedAt(final Date lastModifiedAt) {
        this.lastModifiedAt = lastModifiedAt;
    }

    /**
     * Entry's title getter.
     *
     * @return entry's title
     */
    @Column(nullable = false, length = 100)
    public String getTitle() {
        return title;
    }

    /**
     * Entry's title setter.
     *
     * @param title
     *            title to set
     */
    public void setTitle(final String title) {
        this.title = title;
    }

    /**
     * Entry's text getter.
     *
     * @return entry's text
     */
    @Column(nullable = false, length = 4096)
    public String getText() {
        return text;
    }

    /**
     * Entry's text setter.
     *
     * @param text
     *            text to set
     */
    public void setText(final String text) {
        this.text = text;
    }

    /**
     * Entry's creator getter.
     *
     * @return {@link BlogUser} that is {@link BlogEntry} creator
     */
    @ManyToOne
    @JoinColumn(nullable = false)
    public BlogUser getCreator() {
        return creator;
    }

    /**
     * Entry's creator setter.
     *
     * @param creator
     *            {@link BlogUser} to set
     */
    public void setCreator(final BlogUser creator) {
        this.creator = creator;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BlogEntry other = (BlogEntry) obj;
        if (id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!id.equals(other.id)) {
            return false;
        }
        return true;
    }
}

package hr.fer.zemris.java.hw15.dao;

import hr.fer.zemris.java.hw15.model.BlogComment;
import hr.fer.zemris.java.hw15.model.BlogEntry;
import hr.fer.zemris.java.hw15.model.BlogUser;

import java.util.List;

/**
 * This interface represents view to subsystem for data persistence.
 *
 * @author Stipan Mikulić
 * @version 1.0
 */
public interface DAO {

    /**
     * Retrieves {@link BlogEntry} for given id. If {@link BlogEntry} with given id does not exists,
     * <code>null</code> is retrieved.
     *
     * @param id
     *            id key
     * @return {@link BlogEntry} or <code>null</code> if entry does not exists
     * @throws DAOException
     *             if error while getting data occurred.
     */
    BlogEntry getBlogEntry(Long id) throws DAOException;

    /**
     * Retrieves {@link BlogUser} for given nick. If {@link BlogUser} with given nick does not
     * exists, <code>null</code> is retrieved.
     *
     * @param nick
     *            nick that is searched for
     * @return {@link BlogUser} or <code>null</code> if {@link BlogUser} does not exists
     * @throws DAOException
     *             if error while getting data occurred.
     */
    BlogUser getBlogUser(final String nick) throws DAOException;

    /**
     * Retrieves {@link List} of {@link BlogUser} objects in database.
     *
     * @return {@link List} of {@link BlogUser} objects
     * @throws DAOException
     *             if error while getting data occurred.
     */
    List<BlogUser> getBlogUsers() throws DAOException;

    /**
     * Retrieves {@link List} of {@link BlogEntry} objects that have given {@link BlogUser} as
     * creator.
     *
     * @param creator
     *            {@link BlogUser} whose {@link BlogEntry} objects are searched for
     * @return {@link List} of {@link BlogEntry} objects
     * @throws DAOException
     *             if error while getting data occurred.
     */
    List<BlogEntry> getBlogEntries(final BlogUser creator) throws DAOException;

    /**
     * Retrieves {@link List} of {@link BlogComment} objects are part of given {@link BlogEntry}.
     *
     * @param blogEntry
     *            parent for searched {@link BlogComment} objects
     * @return {@link List} of {@link BlogComment} objects
     */
    List<BlogComment> getBlogComments(final BlogEntry blogEntry);

}

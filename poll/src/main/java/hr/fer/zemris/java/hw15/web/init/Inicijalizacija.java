package hr.fer.zemris.java.hw15.web.init;

import hr.fer.zemris.java.hw15.dao.jpa.JPAEMFProvider;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * <p>
 * This class represents {@link WebListener} which initialize needed databases for this blog
 * application.
 * </p>
 *
 * @author Stipan Mikulić
 * @version 1.0
 */
@WebListener
public class Inicijalizacija implements ServletContextListener {

    @Override
    public void contextInitialized(final ServletContextEvent sce) {
        final EntityManagerFactory emf =
                Persistence.createEntityManagerFactory("baza.podataka.za.blog");
        sce.getServletContext().setAttribute("my.application.emf", emf);
        JPAEMFProvider.setEmf(emf);
    }

    @Override
    public void contextDestroyed(final ServletContextEvent sce) {
        JPAEMFProvider.setEmf(null);
        final EntityManagerFactory emf =
                (EntityManagerFactory) sce.getServletContext().getAttribute("my.application.emf");
        if (emf != null) {
            emf.close();
        }
    }
}

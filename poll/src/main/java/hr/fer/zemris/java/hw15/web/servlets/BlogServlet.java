package hr.fer.zemris.java.hw15.web.servlets;


import hr.fer.zemris.java.hw15.dao.DAOProvider;
import hr.fer.zemris.java.hw15.dao.jpa.JPAEMProvider;
import hr.fer.zemris.java.hw15.model.BlogComment;
import hr.fer.zemris.java.hw15.model.BlogEntry;
import hr.fer.zemris.java.hw15.model.BlogUser;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This class represents {@link HttpServlet} that process page with list of entries and each entry
 * page.
 *
 * @author Stipan Mikulić
 * @version 1.0
 */
@WebServlet(urlPatterns = {"/servleti/author/*"})
public class BlogServlet extends HttpServlet {

    /**
     * Default serial ID.
     */
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp)
            throws ServletException, IOException {
        process(req, resp);
    }

    @Override
    protected void doPost(final HttpServletRequest req, final HttpServletResponse resp)
            throws ServletException, IOException {
        process(req, resp);
    }

    /**
     * This method determines which page has user requested and procces data to that page.
     *
     * @param req
     *            an HttpServletRequest object that contains the request the client has made of the
     *            servlet
     * @param resp
     *            an HttpServletResponse object that contains the response the servlet sends to the
     *            client
     * @throws ServletException
     *             if the request for the POST could not be handled
     * @throws IOException
     *             if an input or output error is detected when the servlet handles the request
     */
    private void process(final HttpServletRequest req, final HttpServletResponse resp)
            throws IOException, ServletException {
        final String pathInfo = req.getPathInfo();
        if (pathInfo.isEmpty()) {

            req.setAttribute("message", "Invalid url.");
            req.getRequestDispatcher("/WEB-INF/pages/error.jsp").forward(req, resp);
            return;
        } else if (pathInfo.endsWith("edit")) {
            final BlogEntry entry =
                    DAOProvider.getDAO().getBlogEntry(Long.parseLong(req.getParameter("entryId")));
            req.setAttribute("entry", entry);
            req.getRequestDispatcher("/WEB-INF/pages/Edit.jsp").forward(req, resp);

        } else if (pathInfo.endsWith("new")) {
            final BlogEntry entry = new BlogEntry();
            req.setAttribute("entry", entry);
            req.getRequestDispatcher("/WEB-INF/pages/New.jsp").forward(req, resp);

        } else if (pathInfo.endsWith("saveEntry")) {
            processNewEntry(req, resp);
        } else if (pathInfo.endsWith("editEntry")) {
            processEditEntry(req, resp);
        } else if (pathInfo.substring(pathInfo.lastIndexOf("/") + 1).matches("[0-9]")) {
            final Long id = Long.parseLong(pathInfo.substring(pathInfo.lastIndexOf("/") + 1));
            processComment(req, resp, id);
        } else if (pathInfo.endsWith("addcomment")) {
            addComment(req, resp);
        } else {
            final String userNick = pathInfo.substring(pathInfo.indexOf("/") + 1).trim();
            final BlogUser user = DAOProvider.getDAO().getBlogUser(userNick);

            final List<BlogEntry> blogEntries = DAOProvider.getDAO().getBlogEntries(user);
            req.setAttribute("blogEntries", blogEntries);

            req.getRequestDispatcher("/WEB-INF/pages/BlogEntries.jsp").forward(req, resp);
        }
    }

    /**
     * This method process given data for editing existing {@link BlogEntry}.
     *
     * @param req
     *            an HttpServletRequest object that contains the request the client has made of the
     *            servlet
     * @param resp
     *            an HttpServletResponse object that contains the response the servlet sends to the
     *            client
     * @throws ServletException
     *             if the request for the POST could not be handled
     * @throws IOException
     *             if an input or output error is detected when the servlet handles the request
     */
    private void processEditEntry(final HttpServletRequest req, final HttpServletResponse resp)
            throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");

        final String method = req.getParameter("editEntry");
        if (!"Submit".equals(method)) {
            resp.sendRedirect(req.getServletContext().getContextPath() + "/servleti/author/"
                    + req.getSession().getAttribute("current.user.nick"));
            return;
        }
        final Long entryId = Long.parseLong(req.getParameter("entryId"));
        final String title = req.getParameter("title");
        final String text = req.getParameter("text");

        final EntityManager em = JPAEMProvider.getEntityManager();
        final BlogEntry entry = em.find(BlogEntry.class, entryId);
        entry.setText(text);
        entry.setTitle(title);
        entry.setLastModifiedAt(new Date());
        em.getTransaction().commit();

        final BlogUser user =
                DAOProvider.getDAO().getBlogUser(
                        (String) req.getSession().getAttribute("current.user.nick"));
        final List<BlogEntry> blogEntries = DAOProvider.getDAO().getBlogEntries(user);
        req.setAttribute("blogEntries", blogEntries);

        req.getRequestDispatcher("/WEB-INF/pages/BlogEntries.jsp").forward(req, resp);
    }

    /**
     * This method adds new comment to selected entry.
     *
     * @param req
     *            an HttpServletRequest object that contains the request the client has made of the
     *            servlet
     * @param resp
     *            an HttpServletResponse object that contains the response the servlet sends to the
     *            client
     * @throws ServletException
     *             if the request for the POST could not be handled
     * @throws IOException
     *             if an input or output error is detected when the servlet handles the request
     */
    private void addComment(final HttpServletRequest req, final HttpServletResponse resp)
            throws IOException, ServletException {
        final String method = req.getParameter("submitComment");
        final EntityManager em = JPAEMProvider.getEntityManager();
        final Long id = Long.parseLong(req.getParameter("entryId"));
        final BlogEntry blogEntry = em.find(BlogEntry.class, id);

        if (method == null) {
            final List<BlogComment> comments = DAOProvider.getDAO().getBlogComments(blogEntry);
            req.setAttribute("blogEntry", blogEntry);
            req.setAttribute("comments", comments);
            req.getRequestDispatcher("/WEB-INF/pages/ShowEntry.jsp").forward(req, resp);
            return;
        }

        final String nick = (String) req.getSession().getAttribute("current.user.nick");
        if (!"Submit".equals(method)) {
            resp.sendRedirect(req.getServletContext().getContextPath() + "/servleti/author/" + nick
                    + "/" + blogEntry.getId());
            return;
        }

        final BlogUser user = DAOProvider.getDAO().getBlogUser(nick);
        final String message = req.getParameter("message");

        final BlogComment comment = new BlogComment();
        comment.setMessage(message);
        if (user != null) {
            comment.setUsersEMail(user.getEmail());
        } else {
            comment.setUsersEMail("user@anonymous.com");
        }
        comment.setPostedOn(new Date());
        comment.setBlogEntry(blogEntry);
        em.persist(comment);
        final List<BlogComment> comments = DAOProvider.getDAO().getBlogComments(blogEntry);
        req.setAttribute("comments", comments);
        req.setAttribute("blogEntry", blogEntry);

        resp.sendRedirect(req.getServletContext().getContextPath() + "/servleti/author/" + nick
                + "/" + blogEntry.getId());
    }

    /**
     * This method process comments to page where each entry is displayed.
     *
     * @param req
     *            an HttpServletRequest object that contains the request the client has made of the
     *            servlet
     * @param resp
     *            an HttpServletResponse object that contains the response the servlet sends to the
     *            client
     * @param id
     *            id for {@link BlogEntry}
     * @throws ServletException
     *             if the request for the POST could not be handled
     * @throws IOException
     *             if an input or output error is detected when the servlet handles the request
     */
    private void processComment(final HttpServletRequest req, final HttpServletResponse resp,
            final Long id) throws IOException, ServletException {
        final EntityManager em = JPAEMProvider.getEntityManager();
        final BlogEntry blogEntry = em.find(BlogEntry.class, id);

        final List<BlogComment> comments = DAOProvider.getDAO().getBlogComments(blogEntry);
        req.setAttribute("comments", comments);
        req.setAttribute("blogEntry", blogEntry);

        req.getRequestDispatcher("/WEB-INF/pages/ShowEntry.jsp").forward(req, resp);
    }

    /**
     * This method process given data from creating new {@link BlogEntry}.
     *
     * @param req
     *            an HttpServletRequest object that contains the request the client has made of the
     *            servlet
     * @param resp
     *            an HttpServletResponse object that contains the response the servlet sends to the
     *            client
     * @throws ServletException
     *             if the request for the POST could not be handled
     * @throws IOException
     *             if an input or output error is detected when the servlet handles the request
     */
    private void processNewEntry(final HttpServletRequest req, final HttpServletResponse resp)
            throws IOException, ServletException {
        req.setCharacterEncoding("UTF-8");

        final String method = req.getParameter("newEntry");
        if (!"Submit".equals(method)) {
            resp.sendRedirect(req.getServletContext().getContextPath() + "/servleti/author/"
                    + req.getSession().getAttribute("current.user.nick"));
            return;
        }
        final String title = req.getParameter("title");
        final String text = req.getParameter("text");
        final BlogEntry entry = new BlogEntry();
        final BlogUser user =
                DAOProvider.getDAO().getBlogUser(
                        (String) req.getSession().getAttribute("current.user.nick"));
        entry.setText(text);
        entry.setTitle(title);
        entry.setCreatedAt(new Date());
        entry.setCreator(user);
        entry.setLastModifiedAt(new Date());

        final EntityManager em = JPAEMProvider.getEntityManager();
        em.persist(entry);
        resp.sendRedirect(req.getServletContext().getContextPath() + "/servleti/author/"
                + req.getSession().getAttribute("current.user.nick"));
    }
}

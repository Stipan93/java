package hr.fer.zemris.java.gui.charts;

import java.util.List;

/**
 * <p>
 * This class represents chart.
 * </p>
 * This class provides methods for getting primary informations about this
 * chart. With those informations it shout be possible to this chart in
 * Cartesian coordinate system.
 * <p>
 * For creating this object necessary informations are:
 * </p>
 * <p>
 * 1) x-axis description and y-axis description
 * </p>
 * <p>
 * 2) Maximal and minimal value for y-axis and range for y-axis
 * </p>
 * <p>
 * 3) {@link XYValue} objects which represents columns of this chart
 * </p>
 * 
 * 
 * 
 * @author Stipan Mikulić
 * @version 1.0
 * @see XYValue
 */
public class BarChart {
	/**
	 * {@link XYValue} objects that represents columns of chart
	 */
	private List<XYValue> values;
	/**
	 * Description for x-axis
	 */
	private String xAxisDescription;
	/**
	 * Description for y-axis
	 */
	private String yAxisDescription;
	/**
	 * Minimal value for y-axis
	 */
	private int yMin;
	/**
	 * Maximal value for y-axis
	 */
	private int yMax;
	/**
	 * Range for values on y-axis
	 */
	private int range;

	/**
	 * Creates new {@link BarChart}. Created object contains all informations
	 * that are necessary for drawing this chart.
	 * 
	 * @param values
	 *            {@link XYValue} objects that represents columns of chart
	 * @param xAxisDescription
	 *            description for x-axis
	 * @param yAxisDescription
	 *            description for y-axis
	 * @param yMin
	 *            minimal value for y-axis
	 * @param yMax
	 *            maximal value for y-axis
	 * @param range
	 *            range for values on y-axis
	 */
	public BarChart(List<XYValue> values, String xAxisDescription,
					String yAxisDescription, int yMin, int yMax, int range) {
		super();
		this.values = values;
		this.xAxisDescription = xAxisDescription;
		this.yAxisDescription = yAxisDescription;
		this.yMin = yMin;
		this.yMax = yMax;
		this.range = range;
	}

	/**
	 * @return {@link List} that contains columns of chart
	 */
	public List<XYValue> getValues() {
		return values;
	}

	/**
	 * @return x-axis description
	 */
	public String getxAxisDescription() {
		return xAxisDescription;
	}

	/**
	 * @return y-axis description
	 */
	public String getyAxisDescription() {
		return yAxisDescription;
	}

	/**
	 * @return minimal value on y-axis
	 */
	public int getyMin() {
		return yMin;
	}

	/**
	 * @return maximal value on y-axis
	 */
	public int getyMax() {
		return yMax;
	}

	/**
	 * @return range for values on y-axis
	 */
	public int getRange() {
		return range;
	}

}

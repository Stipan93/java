package hr.fer.zemris.java.gui.charts;

import java.awt.Polygon;
import java.awt.Rectangle;

/**
 * This class contains all measures for chart that needs to be drawn.
 * 
 * @author Stipan Mikulić
 * @version 1.0
 */
public class ChartMeasures {
	/**
	 * Distance between left side of frame and x-axis
	 */
	public static final int X_AXIS_POSITION = 80;
	/**
	 * Distance between bottom of frame and y-axis
	 */
	public static final int Y_AXIS_POSITION = 70;
	/**
	 * Distance between axes pointers and end of frame
	 */
	public static final int END_OF_AXES = 30;
	/**
	 * Size of dash that mark place on axis
	 */
	public static final int SIZE_OF_LITTLE_LINES = 5;
	/**
	 * Range between numbers that mark axis and axis
	 */
	public static final int RANGE_BETWEEN_AXIS_AND_NUMBERS = 20;
	/**
	 * Size of every edge of pointers (i.e. size of edge of equilateral
	 * triangle)
	 */
	public static final int POINTER_EDGE_SIZE = 10;
	/**
	 * X-coordinate for start of x-axis (i.e. origin of coordinate system)
	 */
	public int xAxisStart;
	/**
	 * X-coordinate for end of x-axis (i.e point where is x-axis pointer)
	 */
	public int xAxisEnd;
	/**
	 * Y-coordinate for x-axis
	 */
	public int xAxisYCoordinate;
	/**
	 * Y-coordinate for numbers that marks columns on x-axis
	 */
	public int xAxisNumbersYCoordinate;
	/**
	 * Length of x-axis
	 */
	public int xAxisLength;
	/**
	 * Y-coordinate for start of y-axis (i.e. origin of coordinate system)
	 */
	public int yAxisStart;
	/**
	 * Y-coordinate for end of y-axis (i.e point where is y-axis pointer)
	 */
	public int yAxisEnd;
	/**
	 * X-coordinate for y-axis
	 */
	public int yAxisXCoordinate;
	/**
	 * X-coordinate for numbers that marks values on y-axis
	 */
	public int yAxisNumbersXCoordinate;
	/**
	 * Length of y-axis
	 */
	public int yAxisLength;
	/**
	 * Width of each column
	 */
	public int columnWidth;
	/**
	 * Height of single cell in coordinate system
	 */
	public int cellHeight;
	/**
	 * Number of columns in charts
	 */
	public int numberOfColumns;
	/**
	 * Number of coordinates marked on y-axis
	 */
	public int numberOfYCoordiantes;
	/**
	 * Height of pointer (i.e. height of equilateral triangle )
	 */
	public int heightOfPointers;
	/**
	 * Pointer for x-axis
	 */
	public Polygon xAxisPointer;
	/**
	 * Pointer for y-axis
	 */
	public Polygon yAxisPointer;

	/**
	 * Creates new {@link ChartMeasures} object and takes needed informations
	 * from given {@link BarChart} and {@link Rectangle}.
	 * 
	 * @param rect
	 *            rectangle that represents panel where chart is drawn
	 * @param chart
	 *            {@link BarChart} that needs to be drawn
	 */
	public ChartMeasures(Rectangle rect, BarChart chart) {
		xAxisStart = X_AXIS_POSITION;
		xAxisEnd = rect.width - END_OF_AXES;
		xAxisYCoordinate = rect.y + rect.height - Y_AXIS_POSITION;
		xAxisNumbersYCoordinate = xAxisYCoordinate
				+ RANGE_BETWEEN_AXIS_AND_NUMBERS;
		xAxisLength = xAxisEnd - xAxisStart + 1;

		yAxisStart = rect.y + rect.height - Y_AXIS_POSITION;
		yAxisEnd = END_OF_AXES;
		yAxisXCoordinate = X_AXIS_POSITION;
		yAxisNumbersXCoordinate = yAxisXCoordinate
				- RANGE_BETWEEN_AXIS_AND_NUMBERS;
		yAxisLength = yAxisStart - yAxisEnd + 1;

		numberOfColumns = chart.getValues().size();
		columnWidth = (xAxisLength - 2 * SIZE_OF_LITTLE_LINES)
				/ numberOfColumns;
		numberOfYCoordiantes = (int) Math.ceil((chart.getyMax() - chart
				.getyMin()) / chart.getRange());
		cellHeight = (yAxisLength - 2 * SIZE_OF_LITTLE_LINES)
				/ numberOfYCoordiantes;

		heightOfPointers = (int) (SIZE_OF_LITTLE_LINES + Math
				.sqrt(POINTER_EDGE_SIZE * POINTER_EDGE_SIZE - POINTER_EDGE_SIZE
						* POINTER_EDGE_SIZE / 4));
		xAxisPointer = new Polygon(new int[] { xAxisEnd + SIZE_OF_LITTLE_LINES,
				xAxisEnd + SIZE_OF_LITTLE_LINES, xAxisEnd + heightOfPointers },
				new int[] { xAxisYCoordinate + POINTER_EDGE_SIZE / 2,
						xAxisYCoordinate - POINTER_EDGE_SIZE / 2,
						xAxisYCoordinate }, 3);
		yAxisPointer = new Polygon(new int[] {
				yAxisXCoordinate - POINTER_EDGE_SIZE / 2,
				yAxisXCoordinate + POINTER_EDGE_SIZE / 2, yAxisXCoordinate },
				new int[] { yAxisEnd - SIZE_OF_LITTLE_LINES,
						yAxisEnd - SIZE_OF_LITTLE_LINES,
						yAxisEnd - heightOfPointers }, 3);
	}
}
package hr.fer.zemris.java.gui.charts;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.Stroke;
import java.awt.geom.AffineTransform;

import javax.swing.JComponent;

/**
 * <p>
 * This class represents container for {@link BarChart} object.
 * </p>
 * It provides methods for drawing given {@link BarChart} in coordinate system.
 * Example for creating new container with drawn chart:
 * 
 * <pre>
 * <code>
 * BarChart chart = new BarChart(...);
 * BarChartComponent chartComponent = new BarChartComponent(chart);
 * </code>
 * </pre>
 * 
 * @author Stipan Mikulić
 * @version 1.0
 * @see BarChart
 */
public class BarChartComponent extends JComponent {
	private static final long serialVersionUID = 1L;
	/**
	 * Size of axes lines
	 */
	private static int BOLD_LINE = 2;
	/**
	 * Size of grid lines
	 */
	private static int THICK_LINE = 1;
	/**
	 * {@link BarChart} that needs to be drawn
	 */
	private BarChart chart;

	/**
	 * Creates new {@link BarChartComponent} that contains chart given as
	 * argument.
	 * 
	 * @param chart
	 *            {@link BarChart} object
	 */
	public BarChartComponent(BarChart chart) {
		super();
		this.chart = chart;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
	 */
	@Override
	protected void paintComponent(final Graphics g) {
		Graphics2D g2d = (Graphics2D) g.create();
		Dimension size = getSize();
		Insets ins = getInsets();

		Rectangle rect = new Rectangle(ins.left, ins.top, size.width - ins.left
				- ins.right, size.height - ins.top - ins.bottom);
		ChartMeasures measures = new ChartMeasures(rect, chart);
		drawAxisDescriptions(g2d, rect);
		drawXAxis((Graphics2D) g2d.create(), measures);
		drawYAxis((Graphics2D) g2d.create(), measures);
		drawColumns((Graphics2D) g2d.create(), measures);
		g2d.dispose();
	}

	/**
	 * This method draws all columns of {@link BarChart} on coordinate grid in
	 * this container.
	 * 
	 * @param g2d
	 *            {@link Graphics2D} object that provides methods for drawing
	 * @param measures
	 *            {@link ChartMeasures} object that contains all necessary
	 *            information about containers frame.
	 */
	private void drawColumns(Graphics2D g2d, ChartMeasures measures) {
		Graphics2D g2dGraph = (Graphics2D) g2d.create();
		for (XYValue value : chart.getValues()) {
			g2dGraph.setColor(new Color(244, 119, 72));
			g2dGraph.fillRect(measures.xAxisStart + (value.getX() - 1)
					* measures.columnWidth + THICK_LINE, measures.yAxisStart
					- measures.cellHeight * (value.getY() - chart.getyMin())
					/ chart.getRange(), measures.columnWidth - THICK_LINE,
					measures.cellHeight * (value.getY() - chart.getyMin())
							/ chart.getRange() - THICK_LINE);
		}
	}

	/**
	 * This method draws x-axis and grid lines that going out of x-axis in this
	 * container.
	 * 
	 * @param g2d
	 *            {@link Graphics2D} object that provides methods for drawing
	 * @param measures
	 *            {@link ChartMeasures} object that contains all necessary
	 *            information about containers frame.
	 */
	private void drawXAxis(Graphics2D g2d, ChartMeasures measures) {
		Graphics2D g2dGraph = (Graphics2D) g2d.create();
		g2d.setStroke((Stroke) new BasicStroke(BOLD_LINE));
		g2d.drawLine(measures.xAxisStart - ChartMeasures.SIZE_OF_LITTLE_LINES,
				measures.xAxisYCoordinate, measures.xAxisEnd
						+ ChartMeasures.SIZE_OF_LITTLE_LINES,
				measures.xAxisYCoordinate);
		g2d.fillPolygon(measures.xAxisPointer);
		g2d.setFont(new Font("default", Font.BOLD, 12));
		for (XYValue value : chart.getValues()) {
			FontMetrics fm = g2d.getFontMetrics();
			int xNumberLength = fm.stringWidth(String.valueOf(value.getX()));

			g2d.drawString(String.valueOf(value.getX()), measures.xAxisStart
					+ (value.getX() - 1) * measures.columnWidth
					+ measures.columnWidth / 2 - xNumberLength / 2,
					measures.xAxisNumbersYCoordinate);

			g2dGraph.setColor(Color.DARK_GRAY);
			g2dGraph.drawLine(measures.xAxisStart + measures.columnWidth
					* value.getX(), measures.xAxisYCoordinate,
					measures.xAxisStart + measures.columnWidth * value.getX(),
					measures.xAxisYCoordinate
							+ ChartMeasures.SIZE_OF_LITTLE_LINES);

			g2dGraph.setColor(Color.lightGray);
			g2dGraph.drawLine(measures.xAxisStart + measures.columnWidth
					* value.getX(), measures.yAxisStart - BOLD_LINE,
					measures.xAxisStart + measures.columnWidth * value.getX(),
					measures.yAxisEnd);
		}
	}

	/**
	 * This method draws y-axis and grid lines that going out of y-axis in this
	 * container.
	 * 
	 * @param g2d
	 *            {@link Graphics2D} object that provides methods for drawing
	 * @param measures
	 *            {@link ChartMeasures} object that contains all necessary
	 *            information about containers frame.
	 */
	private void drawYAxis(Graphics2D g2d, ChartMeasures measures) {
		Graphics2D g2dGraph = (Graphics2D) g2d.create();
		g2d.setStroke((Stroke) new BasicStroke(BOLD_LINE));
		g2d.drawLine(measures.yAxisXCoordinate, measures.yAxisStart
				+ ChartMeasures.SIZE_OF_LITTLE_LINES,
				measures.yAxisXCoordinate, measures.yAxisEnd
						- ChartMeasures.SIZE_OF_LITTLE_LINES);
		g2d.fillPolygon(measures.yAxisPointer);
		int y = chart.getyMin();
		FontMetrics fm = g2d.getFontMetrics();
		int xNumberLength = fm.stringWidth(String.valueOf(y));
		g2d.setFont(new Font("default", Font.BOLD, 12));
		g2d.drawString(String.valueOf(y), measures.yAxisNumbersXCoordinate
				- xNumberLength,
				measures.yAxisStart + (fm.getAscent() - fm.getDescent()) / 2);
		y += chart.getRange();
		for (; y <= chart.getyMax(); y += chart.getRange()) {
			fm = g2d.getFontMetrics();
			xNumberLength = fm.stringWidth(String.valueOf(y));
			g2d.setFont(new Font("default", Font.BOLD, 12));
			g2d.drawString(String.valueOf(y), measures.yAxisNumbersXCoordinate
					- xNumberLength, measures.yAxisStart
					- (y - chart.getyMin()) / chart.getRange()
					* measures.cellHeight + (fm.getAscent() - fm.getDescent())
					/ 2);

			g2dGraph.setColor(Color.DARK_GRAY);
			g2dGraph.drawLine(measures.yAxisXCoordinate, measures.yAxisStart
					- (y - chart.getyMin()) / chart.getRange()
					* measures.cellHeight, measures.yAxisXCoordinate
					- ChartMeasures.SIZE_OF_LITTLE_LINES, measures.yAxisStart
					- (y - chart.getyMin()) / chart.getRange()
					* measures.cellHeight);

			g2dGraph.setColor(Color.lightGray);
			g2dGraph.drawLine(
					measures.yAxisXCoordinate + BOLD_LINE,
					measures.yAxisStart - (y - chart.getyMin())
							/ chart.getRange() * measures.cellHeight,
					measures.xAxisEnd + ChartMeasures.SIZE_OF_LITTLE_LINES,
					measures.yAxisStart - (y - chart.getyMin())
							/ chart.getRange() * measures.cellHeight);
		}
	}

	/**
	 * This method draws descriptions for x-axis and y-axis in this container.
	 * 
	 * @param g2d
	 *            {@link Graphics2D} object that provides methods for drawing
	 * @param rect
	 *            rectangle that represents panel where chart is drawn
	 */
	private void drawAxisDescriptions(Graphics2D g2d, Rectangle rect) {
		FontMetrics fm = g2d.getFontMetrics();
		int xDescriptionLength = fm.stringWidth(chart.getxAxisDescription());

		g2d.drawString(chart.getxAxisDescription(), rect.x
				+ (rect.width - xDescriptionLength) / 2, rect.y + rect.height
				- fm.getAscent());

		AffineTransform at = new AffineTransform();
		Graphics2D g2dRotate = (Graphics2D) g2d.create();
		at.rotate(-Math.PI / 2);
		g2dRotate.setTransform(at);
		int yDescriptionLength = fm.stringWidth(chart.getyAxisDescription());
		g2dRotate.drawString(chart.getyAxisDescription(),
				-(rect.x + (rect.height + yDescriptionLength) / 2), rect.y + 2
						* fm.getAscent());
	}
}

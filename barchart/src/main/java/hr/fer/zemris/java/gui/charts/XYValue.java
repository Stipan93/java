package hr.fer.zemris.java.gui.charts;

/**
 * <p>
 * This class represents each column in chart.
 * <p>
 * Every column has its number and value. Number represents where column should
 * be on x-axis. Value represents height of column.
 * 
 * 
 * Example of creating {@link XYValue} with value equal to 2 and value equal to
 * 4:
 * 
 * <pre>
 * <code>
 * XYValue column = new XYValue(2,4);
 * </code>
 * </pre>
 * 
 * @author Stipan Mikulić
 * @version 1.0
 */
public class XYValue {
	/**
	 * Number of column
	 */
	private int x;
	/**
	 * Value of column
	 */
	private int y;

	/**
	 * Creates new {@link XYValue} with number and value given as arguments.
	 * 
	 * @param x
	 *            number of column
	 * @param y
	 *            value of column
	 */
	public XYValue(int x, int y) {
		super();
		this.x = x;
		this.y = y;
	}

	/**
	 * @return number of column
	 */
	public int getX() {
		return x;
	}

	/**
	 * @return value of column
	 */
	public int getY() {
		return y;
	}

}

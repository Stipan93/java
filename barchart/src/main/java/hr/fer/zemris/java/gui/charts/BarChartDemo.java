package hr.fer.zemris.java.gui.charts;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

/**
 * <p>
 * This class represents frame which contains {@link BarChartComponent} where
 * given {@link BarChart} is drawn.
 * </p>
 * This program accepts one command line argument. The argument must be name of
 * file where definition of {@link BarChart} is stored. If argument is not given
 * then program is closed.
 * 
 * @author Stipan Mikulić
 * @version 1.0
 * @see JFrame
 * @see BarChartComponent
 */
public class BarChartDemo extends JFrame {
	private static final long serialVersionUID = 1L;
	/**
	 * Number of arguments for {@link BarChart} definition
	 */
	private static final int NUMBER_OF_ARGUMENTS = 6;
	/**
	 * Name of file where {@link BarChart} definition is stored
	 */
	private static String fileName;

	/**
	 * Creates new frame where given {@link BarChart} should be drawn.
	 * 
	 * @param chart
	 *            {@link BarChart} to draw
	 */
	public BarChartDemo(BarChart chart) {
		getContentPane().setLayout(new BorderLayout());
		setLocation(200, 200);
		setSize(new Dimension(600, 400));
		setMinimumSize(new Dimension(300, 300));
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

		BarChartComponent chartComponent = new BarChartComponent(chart);
		JLabel filePath = new JLabel(fileName);
		filePath.setHorizontalAlignment(SwingConstants.CENTER);
		getContentPane().add(filePath, BorderLayout.NORTH);
		getContentPane().add(chartComponent, BorderLayout.CENTER);
	}

	/**
	 * This method is called when program start.
	 * 
	 * @param args
	 *            command line arguments
	 */
	public static void main(String[] args) {
		List<String> arguments = null;
		if (args.length == 1) {
			fileName = args[0];
			try {
				arguments = readFile(fileName);
			} catch (Exception e) {
				System.err.println("Error while reading file : " + fileName);
			}
		} else {
			System.err.println("Invalid number of arguments!");
			System.exit(1);
		}
		if (arguments.size() != NUMBER_OF_ARGUMENTS) {
			System.out.println("Invalid number of arguments for graph!");
			System.exit(1);
		}
		BarChart chart = new BarChart(getValues(arguments.get(2)),
				arguments.get(0), arguments.get(1), Integer.parseInt(arguments
						.get(3)), Integer.parseInt(arguments.get(4)),
				Integer.parseInt(arguments.get(5)));

		SwingUtilities.invokeLater(() -> {
			BarChartDemo frame = new BarChartDemo(chart);
			frame.setVisible(true);
		});
	}

	/**
	 * This method reads {@link BarChart} arguments from given file.
	 * 
	 * @param fileName
	 *            name of file where {@link BarChart} definition is stored
	 * @return {@link List} that contains all arguments for {@link BarChart}
	 * @throws IOException
	 *             if error while reading file has occurred
	 */
	private static List<String> readFile(String fileName) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(
				new FileInputStream(fileName)));
		String line = null;
		List<String> list = new ArrayList<>();
		while ((line = reader.readLine()) != null) {
			list.add(line);
		}
		reader.close();
		return list;
	}

	/**
	 * This method makes {@link XYValue} objects whose definitions are read from
	 * file.
	 * 
	 * @param values
	 *            {@link String} with definitions for every {@link XYValue}
	 *            object that chart contains
	 * @return {@link List} of separated definitions for each {@link XYValue}
	 *         object
	 * @see XYValue
	 */
	private static List<XYValue> getValues(String values) {
		List<XYValue> XYvalues = new ArrayList<XYValue>();
		for (String value : values.split("\\s+")) {
			String[] args = value.split(",");
			XYvalues.add(new XYValue(Integer.parseInt(args[0]), Integer
					.parseInt(args[1])));
		}
		return XYvalues;

	}
}

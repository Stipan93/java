package hr.fer.zemris.java.hw12.jvdraw;

import hr.fer.zemris.java.hw12.jvdraw.geometricalobjects.GeometricalObject;

/**
 * <p>
 * This interface represents model for drawing geometric objects.
 * </p>
 *
 * @author Stipan Mikulić
 * @version 1.0
 */
public interface DrawingModel {
    /**
     * This method returns number of objects in this {@link DrawingModel}.
     *
     * @return size of {@link DrawingModel}
     */
    int getSize();

    /**
     * Gets {@link GeometricalObject} at given index.
     *
     * @param index
     *            index to get
     * @return {@link GeometricalObject} at specified index
     */
    GeometricalObject getObject(int index);

    /**
     * Adds new {@link GeometricalObject} to objects list.
     *
     * @param object
     *            {@link GeometricalObject} to add
     */
    void add(GeometricalObject object);

    /**
     * Adds {@link DrawingModelListener} to listeners list.
     *
     * @param l
     *            {@link DrawingModelListener} to add
     */
    void addDrawingModelListener(DrawingModelListener l);

    /**
     * Removes {@link DrawingModelListener} from listeners list.
     *
     * @param l
     *            {@link DrawingModelListener} to remove
     */
    void removeDrawingModelListener(DrawingModelListener l);

    /**
     * Getter for number of lines.
     *
     * @return number of lines
     */
    int getNumberOfLines();

    /**
     * Getter for number of circle.
     *
     * @return number of circles
     */
    int getNumberOfCircles();

    /**
     * Getter for number of filled circle.
     *
     * @return number of filled circles
     */
    int getNumberOfFilledCircles();

    /**
     * Getter for change flag.
     *
     * @return change flag of this {@link DrawingModel}.
     */
    boolean isChanged();

    /**
     * This method sets change flag of this {@link DrawingModel}.
     *
     * @param hasChanged
     *            value to set
     */
    void setChanged(boolean hasChanged);

    /**
     * This method removes all objects from {@link DrawingModel}.
     */
    void clear();

}

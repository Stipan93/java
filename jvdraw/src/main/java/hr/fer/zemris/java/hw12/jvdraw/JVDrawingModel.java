package hr.fer.zemris.java.hw12.jvdraw;

import hr.fer.zemris.java.hw12.jvdraw.geometricalobjects.Circle;
import hr.fer.zemris.java.hw12.jvdraw.geometricalobjects.FilledCircle;
import hr.fer.zemris.java.hw12.jvdraw.geometricalobjects.GeometricalObject;
import hr.fer.zemris.java.hw12.jvdraw.geometricalobjects.Line;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * This class implements {@link DrawingModel} interface.
 * </p>
 * It implements methods for adding {@link GeometricalObject}, adding {@link DrawingModelListener}
 * and other primary methods that are crucial to describe simple {@link DrawingModel}.
 *
 * @author Stipan Mikulić
 * @version 1.0
 * @see DrawingModel
 */
public class JVDrawingModel implements DrawingModel {
    /**
     * {@link List} that contains all {@link GeometricalObject} in this {@link JVDrawingModel}.
     */
    private final List<GeometricalObject> geometricalObjects;
    /**
     * {@link List} that contains registered {@link DrawingModelListener} objects.
     */
    private final List<DrawingModelListener> listeners;
    /**
     * Number of {@link Line} in this {@link JVDrawingModel}.
     */
    private int numberOfLines;
    /**
     * Number of {@link Circle} in this {@link JVDrawingModel}.
     */
    private int numberOfCircles;
    /**
     * Number of {@link FilledCircle} in this {@link JVDrawingModel}.
     */
    private int numberOfFilledCircles;
    /**
     * Flag that determines is any change made in this {@link JVDrawingModel}.
     */
    private boolean hasChanged;

    /**
     * Creates new {@link JVDrawingModel}.
     */
    public JVDrawingModel() {
        geometricalObjects = new ArrayList<GeometricalObject>();
        listeners = new ArrayList<DrawingModelListener>();
        numberOfLines = 1;
        numberOfCircles = 1;
        numberOfFilledCircles = 1;
        hasChanged = false;
    }

    @Override
    public int getSize() {
        return geometricalObjects.size();
    }

    @Override
    public GeometricalObject getObject(final int index) {
        return geometricalObjects.get(index);
    }

    @Override
    public void add(final GeometricalObject object) {
        if (object instanceof Line) {
            numberOfLines++;
        } else if (object instanceof Circle) {
            numberOfCircles++;
        } else if (object instanceof FilledCircle) {
            numberOfFilledCircles++;
        }
        hasChanged = true;
        geometricalObjects.add(object);
        fire();
    }

    @Override
    public void addDrawingModelListener(final DrawingModelListener listener) {
        listeners.add(listener);
    }

    @Override
    public void removeDrawingModelListener(final DrawingModelListener listener) {
        listeners.remove(listener);
    }

    @Override
    public void clear() {
        geometricalObjects.clear();
        numberOfCircles = 1;
        numberOfFilledCircles = 1;
        numberOfLines = 1;
    }

    /**
     * This method notifies all registered {@link DrawingModelListener} objects that change is made.
     */
    private void fire() {
        for (final DrawingModelListener listener : listeners) {
            listener.objectsAdded(this, getSize() - 1, getSize() - 1);
        }
    }

    @Override
    public int getNumberOfLines() {
        return numberOfLines;
    }

    @Override
    public int getNumberOfCircles() {
        return numberOfCircles;
    }

    @Override
    public int getNumberOfFilledCircles() {
        return numberOfFilledCircles;
    }

    @Override
    public boolean isChanged() {
        return hasChanged;
    }

    @Override
    public void setChanged(final boolean hasChanged) {
        this.hasChanged = hasChanged;
    }
}

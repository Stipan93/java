package hr.fer.zemris.java.hw12.jvdraw;

import java.awt.Color;

/**
 * <p>
 * This interface makes contract between {@link IColorProvider} and object that needs to know when
 * color is changed.
 * </p>
 * Class that implements this interface must implement method
 * {@link #newColorSelected(IColorProvider, Color, Color)}.
 *
 * @author Stipan Mikulić
 * @version 1.0
 */
public interface ColorChangeListener {
    /**
     * This method is called when modification in {@link IColorProvider} is made. It implements
     * actions that need to be made when subject notifies this object that color is changed.
     *
     * @param source
     *            {@link IColorProvider} object that offers {@link Color} manipulation
     * @param oldColor
     *            old color
     * @param newColor
     *            new color
     */
    void newColorSelected(IColorProvider source, Color oldColor, Color newColor);
}

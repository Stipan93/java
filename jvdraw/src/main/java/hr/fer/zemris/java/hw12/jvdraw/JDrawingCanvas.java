package hr.fer.zemris.java.hw12.jvdraw;

import hr.fer.zemris.java.hw12.jvdraw.geometricalobjects.Circle;
import hr.fer.zemris.java.hw12.jvdraw.geometricalobjects.FilledCircle;
import hr.fer.zemris.java.hw12.jvdraw.geometricalobjects.Line;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.util.Enumeration;

import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.JComponent;

/**
 * <p>
 * This class represents drawing canvas.
 * </p>
 * This class is {@link DrawingModelListener} and {@link ColorChangeListener} so it has automatic
 * mechanism for getting selected {@link Color} and objects in {@link DrawingModel}.
 *
 * @author Stipan Mikulić
 * @version 1.0
 */
//@formatter:off
public class JDrawingCanvas extends JComponent
        implements DrawingModelListener, ColorChangeListener {
//@formatter:on
    /**
     * Default serial ID.
     */
    private static final long serialVersionUID = 1L;
    /**
     * Button group with buttons for selecting objects to draw.
     */
    private final ButtonGroup drawingTools;
    /**
     * Start {@link Point} on canvas that is saved on first mouse click.
     */
    private Point start;
    /**
     * End {@link Point} on canvas that is saved on second mouse click.
     */
    private Point end;
    /**
     * Current {@link Point} on canvas that is saved on every mouse move.
     */
    private Point current;
    /**
     * This flag determines does first mouse click occurred. (i.e. is some object drawing at this
     * moment on canvas)
     */
    private boolean isDrawing;
    /**
     * Foreground {@link Color}.
     */
    private Color foreground;
    /**
     * Background {@link Color}.
     */
    private Color background;
    /**
     * {@link DrawingModel} object.
     */
    private final DrawingModel drawingModel;
    /**
     * Selected button that represents object that needs to be drawn.
     */
    private AbstractButton selectedButton;

    /**
     * Creates new instance {@link JDrawingCanvas}.
     *
     * @param tools
     *            group of buttons with objects that are drawn
     * @param model
     *            {@link DrawingModel} object
     */
    public JDrawingCanvas(final ButtonGroup tools, final DrawingModel model) {
        drawingTools = tools;
        isDrawing = false;
        foreground = Color.BLACK;
        background = Color.WHITE;
        drawingModel = model;
        selectedButton = null;
    }

    // add mouse listener and mouse motion listener
    {
        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(final MouseEvent e) {
                final Enumeration<AbstractButton> en = drawingTools.getElements();
                boolean hasSelected = false;
                while (en.hasMoreElements()) {
                    selectedButton = en.nextElement();
                    if (selectedButton.getName().equals("pointer")) {
                        continue;
                    }
                    if (selectedButton.isSelected()) {
                        hasSelected = true;
                        break;
                    }
                }
                if (!hasSelected) {
                    selectedButton = null;
                    return;
                }
                if (!isDrawing) {
                    isDrawing = true;
                    end = null;
                    start = e.getPoint();
                } else {
                    isDrawing = false;
                    end = e.getPoint();
                }
            }
        });

        addMouseMotionListener(new MouseMotionAdapter() {
            @Override
            public void mouseMoved(final MouseEvent e) {
                current = e.getPoint();
                repaint();
            }
        });
    }

    @Override
    public void paint(final Graphics g) {
        final Graphics2D g2d = (Graphics2D) g;
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        for (int i = 0, size = drawingModel.getSize(); i < size; i++) {
            drawingModel.getObject(i).paint(g2d);
        }
        if (selectedButton == null) {
            return;
        }
        g2d.setColor(foreground);
        if (selectedButton.getText().equals("line")) {
            if (end == null) {
                if (start != null && current != null) {
                    g2d.drawLine(start.x, start.y, current.x, current.y);
                }
            } else {
                drawingModel.add(new Line(drawingModel.getNumberOfLines(), start, end, foreground));
                start = null;
                end = null;
                current = null;
            }
        } else if (selectedButton.getText().equals("circle")) {
            if (end == null) {
                if (start != null && current != null) {
                    final double radius = Math.hypot(current.x - start.x, current.y - start.y);
                    final int x = (int) (start.x - radius);
                    final int y = (int) (start.y - radius);
                    g2d.drawOval(x, y, (int) (2 * radius), (int) (2 * radius));
                }
            } else {
                final double radius = Math.hypot(end.x - start.x, end.y - start.y);
                drawingModel.add(new Circle(drawingModel.getNumberOfCircles(), new Point(start.x,
                        start.y), (int) radius, foreground));
                start = null;
                end = null;
                current = null;
            }
        } else if (selectedButton.getText().equals("filled circle")) {
            if (end == null) {
                if (start != null && current != null) {
                    final double radius = Math.hypot(current.x - start.x, current.y - start.y);
                    final int x = (int) (start.x - radius);
                    final int y = (int) (start.y - radius);
                    g2d.setColor(background);
                    g2d.fillOval(x, y, (int) (2 * radius), (int) (2 * radius));
                    g2d.setColor(foreground);
                    g2d.drawOval(x, y, (int) (2 * radius), (int) (2 * radius));
                }
            } else {
                final double radius = Math.hypot(end.x - start.x, end.y - start.y);
                drawingModel.add(new FilledCircle(drawingModel.getNumberOfFilledCircles(),
                        new Point(start.x, start.y), (int) radius, foreground, background));
                start = null;
                end = null;
                current = null;
            }
        }
    };


    @Override
    public void objectsAdded(final DrawingModel source, final int index0, final int index1) {
        repaint();
    }

    @Override
    public void objectsRemoved(final DrawingModel source, final int index0, final int index1) {
        repaint();
    }

    @Override
    public void objectsChanged(final DrawingModel source, final int index0, final int index1) {
        repaint();
    }

    @Override
    public void newColorSelected(final IColorProvider source, final Color oldColor,
            final Color newColor) {
        if (foreground.equals(oldColor)) {
            foreground = newColor;
        } else if (background.equals(oldColor)) {
            background = newColor;
        }
    }
}

package hr.fer.zemris.java.hw12.jvdraw.geometricalobjects;

import hr.fer.zemris.java.hw12.jvdraw.JVDraw;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.Point;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * <p>
 * This class represents circle.
 * </p>
 * Circle is defined by center point, radius and outline color. Each {@link Circle} has its
 * identification number.
 *
 * @author Stipan Mikulić
 * @version 1.0
 */
public class Circle extends GeometricalObject {
    /**
     * Circle generic name.
     */
    private static final String NAME = "circle";
    /**
     * Circle center.
     */
    private final Point center;
    /**
     * Circle radius.
     */
    private int radius;
    /**
     * Circle outline color.
     */
    private Color outlineColor;

    /**
     * Create new instance of {@link Circle}.
     *
     * @param num
     *            circle number
     * @param center
     *            center of circle
     * @param radius
     *            radius of circle
     * @param outlineColor
     *            outline color of circle
     */
    public Circle(final int num, final Point center, final int radius, final Color outlineColor) {
        super(NAME + " " + num);
        this.center = center;
        this.radius = radius;
        this.outlineColor = outlineColor;
    }

    @Override
    public void paint(final Graphics2D g2d) {
        g2d.setColor(outlineColor);
        g2d.drawOval(center.x - radius, center.y - radius, 2 * radius, 2 * radius);
    }

    @Override
    public void showModifyDialog(final JVDraw frame) {
        final JPanel panel = new JPanel(new GridLayout(4, 2));
        addPair("Center-x", panel);
        addPair("Center-y", panel);
        addPair("Radius", panel);
        addPair("Outline color(r,g,b)", panel);
        final int result =
                JOptionPane.showConfirmDialog(frame, panel,
                        "Modify circle. Leave empty to not modify attribute.",
                        JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
        if (result == JOptionPane.OK_OPTION) {
            boolean hasChanged = false;
            final int xOfCenterPointInput = 1;
            final int yOfCenterPointInput = 3;
            final int radiusInput = 5;
            final int outlineColorInput = 7;
            try {
                final String centerX =
                        ((JTextField) panel.getComponent(xOfCenterPointInput)).getText();
                if (!centerX.isEmpty()) {
                    center.x = Integer.parseInt(centerX);
                    hasChanged = true;
                }
                final String centerY =
                        ((JTextField) panel.getComponent(yOfCenterPointInput)).getText();
                if (!centerY.isEmpty()) {
                    center.y = Integer.parseInt(centerY);
                    hasChanged = true;
                }
                final String radiusString =
                        ((JTextField) panel.getComponent(radiusInput)).getText();
                if (!radiusString.isEmpty()) {
                    radius = Integer.parseInt(radiusString);
                    hasChanged = true;
                }
                final String rgbString =
                        ((JTextField) panel.getComponent(outlineColorInput)).getText();
                if (!rgbString.isEmpty()) {
                    final String[] rgb = rgbString.split(",");
                    outlineColor =
                            new Color(Integer.parseInt(rgb[0]), Integer.parseInt(rgb[1]),
                                    Integer.parseInt(rgb[2]));
                    hasChanged = true;
                }
                frame.getDrawingModel().setChanged(hasChanged);
            } catch (final Exception e) {
                JOptionPane.showMessageDialog(frame, "Incorrectly filled form!");
            }
        }
    }

    /**
     * Getter for center {@link Point}.
     *
     * @return center of circle
     */
    public Point getCenter() {
        return center;
    }

    /**
     * Getter for circle radius.
     *
     * @return circle radius
     */
    public int getRadius() {
        return radius;
    }

    /**
     * Getter for outline color.
     *
     * @return outline color
     */
    public Color getOutlineColor() {
        return outlineColor;
    }

    @Override
    public int getMinX() {
        return center.x - radius;
    }

    @Override
    public int getMinY() {
        return center.y - radius;
    }

    @Override
    public int getMaxX() {
        return center.x + radius;
    }

    @Override
    public int getMaxY() {
        return center.y + radius;
    }

    @Override
    public String getInformation() {
        return "CIRCLE " + center.x + " " + center.y + " " + radius + " " + outlineColor.getRed()
                + " " + outlineColor.getGreen() + " " + outlineColor.getBlue() + "\n";
    }
}

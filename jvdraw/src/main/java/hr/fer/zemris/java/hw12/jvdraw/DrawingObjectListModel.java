package hr.fer.zemris.java.hw12.jvdraw;

import hr.fer.zemris.java.hw12.jvdraw.geometricalobjects.GeometricalObject;

import javax.swing.AbstractListModel;

/**
 * <p>
 * This class represents simple list model.
 * </p>
 * This list model contains {@link GeometricalObject} that are drawn on {@link JDrawingCanvas}.
 *
 * @author Stipan Mikulić
 * @version 1.0
 */
public class DrawingObjectListModel extends AbstractListModel<GeometricalObject> implements
        DrawingModelListener {
    /**
     * Default serial ID.
     */
    private static final long serialVersionUID = 1L;
    /**
     * {@link DrawingModel} object. Adaptee for this adapter.
     */
    private final DrawingModel model;

    /**
     * Creates new instance of {@link DrawingObjectListModel}.
     *
     * @param model
     *            {@link DrawingModel} object that is adaptee for this adapter.
     */
    public DrawingObjectListModel(final DrawingModel model) {
        this.model = model;
    }

    @Override
    public int getSize() {
        return model.getSize();
    }

    @Override
    public GeometricalObject getElementAt(final int index) {
        return model.getObject(index);
    }

    @Override
    public void objectsAdded(final DrawingModel source, final int index0, final int index1) {
        fireIntervalAdded(source, index0, index1);
    }

    @Override
    public void objectsRemoved(final DrawingModel source, final int index0, final int index1) {
        fireIntervalRemoved(source, index0, index1);
    }

    @Override
    public void objectsChanged(final DrawingModel source, final int index0, final int index1) {
        fireContentsChanged(source, index0, index1);
    }

}

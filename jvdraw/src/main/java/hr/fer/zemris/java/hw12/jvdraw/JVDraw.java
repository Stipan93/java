package hr.fer.zemris.java.hw12.jvdraw;

import hr.fer.zemris.java.hw12.jvdraw.geometricalobjects.Circle;
import hr.fer.zemris.java.hw12.jvdraw.geometricalobjects.FilledCircle;
import hr.fer.zemris.java.hw12.jvdraw.geometricalobjects.GeometricalObject;
import hr.fer.zemris.java.hw12.jvdraw.geometricalobjects.Line;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.nio.file.Path;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

/**
 * <p>
 * This class represents simple drawing application.
 * </p>
 * This program supports drawing lines, circles and circles filled with specified color. Each object
 * can be drawn with other color.
 *
 * @author Stipan Mikulić
 * @version 1.0
 */
public class JVDraw extends JFrame {
    /**
     * Default serial ID.
     */
    private static final long serialVersionUID = 1L;
    /**
     * Foreground color description for status bar label.
     */
    private static final String FOREGROUND = "Foreground color: ";
    /**
     * Background color description for status bar label.
     */
    private static final String BACKGROUND = ", background color: ";
    /**
     * Preferred size of list of objects.
     */
    private static final Dimension JLIST_PREFERRED_SIZE = new Dimension(150, 0);
    /**
     * Default frame size.
     */
    private static final Dimension DEFAULT_FRAME_SIZE = new Dimension(800, 600);
    /**
     * Tool bar for this {@link JVDraw} frame.
     */
    private JToolBar toolBar;
    /**
     * Status bar for this {@link JVDraw} frame.
     */
    private JPanel statusBar;
    /**
     * Foreground color.
     */
    private JColorArea foreground;
    /**
     * Background color.
     */
    private JColorArea background;
    /**
     * Button group where geometric objects for drawing can be selected.
     */
    private ButtonGroup drawTools;
    /**
     * {@link DrawingModel} object.
     */
    private final DrawingModel drawingModel;
    /**
     * {@link DrawingObjectListModel} where list of drawn objects is stored.
     */
    private final DrawingObjectListModel drawingObjectList;
    /**
     * {@link JDrawingCanvas} where objects are drawn.
     */
    private final JDrawingCanvas drawingCanvas;
    /**
     * Storage for {@link Path} of currently opened file.
     */
    private Path openedFile;

    /**
     * Creates new {@link JVDraw} object.
     */
    public JVDraw() {
        setTitle("JVDraw");
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        setSize(DEFAULT_FRAME_SIZE);
        setLayout(new BorderLayout());
        initToolBar();
        initStatusBar();
        openedFile = null;
        drawingModel = new JVDrawingModel();
        drawingCanvas = new JDrawingCanvas(drawTools, drawingModel);
        foreground.addColorChangeListener(drawingCanvas);
        background.addColorChangeListener(drawingCanvas);
        drawingObjectList = new DrawingObjectListModel(drawingModel);
        drawingModel.addDrawingModelListener(drawingObjectList);
        setJMenuBar(new JVMenuBar(this));
        getContentPane().add(toolBar, BorderLayout.PAGE_START);
        final JList<GeometricalObject> geoObjectsList =
                new JList<GeometricalObject>(drawingObjectList);
        geoObjectsList.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(final MouseEvent e) {
                @SuppressWarnings("unchecked")
                final JList<GeometricalObject> list = (JList<GeometricalObject>) e.getSource();
                if (e.getClickCount() == 2) {
                    final int index = list.locationToIndex(e.getPoint());
                    final GeometricalObject object = list.getModel().getElementAt(index);
                    if (object instanceof Line) {
                        ((Line) object).showModifyDialog(JVDraw.this);
                    } else if (object instanceof Circle) {
                        ((Circle) object).showModifyDialog(JVDraw.this);
                    } else if (object instanceof FilledCircle) {
                        ((FilledCircle) object).showModifyDialog(JVDraw.this);
                    }
                }
            }
        });
        final JScrollPane scroolPane =
                new JScrollPane(geoObjectsList, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                        JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        scroolPane.setPreferredSize(JLIST_PREFERRED_SIZE);
        getContentPane().add(scroolPane, BorderLayout.LINE_END);
        getContentPane().add(statusBar, BorderLayout.PAGE_END);
        getContentPane().add(drawingCanvas, BorderLayout.CENTER);
    }

    /**
     * This method initialize tool bar for this {@link JVDraw}.
     */
    private void initToolBar() {
        toolBar = new JToolBar();
        foreground = new JColorArea(Color.BLACK);
        background = new JColorArea(Color.WHITE);
        toolBar.setName("Tools");
        toolBar.setFloatable(true);
        toolBar.add(foreground);
        toolBar.addSeparator();
        toolBar.add(background);
        toolBar.addSeparator();
        drawTools = new ButtonGroup();

        final JToggleButton mousePointer = new JToggleButton();
        mousePointer.setName("pointer");
        mousePointer.setIcon(new ImageIcon("src/main/resources/mousePointer.png"));

        final JToggleButton line = new JToggleButton("line");
        line.setName("line");
        line.setIcon(new ImageIcon("src/main/resources/blueLine.png"));

        final JToggleButton circle = new JToggleButton("circle");
        circle.setName("circle");
        circle.setIcon(new ImageIcon("src/main/resources/circle.png"));

        final JToggleButton filledCircle = new JToggleButton("filled circle");
        filledCircle.setName("filled circle");
        filledCircle.setIcon(new ImageIcon("src/main/resources/fcircle.png"));

        drawTools.add(mousePointer);
        drawTools.add(line);
        drawTools.add(circle);
        drawTools.add(filledCircle);
        toolBar.add(mousePointer);
        toolBar.add(line);
        toolBar.add(circle);
        toolBar.add(filledCircle);
    }

    /**
     * This method initialize status bar for this {@link JVDraw}.
     */
    private void initStatusBar() {
        statusBar = new JPanel();
        final CountLabel foregroundLabel = new CountLabel(FOREGROUND);
        final CountLabel backgroundLabel = new CountLabel(BACKGROUND);
        foreground.addColorChangeListener(foregroundLabel);
        background.addColorChangeListener(backgroundLabel);
        statusBar.add(foregroundLabel);
        statusBar.add(backgroundLabel);
    }

    /**
     * Getter for {@link DrawingModel} of this {@link JVDraw}.
     *
     * @return {@link DrawingModel} object.
     */
    public DrawingModel getDrawingModel() {
        return drawingModel;
    }

    /**
     * Getter for currently opened file.
     *
     * @return {@link Path} of currently opened file
     */
    public Path getOpenedFile() {
        return openedFile;
    }

    /**
     * Setter for {@link Path} of currently opened file.
     *
     * @param openedFile
     *            {@link Path} to set
     */
    public void setOpenedFile(final Path openedFile) {
        this.openedFile = openedFile;
    }

    /**
     * This method is called when program starts.
     *
     * @param args
     *            command line arguments
     */
    public static void main(final String[] args) {
        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.gtk.GTKLookAndFeel");
        } catch (final Exception ignorable) {
            System.err.println("Nonexistent L&F.");
        }
        SwingUtilities.invokeLater(() -> {
            new JVDraw().setVisible(true);
        });
    }

}

package hr.fer.zemris.java.hw12.jvdraw.geometricalobjects;

import hr.fer.zemris.java.hw12.jvdraw.JVDraw;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.Point;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * <p>
 * This class represents filled circle.
 * </p>
 * Filled circle is defined by center point, radius, outline color and fill color. Each
 * {@link FilledCircle} has its identification number.
 *
 * @author Stipan Mikulić
 * @version 1.0
 */
public class FilledCircle extends GeometricalObject {
    /**
     * Generic name for {@link FilledCircle}.
     */
    private static final String NAME = "filled circle";
    /**
     * Center of this {@link FilledCircle}.
     */
    private final Point center;
    /**
     * Radius of this {@link FilledCircle}.
     */
    private int radius;
    /**
     * Outline color of this {@link FilledCircle}.
     */
    private Color outlineColor;
    /**
     * Fill color of this {@link FilledCircle}.
     */
    private Color fillColor;

    /**
     * Creates new instance of {@link FilledCircle}.
     *
     * @param number
     *            identification number for this {@link FilledCircle}
     * @param center
     *            center of this {@link FilledCircle}
     * @param radius
     *            radius of this {@link FilledCircle}
     * @param outlineColor
     *            outline color of this {@link FilledCircle}
     * @param fillColor
     *            fill color of this {@link FilledCircle}
     */
    public FilledCircle(final int number, final Point center, final int radius,
            final Color outlineColor, final Color fillColor) {
        super(NAME + " " + number);
        this.center = center;
        this.radius = radius;
        this.outlineColor = outlineColor;
        this.fillColor = fillColor;
    }

    @Override
    public void paint(final Graphics2D g2d) {
        g2d.setColor(fillColor);
        g2d.fillOval(center.x - radius, center.y - radius, 2 * radius, 2 * radius);
        g2d.setColor(outlineColor);
        g2d.drawOval(center.x - radius, center.y - radius, 2 * radius, 2 * radius);
    }

    @Override
    public void showModifyDialog(final JVDraw frame) {
        final JPanel panel = new JPanel(new GridLayout(5, 2));
        addPair("Center-x", panel);
        addPair("Center-y", panel);
        addPair("Radius", panel);
        addPair("Outline color(r,g,b)", panel);
        addPair("Fill color(r,g,b)", panel);
        final int result =
                JOptionPane.showConfirmDialog(frame, panel,
                        "Modify filled circle.  Leave empty to not modify attribute.",
                        JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
        if (result == JOptionPane.OK_OPTION) {
            boolean hasChanged = false;
            final int xOfCenterPointInput = 1;
            final int yOfCenterPointInput = 3;
            final int radiusInput = 5;
            final int outlineColorInput = 7;
            final int fillColorInput = 9;
            try {
                final String centerX =
                        ((JTextField) panel.getComponent(xOfCenterPointInput)).getText();
                if (!centerX.isEmpty()) {
                    center.x = Integer.parseInt(centerX);
                    hasChanged = true;
                }
                final String centerY =
                        ((JTextField) panel.getComponent(yOfCenterPointInput)).getText();
                if (!centerY.isEmpty()) {
                    center.y = Integer.parseInt(centerY);
                    hasChanged = true;
                }
                final String radiusString =
                        ((JTextField) panel.getComponent(radiusInput)).getText();
                if (!radiusString.isEmpty()) {
                    radius = Integer.parseInt(radiusString);
                    hasChanged = true;
                }
                String rgbString = ((JTextField) panel.getComponent(outlineColorInput)).getText();
                if (!rgbString.isEmpty()) {
                    final String[] rgb = rgbString.split(",");
                    outlineColor =
                            new Color(Integer.parseInt(rgb[0]), Integer.parseInt(rgb[1]),
                                    Integer.parseInt(rgb[2]));
                    hasChanged = true;
                }
                rgbString = ((JTextField) panel.getComponent(fillColorInput)).getText();
                if (!rgbString.isEmpty()) {
                    final String[] rgb = rgbString.split(",");
                    fillColor =
                            new Color(Integer.parseInt(rgb[0]), Integer.parseInt(rgb[1]),
                                    Integer.parseInt(rgb[2]));
                    hasChanged = true;
                }
                frame.getDrawingModel().setChanged(hasChanged);
            } catch (final Exception e) {
                JOptionPane.showMessageDialog(frame, "Incorrectly filled form!");
            }
        }
    }

    /**
     * Center {@link Point} for this {@link FilledCircle}.
     *
     * @return center {@link Point}
     */
    public Point getCenter() {
        return center;
    }

    /**
     * Radius for this {@link FilledCircle}.
     *
     * @return radius
     */
    public int getRadius() {
        return radius;
    }

    /**
     * Outline color for this {@link FilledCircle}.
     *
     * @return outline color
     */
    public Color getOutlineColor() {
        return outlineColor;
    }

    /**
     * Fill color for this {@link FilledCircle}.
     *
     * @return fill color
     */
    public Color getFillColor() {
        return fillColor;
    }

    @Override
    public int getMinX() {
        return center.x - radius;
    }

    @Override
    public int getMinY() {
        return center.y - radius;
    }

    @Override
    public int getMaxX() {
        return center.x + radius;
    }

    @Override
    public int getMaxY() {
        return center.y + radius;
    }

    @Override
    public String getInformation() {
        return "FCIRCLE " + center.x + " " + center.y + " " + radius + " " + outlineColor.getRed()
                + " " + outlineColor.getGreen() + " " + outlineColor.getBlue() + " "
                + fillColor.getRed() + " " + fillColor.getGreen() + " " + fillColor.getBlue()
                + "\n";
    }
}

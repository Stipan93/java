package hr.fer.zemris.java.hw12.jvdraw;

import hr.fer.zemris.java.hw12.jvdraw.geometricalobjects.Circle;
import hr.fer.zemris.java.hw12.jvdraw.geometricalobjects.FilledCircle;
import hr.fer.zemris.java.hw12.jvdraw.geometricalobjects.GeometricalObject;
import hr.fer.zemris.java.hw12.jvdraw.geometricalobjects.Line;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;

/**
 * <p>
 * This class represents menu bar for {@link JVDraw} application.
 * </p>
 * This menu bar has only one menu named file. That menu supports next actions: open, save, save as,
 * export and exit.
 *
 * @author Stipan Mikulić
 * @version 1..0
 */
public class JVMenuBar extends JMenuBar {
    /**
     * Default serial ID.
     */
    private static final long serialVersionUID = 1L;
    /**
     * Reference to {@link JVDraw} frame.
     */
    private final JVDraw frame;
    /**
     * {@link DrawingModel} object.
     */
    private final DrawingModel model;

    /**
     * Creates new {@link JVMenuBar} for {@link JVDraw} frame.
     *
     * @param frame
     *            {@link JVDraw} object
     */
    public JVMenuBar(final JVDraw frame) {
        this.frame = frame;
        model = frame.getDrawingModel();
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(final WindowEvent e) {
                super.windowClosing(e);
                exitAction.actionPerformed(null);
            }
        });
        final JMenu file = new JMenu("File");
        openAction.putValue(Action.NAME, "Open...");
        saveAction.putValue(Action.NAME, "Save");
        saveAsAction.putValue(Action.NAME, "Save as...");
        exportAction.putValue(Action.NAME, "Export...");
        exitAction.putValue(Action.NAME, "Exit");
        file.add(openAction);
        file.add(saveAction);
        file.add(saveAsAction);
        file.addSeparator();
        file.add(exportAction);
        file.addSeparator();
        file.add(exitAction);
        add(file);
    }

    /**
     * Open action for opening new files that contains definitions of objects that are drawn on
     * canvas.
     */
    private final Action openAction = new AbstractAction() {
        /**
         * Default serial ID.
         */
        private static final long serialVersionUID = 1L;

        @Override
        public void actionPerformed(final ActionEvent e) {
            final JFileChooser fc = new JFileChooser();
            fc.setDialogTitle("Open file");
            if (fc.showOpenDialog(frame) != JFileChooser.APPROVE_OPTION) {
                return;
            }
            final Path file = fc.getSelectedFile().toPath();
            if (!Files.isReadable(file)) {
                JOptionPane.showMessageDialog(frame, "Selected file (" + file
                        + ") is not readable!", "Error", JOptionPane.ERROR_MESSAGE);
                return;
            }
            if (model.getSize() > 0) {
                saveAction.actionPerformed(e);
            }
            model.clear();
            try {
                final byte[] bytes = Files.readAllBytes(file);
                parseFile(new String(bytes, StandardCharsets.UTF_8));
                frame.setOpenedFile(file);
            } catch (final IOException e1) {
                JOptionPane.showMessageDialog(frame,
                        "Error while reading file " + file + " :" + e1.getMessage(), "Error",
                        JOptionPane.ERROR_MESSAGE);
                return;
            }
        }

        /**
         * This method parses textual definition of objects into {@link GeometricalObject}
         * instances.
         *
         * @param file
         *            file where definitions of objects are stored
         */
        private void parseFile(final String file) {
            final String[] lines = file.split("\n");
            for (final String line : lines) {
                final String[] arguments = line.split("\\s+");
                if (arguments[0].trim().equals("LINE")) {
                    final int startX = Integer.parseInt(arguments[1]);
                    final int startY = Integer.parseInt(arguments[2]);
                    final int endX = Integer.parseInt(arguments[3]);
                    final int endY = Integer.parseInt(arguments[4]);
                    final int r = Integer.parseInt(arguments[5]);
                    final int g = Integer.parseInt(arguments[6]);
                    final int b = Integer.parseInt(arguments[7]);
                    model.add(new Line(model.getNumberOfLines(), new Point(startX, startY),
                            new Point(endX, endY), new Color(r, g, b)));
                } else if (arguments[0].trim().equals("CIRCLE")
                        || arguments[0].trim().equals("FCIRCLE")) {
                    final int centerX = Integer.parseInt(arguments[1]);
                    final int centerY = Integer.parseInt(arguments[2]);
                    final int radius = Integer.parseInt(arguments[3]);
                    final int r = Integer.parseInt(arguments[4]);
                    final int g = Integer.parseInt(arguments[5]);
                    final int b = Integer.parseInt(arguments[6]);
                    if (arguments[0].trim().equals("CIRCLE")) {
                        model.add(new Circle(model.getNumberOfCircles(),
                                new Point(centerX, centerY), radius, new Color(r, g, b)));
                    } else {
                        final int rFill = Integer.parseInt(arguments[7]);
                        final int gFill = Integer.parseInt(arguments[8]);
                        final int bFill = Integer.parseInt(arguments[9]);
                        model.add(new FilledCircle(model.getNumberOfFilledCircles(), new Point(
                                centerX, centerY), radius, new Color(r, g, b), new Color(rFill,
                                gFill, bFill)));
                    }
                }
            }
        }
    };

    /**
     * Save action that saves currently drawn objects into file.
     */
    private final Action saveAction = new AbstractAction() {
        /**
         * Default serial ID.
         */
        private static final long serialVersionUID = 1L;

        @Override
        public void actionPerformed(final ActionEvent e) {
            Path file;
            if (frame.getOpenedFile() == null) {
                final JFileChooser fc = new JFileChooser();
                fc.setDialogTitle("Save document");
                if (fc.showSaveDialog(frame) != JFileChooser.APPROVE_OPTION) {
                    informationalDialog("Message", "Nothing was saved.");
                    return;
                }
                file = fc.getSelectedFile().toPath();
                if (Files.exists(file)) {
                    final int rez = openOverwriteDialog(file);
                    if (rez != JOptionPane.YES_OPTION) {
                        return;
                    }
                }
                frame.setOpenedFile(file);
            } else {
                file = frame.getOpenedFile();
            }
            try {
                parseImageToFile(file);
                model.setChanged(false);
            } catch (final IOException e1) {
                errorWhileWriteingToFile(e1);
                return;
            }
        }


    };

    /**
     * Save as action that saves currently drawn objects into file by new specified name.
     */
    private final Action saveAsAction = new AbstractAction() {
        /**
         * Default serial ID.
         */
        private static final long serialVersionUID = 1L;

        @Override
        public void actionPerformed(final ActionEvent e) {
            final JFileChooser fc = new JFileChooser();
            fc.setDialogTitle("Save document as");
            if (fc.showSaveDialog(frame) != JFileChooser.APPROVE_OPTION) {
                informationalDialog("Message", "Nothing was saved.");
                return;
            }
            final Path file = fc.getSelectedFile().toPath();
            if (Files.exists(file)) {
                final int rez = openOverwriteDialog(file);
                if (rez != JOptionPane.YES_OPTION) {
                    return;
                }
            }
            frame.setOpenedFile(file);
            try {
                parseImageToFile(file);
                model.setChanged(false);
            } catch (final IOException e1) {
                errorWhileWriteingToFile(e1);
                return;
            }
        }
    };

    /**
     * This method parses currently drawn objects on canvas to file.
     *
     * @param file
     *            {@link File} where definitions of drawn objects need to be stored
     * @throws IOException
     *             if error while writing to file occurred
     */
    private void parseImageToFile(final Path file) throws IOException {
        final StringBuilder builder = new StringBuilder();
        for (int i = 0, size = model.getSize(); i < size; i++) {
            final GeometricalObject object = model.getObject(i);
            builder.append(object.getInformation());
        }
        Files.write(file, builder.toString().getBytes(StandardCharsets.UTF_8));
    }

    /**
     * Export {@link Action} that exports currently drawn objects to image.
     */
    private final Action exportAction = new AbstractAction() {
        /**
         * Default serial ID.
         */
        private static final long serialVersionUID = 1L;

        @Override
        public void actionPerformed(final ActionEvent e) {
            final String[] options = new String[] {"png", "jpg", "gif"};
            final int response =
                    JOptionPane.showOptionDialog(null, "Message", "Choose extension",
                            JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE, null, options,
                            options[0]);
            if (response == JOptionPane.CLOSED_OPTION) {
                return;
            }
            final JFileChooser fc = new JFileChooser();
            fc.setDialogTitle("Save document");
            if (fc.showSaveDialog(frame) != JFileChooser.APPROVE_OPTION) {
                informationalDialog("Message", "Nothing was saved.");
                return;
            }
            final File file = fc.getSelectedFile();
            if (Files.exists(file.toPath())) {
                final int rez = openOverwriteDialog(file.toPath());
                if (rez != JOptionPane.YES_OPTION) {
                    return;
                }
            }
            int minX = Integer.MAX_VALUE;
            int minY = Integer.MAX_VALUE;
            int maxX = 0;
            int maxY = 0;
            if (model.getSize() == 0) {
                return;
            }
            for (int index = 0, size = model.getSize(); index < size; index++) {
                final GeometricalObject object = model.getObject(index);
                if (minX > object.getMinX()) {
                    minX = object.getMinX();
                }
                if (minY > object.getMinY()) {
                    minY = object.getMinY();
                }
                if (maxX < object.getMaxX()) {
                    maxX = object.getMaxX();
                }
                if (maxY < object.getMaxY()) {
                    maxY = object.getMaxY();
                }
            }
            final BufferedImage image =
                    new BufferedImage(maxX - minX, maxY - minY, BufferedImage.TYPE_3BYTE_BGR);
            final Graphics2D g = image.createGraphics();
            g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            g.translate(-minX, -minY);
            for (int i = 0, size = model.getSize(); i < size; i++) {
                model.getObject(i).paint(g);
            }
            g.dispose();
            try {
                ImageIO.write(image, options[response], file);
            } catch (final IOException e1) {
                informationalDialog("Message", "Error while exporting file " + file);
            }
            informationalDialog("Message", "Exported to file " + file);
        }
    };

    /**
     * Exit {@link Action} that exits application.
     */
    private final Action exitAction = new AbstractAction() {
        /**
         * Default serial ID.
         */
        private static final long serialVersionUID = 1L;

        @Override
        public void actionPerformed(final ActionEvent e) {
            if (frame.getDrawingModel().isChanged()) {
                final int response =
                        JOptionPane
                                .showConfirmDialog(frame, "Do you want to save current drawing?");
                if (response == JOptionPane.CANCEL_OPTION) {
                    return;
                } else if (response == JOptionPane.YES_OPTION) {
                    saveAction.actionPerformed(null);
                }
            }
            frame.dispose();
        }
    };

    /**
     * This method shows information in new message dialog.
     *
     * @param title
     *            title for message dialog
     * @param text
     *            message that shows in message dialog
     */
    private void informationalDialog(final String title, final String text) {
        JOptionPane.showMessageDialog(frame, text, title, JOptionPane.INFORMATION_MESSAGE);
    }

    /**
     * This method show confirm dialog which ask user to overwrite existing file.
     *
     * @param file
     *            file to overwrite
     * @return users choice, yes or no option from confirm dialog
     */
    private int openOverwriteDialog(final Path file) {
        return JOptionPane.showConfirmDialog(frame, "Selected file (" + file
                + ") already exists. Do you want to overwrite it?", "Warning",
                JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);

    }

    /**
     * This method shows message dialog when error while writing to a file occurred.
     *
     * @param e1
     *            {@link Exception} for which message is printed
     */
    private void errorWhileWriteingToFile(final IOException e1) {
        JOptionPane.showMessageDialog(frame, "Error while writing to file " + frame.getOpenedFile()
                + " :" + e1.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
    }
}

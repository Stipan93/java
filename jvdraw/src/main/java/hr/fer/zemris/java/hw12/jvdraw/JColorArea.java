package hr.fer.zemris.java.hw12.jvdraw;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JColorChooser;
import javax.swing.JComponent;

/**
 * <p>
 * This class represents object that can change color.
 * </p>
 * Selected color is used to draw objects on {@link JDrawingCanvas}.
 *
 * @author Stipan Mikulić
 * @version 1.0
 */
public class JColorArea extends JComponent implements IColorProvider {
    /**
     * Default serial ID.
     */
    private static final long serialVersionUID = 1L;
    /**
     * Dimension for {@link JColorArea}.
     */
    private static final int DIMENSION = 15;
    /**
     * Selected color storage.
     */
    private Color selectedColor;
    /**
     * Old color storage.
     */
    private Color oldColor;
    /**
     * {@link List} of {@link ColorChangeListener} objects.
     */
    private final List<ColorChangeListener> colorChangeListeners;

    /**
     * Creates new {@link JColorArea}.
     *
     * @param color
     *            initial color to set
     */
    public JColorArea(final Color color) {
        selectedColor = color;
        oldColor = color;
        setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY));
        colorChangeListeners = new ArrayList<ColorChangeListener>();
    }

    @Override
    public Dimension getMaximumSize() {
        return new Dimension(DIMENSION, DIMENSION);
    }

    @Override
    public Dimension getMinimumSize() {
        return new Dimension(DIMENSION, DIMENSION);
    }

    @Override
    public Dimension getSize() {
        return new Dimension(DIMENSION, DIMENSION);
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(DIMENSION, DIMENSION);
    }

    @Override
    protected void paintComponent(final Graphics g) {
        g.setColor(selectedColor);
        g.fillRect(0, 0, DIMENSION, DIMENSION);
    }

    @Override
    public Color getCurrentColor() {
        return selectedColor;
    }

    /**
     * This method adds specified {@link ColorChangeListener} given as argument to listeners list.
     *
     * @param listener
     *            {@link ColorChangeListener} to add
     */
    public void addColorChangeListener(final ColorChangeListener listener) {
        colorChangeListeners.add(listener);
    }

    /**
     * This method removes specified {@link ColorChangeListener} given as argument from listeners
     * list.
     *
     * @param listener
     *            {@link ColorChangeListener} to remove
     */
    public void removeColorChangeListener(final ColorChangeListener listener) {
        colorChangeListeners.remove(listener);
    }

    /**
     * Notifies all registered {@link ColorChangeListener} objects that new color is selected.
     */
    public void fire() {
        for (final ColorChangeListener listener : colorChangeListeners) {
            listener.newColorSelected(this, oldColor, selectedColor);
        }
    }

    // add mouse listener to this JColorArea
    {
        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(final MouseEvent e) {
                final Color newColor =
                        JColorChooser.showDialog(JColorArea.this, "Choose Background Color",
                                selectedColor);
                if (newColor != null) {
                    oldColor = selectedColor;
                    selectedColor = newColor;
                }
                repaint();
                fire();
            }
        });
    }
}

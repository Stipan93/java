package hr.fer.zemris.java.hw12.jvdraw;

import java.awt.Color;

import javax.swing.JLabel;

/**
 * <p>
 * This class represents status bar.
 * </p>
 * It contains rgb values for foreground and background color.
 *
 * @author Stipan Mikulić
 * @version 1.0
 */
public class CountLabel extends JLabel implements ColorChangeListener {
    /**
     * Default serial ID.
     */
    private static final long serialVersionUID = 1L;
    /**
     * Text of this {@link CountLabel}.
     */
    private final String text;

    /**
     * Creates new {@link CountLabel} with initial text.
     *
     * @param text
     *            initial text for this label
     */
    public CountLabel(final String text) {
        this.text = text;
        final Color color;
        if (text.startsWith("Fore")) {
            color = Color.BLACK;
        } else {
            color = Color.WHITE;
        }
        setText(text + "(" + color.getRed() + ", " + color.getGreen() + ", " + color.getBlue()
                + "),");
    }

    @Override
    public void newColorSelected(final IColorProvider source, final Color oldColor,
            final Color newColor) {
        setText(text + "(" + newColor.getRed() + ", " + newColor.getGreen() + ", "
                + newColor.getBlue() + ")");
    }

}

package hr.fer.zemris.java.hw12.jvdraw;

/**
 * <p>
 * This interface provides methods which every observer on {@link DrawingModel} must implement.
 * </p>
 *
 * @author Stipan Mikulić
 * @version 1.0
 */
public interface DrawingModelListener {
    /**
     * This method is called when objects are added to {@link DrawingModel}.
     *
     * @param source
     *            {@link DrawingModel} object
     * @param index0
     *            start index of added objects
     * @param index1
     *            end index of added objects
     */
    void objectsAdded(DrawingModel source, int index0, int index1);

    /**
     * This method is called when objects are removed from {@link DrawingModel}.
     *
     * @param source
     *            {@link DrawingModel} object
     * @param index0
     *            start index of removed objects
     * @param index1
     *            end index of removed objects
     */
    void objectsRemoved(DrawingModel source, int index0, int index1);

    /**
     * This method is called when objects are changed in {@link DrawingModel}.
     *
     * @param source
     *            {@link DrawingModel} object
     * @param index0
     *            start index of changed objects
     * @param index1
     *            end index of changed objects
     */
    void objectsChanged(DrawingModel source, int index0, int index1);
}

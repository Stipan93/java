package hr.fer.zemris.java.hw12.jvdraw.geometricalobjects;

import hr.fer.zemris.java.hw12.jvdraw.JVDraw;

import java.awt.Graphics2D;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;


/**
 * <p>
 * This class represents geometric object.
 * </p>
 * Each {@link GeometricalObject} has its name. It also provides abstract methods that needs to be
 * implemented: {@link GeometricalObject#showModifyDialog(JVDraw)} and
 * {@link GeometricalObject#paint(Graphics2D)}.
 *
 * @author Stipan Mikulić
 * @version 1.0
 */
public abstract class GeometricalObject {
    /**
     * Name of geometric object.
     */
    private final String name;

    /**
     * Creates new instance of {@link GeometricalObject} with given name.
     *
     * @param name
     *            name of {@link GeometricalObject}
     */
    public GeometricalObject(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

    /**
     * This method paints {@link GeometricalObject} on canvas whose given {@link Graphics2D} object
     * is.
     *
     * @param g2d
     *            {@link Graphics2D} object
     */
    public abstract void paint(Graphics2D g2d);

    /**
     * This method creates and shows dialog for modification of {@link GeometricalObject}.
     *
     * @param frame
     *            parent component for created dialog
     */
    public abstract void showModifyDialog(JVDraw frame);

    /**
     * This method adds new field in form. Description of input is defined by {@link JLabel} and
     * input is entered in {@link JTextField}.
     *
     * @param message
     *            description of input
     * @param panel
     *            {@link JPanel} to which field is added
     */
    protected void addPair(final String message, final JPanel panel) {
        panel.add(new JLabel(message));
        panel.add(new JTextField());
    }

    /**
     * This method returns minimal x coordinate for {@link GeometricalObject}.
     *
     * @return minimal x coordinate
     */
    public abstract int getMinX();

    /**
     * This method returns minimal y coordinate for {@link GeometricalObject}.
     *
     * @return minimal y coordinate
     */
    public abstract int getMinY();

    /**
     * This method returns maximal x coordinate for {@link GeometricalObject}.
     *
     * @return maximal x coordinate
     */
    public abstract int getMaxX();

    /**
     * This method returns maximal y coordinate for {@link GeometricalObject}.
     *
     * @return maximal y coordinate
     */
    public abstract int getMaxY();

    /**
     * This method returns {@link String} that contains all needed information about
     * {@link GeometricalObject}.
     * 
     * @return information in {@link String} about {@link GeometricalObject}
     */
    public abstract String getInformation();
}

package hr.fer.zemris.java.hw12.jvdraw.geometricalobjects;

import hr.fer.zemris.java.hw12.jvdraw.JVDraw;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.Point;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * <p>
 * This class represents line.
 * </p>
 * Line is defined by start point, end point and color. Each {@link Line} has its identification
 * number.
 *
 * @author Stipan Mikulić
 * @version 1.0
 */
public class Line extends GeometricalObject {
    /**
     * Line's generic name.
     */
    private static final String NAME = "line";
    /**
     * Start {@link Point} of this line.
     */
    private final Point start;
    /**
     * End {@link Point} of this line.
     */
    private final Point end;
    /**
     * Color of this line.
     */
    private Color color;

    /**
     * Creates new instance of {@link Line}.
     *
     * @param number
     *            line's number
     * @param start
     *            line's start {@link Point}
     * @param end
     *            line's end {@link Point}
     * @param color
     *            line's {@link Color}
     */
    public Line(final int number, final Point start, final Point end, final Color color) {
        super(NAME + " " + number);
        this.start = start;
        this.end = end;
        this.color = color;
    }

    @Override
    public void paint(final Graphics2D g2d) {
        g2d.setColor(color);
        g2d.drawLine(start.x, start.y, end.x, end.y);
    }

    @Override
    public void showModifyDialog(final JVDraw frame) {
        final JPanel panel = new JPanel(new GridLayout(5, 2));
        addPair("Start-x", panel);
        addPair("Start-y", panel);
        addPair("End-x", panel);
        addPair("End-y", panel);
        addPair("Color(r,g,b)", panel);
        final int result =
                JOptionPane.showConfirmDialog(frame, panel,
                        "Modify line. Leave empty to not modify attribute.",
                        JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
        if (result == JOptionPane.OK_OPTION) {
            boolean hasChanged = false;
            final int xOfStartPointInput = 1;
            final int yOfStartPointInput = 3;
            final int xOfEndPointInput = 5;
            final int yOfEndPointInput = 7;
            final int colorOfLineInput = 9;
            try {
                final String startX =
                        ((JTextField) panel.getComponent(xOfStartPointInput)).getText();
                if (!startX.isEmpty()) {
                    start.x = Integer.parseInt(startX);
                    hasChanged = true;
                }
                final String startY =
                        ((JTextField) panel.getComponent(yOfStartPointInput)).getText();
                if (!startY.isEmpty()) {
                    start.y = Integer.parseInt(startY);
                    hasChanged = true;
                }
                final String endX = ((JTextField) panel.getComponent(xOfEndPointInput)).getText();
                if (!endX.isEmpty()) {
                    end.x = Integer.parseInt(endX);
                    hasChanged = true;
                }
                final String endY = ((JTextField) panel.getComponent(yOfEndPointInput)).getText();
                if (!endY.isEmpty()) {
                    end.y = Integer.parseInt(endY);
                    hasChanged = true;
                }
                final String rgbString =
                        ((JTextField) panel.getComponent(colorOfLineInput)).getText();
                if (!rgbString.isEmpty()) {
                    final String[] rgb = rgbString.split(",");
                    color =
                            new Color(Integer.parseInt(rgb[0]), Integer.parseInt(rgb[1]),
                                    Integer.parseInt(rgb[2]));
                    hasChanged = true;
                }
                frame.getDrawingModel().setChanged(hasChanged);
            } catch (final Exception e) {
                JOptionPane.showMessageDialog(frame, "Incorrectly filled form!");
            }
        }
    }

    /**
     * Getter for start {@link Point} of this line.
     *
     * @return start {@link Point}
     */
    public Point getStart() {
        return start;
    }

    /**
     * Getter for end {@link Point} for this line.
     *
     * @return end {@link Point}
     */
    public Point getEnd() {
        return end;
    }

    /**
     * Getter for line color.
     *
     * @return color of line
     */
    public Color getColor() {
        return color;
    }

    @Override
    public int getMinX() {
        return start.x < end.x ? start.x : end.x;
    }

    @Override
    public int getMinY() {
        return start.y < end.y ? start.y : end.y;
    }

    @Override
    public int getMaxX() {
        return start.x > end.x ? start.x : end.x;
    }

    @Override
    public int getMaxY() {
        return start.y > end.y ? start.y : end.y;
    }

    @Override
    public String getInformation() {
        return "LINE " + start.x + " " + start.y + " " + end.x + " " + end.y + " " + color.getRed()
                + " " + color.getGreen() + " " + color.getBlue() + "\n";
    }
}

package hr.fer.zemris.java.hw12.jvdraw;

import java.awt.Color;

/**
 * <p>
 * This interface obligates class that implements it to provide method for getting {@link Color}.
 * </p>
 * It represents objects that have options for selecting {@link Color}.
 *
 * @author Stipan Mikulić
 * @version 1.0
 */
public interface IColorProvider {
    /**
     * This method is getter for currently selected {@link Color}.
     *
     * @return current {@link Color}
     */
    Color getCurrentColor();
}

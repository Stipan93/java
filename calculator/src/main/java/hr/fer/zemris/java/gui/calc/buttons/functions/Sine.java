package hr.fer.zemris.java.gui.calc.buttons.functions;

import hr.fer.zemris.java.gui.calc.CalcUtility;

/**
 * <p>
 * This class represents sine function.
 * </p>
 * It makes contract with {@link IFunction} interface and implements needed
 * method for performing sine function.
 * 
 * @author Stipan Mikulić
 * @version 1.0
 * @see AbstractFunction
 */
public class Sine extends AbstractFunction {
	private static final long serialVersionUID = 1L;
	/**
	 * Function name
	 */
	private static final String NAME = "sin";

	/**
	 * Creates new {@link Sine} function button.
	 * 
	 * @param calcUtil
	 *            calculator utility
	 */
	public Sine(CalcUtility calcUtil) {
		super(calcUtil);
		setText(NAME);
		setName(NAME);
	}

	@Override
	public double accept(double arg) {
		return Math.sin(arg);
	}
}

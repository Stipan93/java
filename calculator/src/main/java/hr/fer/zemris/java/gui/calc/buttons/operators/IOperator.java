package hr.fer.zemris.java.gui.calc.buttons.operators;

/**
 * This interface makes contract with class which implements it and it provides
 * method which defines algorithm for implemented operation. Concrete strategy
 * class needs to implement method {@link #accept(double, double)} in its
 * appropriate way.
 * 
 * @author Stipan Mikulić
 * @version 1.0
 */
public interface IOperator {
	/**
	 * This method calculates defined operation on two given arguments.
	 * 
	 * @param arg1
	 *            first operand
	 * @param arg2
	 *            second operand
	 * @return result of operation
	 */
	public double accept(double arg1, double arg2);
}

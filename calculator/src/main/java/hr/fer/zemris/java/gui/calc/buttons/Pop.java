package hr.fer.zemris.java.gui.calc.buttons;

import java.awt.event.ActionEvent;

import hr.fer.zemris.java.gui.calc.CalcUtility;

import javax.swing.JButton;

/**
 * <p>
 * This class represents pop button.
 * </p>
 * When is clicked this button pops number from stack and print it on calculator
 * screen.
 * 
 * @author Stipan Mikulić
 * @version 1.0
 */
public class Pop extends JButton {
	private static final long serialVersionUID = 1L;
	/**
	 * Button name
	 */
	private static final String NAME = "pop";

	/**
	 * Creates new {@link Pop} button.
	 * 
	 * @param calcUtil
	 *            calculator utility
	 */
	public Pop(CalcUtility calcUtil) {
		setText(NAME);
		setName(NAME);
		this.addActionListener((ActionEvent e) -> {
			if (calcUtil.getStack().isEmpty()) {
				calcUtil.getScreen().setText("Stack is empty!");
			} else {
				calcUtil.getScreen().setText(calcUtil.getStack().pop());
			}
		});
	}
}

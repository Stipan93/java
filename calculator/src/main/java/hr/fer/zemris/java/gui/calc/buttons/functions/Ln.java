package hr.fer.zemris.java.gui.calc.buttons.functions;

import hr.fer.zemris.java.gui.calc.CalcUtility;

/**
 * <p>
 * This class represents natural logarithmic function.
 * </p>
 * It makes contract with {@link IFunction} interface and implements needed
 * method for performing natural logarithmic function.
 * 
 * @author Stipan Mikulić
 * @version 1.0
 * @see AbstractFunction
 */
public class Ln extends AbstractFunction {
	private static final long serialVersionUID = 1L;
	/**
	 * Function name
	 */
	private static final String NAME = "ln";

	/**
	 * Creates new {@link Ln} function button.
	 * 
	 * @param calcUtil
	 *            calculator utility
	 */
	public Ln(CalcUtility calcUtil) {
		super(calcUtil);
		setText(NAME);
		setName(NAME);
	}

	@Override
	public double accept(double arg) {
		return Math.log(arg);
	}

}

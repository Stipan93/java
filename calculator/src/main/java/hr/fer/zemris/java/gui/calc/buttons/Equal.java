package hr.fer.zemris.java.gui.calc.buttons;

import hr.fer.zemris.java.gui.calc.CalcUtility;

import javax.swing.JButton;

/**
 * <p>
 * This class represents equal button.
 * </p>
 * When is clicked this button performs stored operation on two operands.
 * 
 * @author Stipan Mikulić
 * @version 1.0
 */
public class Equal extends JButton {
	private static final long serialVersionUID = 1L;
	/**
	 * Button name
	 */
	private static final String NAME = "=";

	/**
	 * Creates new {@link Equal} button.
	 * 
	 * @param calcUtil
	 *            calculator utility
	 */
	public Equal(CalcUtility calcUtil) {
		setText(NAME);
		setName(NAME);
		this.addActionListener((e) -> {
			if (calcUtil.getOperator() != null) {
				calcUtil.setSecondOperand(calcUtil.getScreen().getText());
				double result = calcUtil.getOperator().accept(
						Double.parseDouble(calcUtil.getFirstOperand()),
						Double.parseDouble(calcUtil.getSecondOperand()));
				if (result == Double.NEGATIVE_INFINITY
						|| result == Double.POSITIVE_INFINITY) {
					calcUtil.getScreen().setText("Cannot divide by zero!");
				} else {
					calcUtil.getScreen().setText(String.valueOf(result));
				}
				calcUtil.setOperator(null);
			}
		});
	}
}

package hr.fer.zemris.java.gui.calc.buttons.operators;

import hr.fer.zemris.java.gui.calc.CalcUtility;

/**
 * <p>
 * This class represent <code>x^n</code> operator.
 * </p>
 * It makes contract with {@link IOperator} interface and implements needed
 * method for performing <code>x^n</code> operation.
 * 
 * @author Stipan Mikulić
 * @version 1.0
 * @see AbstractOperator
 */
public class XtoPowerOfN extends AbstractOperator {
	private static final long serialVersionUID = 1L;
	/**
	 * Operation name
	 */
	private static final String NAME = "x^n";

	/**
	 * Creates new {@link XtoPowerOfN} button which provides <code>x^n</code>
	 * operation.
	 * 
	 * @param calcUtil
	 *            calculator utility
	 */
	public XtoPowerOfN(CalcUtility calcUtil) {
		super(calcUtil);
		setText(NAME);
		setName(NAME);
	}

	@Override
	public double accept(double arg1, double arg2) {
		return Math.pow(arg1, arg2);
	}

}

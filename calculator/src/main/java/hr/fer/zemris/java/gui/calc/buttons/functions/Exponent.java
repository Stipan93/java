package hr.fer.zemris.java.gui.calc.buttons.functions;

import hr.fer.zemris.java.gui.calc.CalcUtility;

/**
 * <p>
 * This class represents exponent function.
 * </p>
 * It makes contract with {@link IFunction} interface and implements needed
 * method for performing exponent function.
 * 
 * @author Stipan Mikulić
 * @version 1.0
 * @see AbstractFunction
 */
public class Exponent extends AbstractFunction {
	private static final long serialVersionUID = 1L;
	/**
	 * Function name
	 */
	private static final String NAME = "e^x";

	/**
	 * Creates new {@link Exponent} function button.
	 * 
	 * @param calcUtil
	 *            calculator utility
	 */
	public Exponent(CalcUtility calcUtil) {
		super(calcUtil);
		setText(NAME);
		setName(NAME);
	}

	@Override
	public double accept(double arg) {
		return Math.exp(arg);
	}

}

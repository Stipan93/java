package hr.fer.zemris.java.gui.layouts;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.LayoutManager2;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;

/**
 * <p>
 * This class represents calculator layout.
 * </p>
 * This class provides methods for adding and removing components from this
 * layout. It also provides method for obtaining layout size and alignment.
 * 
 * @author Stipan Mikulić
 * @version 1.0
 * @see LayoutManager2
 */
public class CalcLayout implements LayoutManager2 {
	/**
	 * Default range between components
	 */
	private static final int DEFAULT_RANGE = 0;
	/**
	 * Default alignment
	 */
	private static final float DEFAULT_ALIGNMENT = 0.5f;
	/**
	 * Number of rows
	 */
	private static final int NUMBER_OF_ROWS = 5;
	/**
	 * Number of columns
	 */
	private static final int NUMBER_OF_COLUMNS = 7;
	/**
	 * Range between components
	 */
	private int rangeBetweenComponents;
	/**
	 * {@link Map} where {@link CalcLayout} components are stored.
	 */
	private Map<RCPosition, Component> components = new HashMap<>();

	/**
	 * Creates new {@link CalcLayout} with default range between components.
	 * 
	 * @see CalcLayout#DEFAULT_RANGE
	 */
	public CalcLayout() {
		this(DEFAULT_RANGE);
	}

	/**
	 * Creates new {@link CalcLayout} with given range between components.
	 * 
	 * @param rangeBetweenComponents
	 *            range between components
	 */
	public CalcLayout(int rangeBetweenComponents) {
		super();
		this.rangeBetweenComponents = rangeBetweenComponents;
	}

	@Override
	public void addLayoutComponent(String name, Component comp) {
		String[] args = name.split(",");
		if (args.length != 2) {
			throw new IllegalArgumentException(
					"Constraints must conatin row and column number!");
		}
		int x = Integer.valueOf(args[0]);
		int y = Integer.valueOf(args[1]);
		checkPosition(x, y);
		RCPosition position = new RCPosition(x, y);
		if (components.containsKey(position)) {
			throw new IllegalArgumentException("Cell is occupied!");
		}
		components.put(position, comp);

	}

	@Override
	public void addLayoutComponent(Component comp, Object constraints) {
		if (constraints == null) {
			throw new IllegalArgumentException("Illegal constraint type!");
		}
		if (constraints instanceof String) {
			addLayoutComponent((String) constraints, comp);
		} else if (constraints instanceof RCPosition) {
			addLayoutComponent(new String(((RCPosition) constraints).getRow()
					+ "," + ((RCPosition) constraints).getColumn()), comp);
		} else {
			throw new IllegalArgumentException("Illegal constraint type!");
		}
	}

	@Override
	public void removeLayoutComponent(Component comp) {
		RCPosition positionToDelete = null;
		if (comp instanceof JLabel) {
			positionToDelete = new RCPosition(1, 1);
		} else if (comp instanceof JCheckBox) {
			positionToDelete = new RCPosition(5, 7);
		} else {
			for (Map.Entry<RCPosition, Component> entry : components.entrySet()) {
				if (comp instanceof JButton
						&& entry.getValue() instanceof JButton) {
					JButton value = (JButton) entry.getValue();
					if (((JButton) comp).getText().equals(value.getText())) {
						positionToDelete = entry.getKey();
						break;
					}
				}
			}
		}
		if (positionToDelete != null) {
			components.remove(positionToDelete);
		}
	}

	@Override
	public Dimension preferredLayoutSize(Container parent) {
		return maximumLayoutSize(parent);
	}

	@Override
	public Dimension minimumLayoutSize(Container parent) {
		int width = 0;
		int height = 0;
		Insets insets = parent.getInsets();

		for (Map.Entry<RCPosition, Component> entry : components.entrySet()) {
			RCPosition position = entry.getKey();
			Component comp = entry.getValue();
			Dimension d = comp.getMinimumSize();
			if (position.equals(new RCPosition(1, 1)) || d == null) {
				continue;
			}
			if (width < d.width) {
				width = d.width;
			}
			if (height < d.height) {
				height = d.height;
			}
		}
		return new Dimension(insets.left + insets.right + NUMBER_OF_COLUMNS
				* width + (NUMBER_OF_COLUMNS - 1) * rangeBetweenComponents,
				insets.top + insets.bottom + NUMBER_OF_ROWS * height
						+ (NUMBER_OF_ROWS - 1) * rangeBetweenComponents);
	}

	@Override
	public void layoutContainer(Container parent) {
		Insets insets = parent.getInsets();
		int width = (parent.getWidth() - insets.right - insets.left - rangeBetweenComponents
				* (NUMBER_OF_COLUMNS - 1))
				/ NUMBER_OF_COLUMNS;
		int height = (parent.getHeight() - insets.bottom - insets.top - rangeBetweenComponents
				* (NUMBER_OF_ROWS - 1))
				/ NUMBER_OF_ROWS;
		for (Map.Entry<RCPosition, Component> entry : components.entrySet()) {
			RCPosition position = entry.getKey();
			Component comp = entry.getValue();
			if (position.equals(new RCPosition(1, 1))) {
				comp.setSize(width * 5 + rangeBetweenComponents * 4, height);
				comp.setLocation(insets.left, insets.top);
			} else {
				comp.setSize(width, height);
				comp.setLocation(insets.left + (rangeBetweenComponents + width)
						* (position.getColumn() - 1),
						insets.top + (rangeBetweenComponents + height)
								* (position.getRow() - 1));
			}
		}
	}

	@Override
	public Dimension maximumLayoutSize(Container target) {
		int width = 0;
		int height = 0;
		Insets insets = target.getInsets();

		for (Map.Entry<RCPosition, Component> entry : components.entrySet()) {
			RCPosition position = entry.getKey();
			Component comp = entry.getValue();
			Dimension d = comp.getMaximumSize();
			if (position.equals(new RCPosition(1, 1)) || d == null) {
				continue;
			}
			if (width > d.width) {
				width = d.width;
			}
			if (height > d.height) {
				height = d.height;
			}
		}
		return new Dimension(insets.left + insets.right + NUMBER_OF_COLUMNS
				* width + (NUMBER_OF_COLUMNS - 1) * rangeBetweenComponents,
				insets.top + insets.bottom + NUMBER_OF_ROWS * height
						+ (NUMBER_OF_ROWS - 1) * rangeBetweenComponents);
	}

	@Override
	public float getLayoutAlignmentX(Container target) {
		return DEFAULT_ALIGNMENT;
	}

	@Override
	public float getLayoutAlignmentY(Container target) {
		return DEFAULT_ALIGNMENT;
	}

	@Override
	public void invalidateLayout(Container target) {

	}

	/**
	 * This method checks are given row and column valid for this
	 * {@link CalcLayout}.
	 * 
	 * @param row
	 *            row number
	 * @param column
	 *            column number
	 * @throws IllegalArgumentException
	 *             if illegal row or column is given
	 */
	private void checkPosition(int row, int column)
			throws IllegalArgumentException {
		if (row <= 0 || row > NUMBER_OF_ROWS) {
			throw new IllegalArgumentException("Invalid row number!");
		}
		if (column <= 0 || column > NUMBER_OF_COLUMNS) {
			throw new IllegalArgumentException("Invalid column number!");
		}
		if (row == 1
				&& (column == 2 || column == 3 || column == 4 || column == 5)) {
			throw new IllegalArgumentException("Invalid cell number!");
		}
	}
}

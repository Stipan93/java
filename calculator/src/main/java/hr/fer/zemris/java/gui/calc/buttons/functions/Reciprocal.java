package hr.fer.zemris.java.gui.calc.buttons.functions;

import hr.fer.zemris.java.gui.calc.CalcUtility;

/**
 * <p>
 * This class represents reciprocal function.
 * </p>
 * It makes contract with {@link IFunction} interface and implements needed
 * method for performing reciprocal function.
 * 
 * @author Stipan Mikulić
 * @version 1.0
 * @see AbstractFunction
 */
public class Reciprocal extends AbstractFunction {
	private static final long serialVersionUID = 1L;
	/**
	 * Function name
	 */
	private static final String NAME = "1/x";

	/**
	 * Creates new {@link Reciprocal} function button.
	 * 
	 * @param calcUtil
	 *            calculator utility
	 */
	public Reciprocal(CalcUtility calcUtil) {
		super(calcUtil);
		setText(NAME);
		setName(NAME);
	}

	@Override
	public double accept(double arg) {
		return 1.0 / arg;
	}

}

package hr.fer.zemris.java.gui.calc;

import java.util.Stack;

import hr.fer.zemris.java.gui.calc.buttons.operators.IOperator;

import javax.swing.JLabel;

/**
 * <p>
 * This class represents calculator utility class.
 * </p>
 * This class contains all resources for performing operations. It provides
 * method for getting calculator screen and stack. It also provides method for
 * operator and operands managing.
 * 
 * @author Stipan Mikulić
 * @version 1.0
 * @see Calculator
 */
public final class CalcUtility {
	/**
	 * Calculator screen
	 */
	private JLabel screen;
	/**
	 * Operator storage
	 */
	private IOperator operator;
	/**
	 * First operand storage
	 */
	private String firstOperand;
	/**
	 * Second operand storage
	 */
	private String secondOperand;
	/**
	 * Calculator stack
	 */
	private Stack<String> stack;

	/**
	 * Creates new {@link CalcUtility}.
	 * 
	 * @param screen
	 *            calculator screen
	 * @param operator
	 *            operator
	 * @param firstOperand
	 *            first operand
	 * @param secondOperand
	 *            second operand
	 * @param stack
	 *            calculator stack
	 */
	public CalcUtility(JLabel screen, IOperator operator, String firstOperand,
						String secondOperand, Stack<String> stack) {
		super();
		this.screen = screen;
		this.operator = operator;
		this.firstOperand = firstOperand;
		this.secondOperand = secondOperand;
		this.stack = stack;
	}

	/**
	 * @return calculator stack
	 */
	public Stack<String> getStack() {
		return stack;
	}

	/**
	 * Sets operator to given operation
	 * 
	 * @param operator
	 *            operation to set
	 */
	public void setOperator(IOperator operator) {
		this.operator = operator;
	}

	/**
	 * Sets first operand to given number
	 * 
	 * @param firstOperand
	 *            number to set
	 */
	public void setFirstOperand(String firstOperand) {
		this.firstOperand = firstOperand;
	}

	/**
	 * Sets second operand to given number
	 * 
	 * @param secondOperand
	 *            number to set
	 */
	public void setSecondOperand(String secondOperand) {
		this.secondOperand = secondOperand;
	}

	/**
	 * @return calculator screen
	 */
	public JLabel getScreen() {
		return screen;
	}

	/**
	 * @return operator
	 */
	public IOperator getOperator() {
		return operator;
	}

	/**
	 * @return first operand
	 */
	public String getFirstOperand() {
		return firstOperand;
	}

	/**
	 * @return second operand
	 */
	public String getSecondOperand() {
		return secondOperand;
	}

}

package hr.fer.zemris.java.gui.calc.buttons.functions;

import hr.fer.zemris.java.gui.calc.CalcUtility;

/**
 * <p>
 * This class represents cosine function.
 * </p>
 * It makes contract with {@link IFunction} interface and implements needed
 * method for performing cosine function.
 * 
 * @author Stipan Mikulić
 * @version 1.0
 * @see AbstractFunction
 */
public class Cosine extends AbstractFunction {
	private static final long serialVersionUID = 1L;
	/**
	 * Function name
	 */
	private static final String NAME = "cos";

	/**
	 * Creates new {@link Cosine} function button.
	 * 
	 * @param calcUtil
	 *            calculator utility
	 */
	public Cosine(CalcUtility calcUtil) {
		super(calcUtil);
		setText(NAME);
		setName(NAME);
	}

	@Override
	public double accept(double arg) {
		return Math.cos(arg);
	}
}

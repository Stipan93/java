package hr.fer.zemris.java.gui.calc.buttons.operators;

import hr.fer.zemris.java.gui.calc.CalcUtility;
import hr.fer.zemris.java.gui.calc.Calculator;

import javax.swing.JButton;

/**
 * <p>
 * This abstract class represents operation which is displayed as a
 * {@link JButton} in {@link Calculator}. It provides algorithm for action
 * listener for every operation.
 * </p>
 * Each operator button on click stores that operator and when second operand is
 * entered and equal is clicked performs it.
 * 
 * @author Stipan Mikulić
 * @version 1.0
 * @see IOperator
 */
public abstract class AbstractOperator extends JButton implements IOperator {
	private static final long serialVersionUID = 1L;

	/**
	 * Creates new {@link AbstractOperator}.
	 * 
	 * @param calcUtil
	 *            calculator utility
	 */
	public AbstractOperator(CalcUtility calcUtil) {
		this.addActionListener((e) -> {
			if (calcUtil.getOperator() != null) {
				calcUtil.setSecondOperand(calcUtil.getScreen().getText());
				double result = calcUtil.getOperator().accept(
						Double.parseDouble(calcUtil.getFirstOperand()),
						Double.parseDouble(calcUtil.getSecondOperand()));
				calcUtil.setFirstOperand(String.valueOf(result));
				calcUtil.setSecondOperand("");
			} else {
				calcUtil.setFirstOperand(calcUtil.getScreen().getText());
			}
			calcUtil.getScreen().setText("");
			calcUtil.setOperator(this);
		});

	}
}

package hr.fer.zemris.java.gui.calc.buttons.functions;

import hr.fer.zemris.java.gui.calc.CalcUtility;

/**
 * <p>
 * This class represents arcus tangent function.
 * </p>
 * It makes contract with {@link IFunction} interface and implements needed
 * method for performing arcus tangent function.
 * 
 * @author Stipan Mikulić
 * @version 1.0
 * @see AbstractFunction
 */
public class ArcusTangent extends AbstractFunction {
	private static final long serialVersionUID = 1L;
	/**
	 * Function name
	 */
	private static final String NAME = "atan";

	/**
	 * Creates new {@link ArcusTangent} function button.
	 * 
	 * @param calcUtil
	 *            calculator utility
	 */
	public ArcusTangent(CalcUtility calcUtil) {
		super(calcUtil);
		setText(NAME);
		setName(NAME);
	}

	@Override
	public double accept(double arg) {
		return Math.atan(arg);
	}
}

package hr.fer.zemris.java.gui.calc.buttons;

import java.awt.event.ActionEvent;

import hr.fer.zemris.java.gui.calc.CalcUtility;

import javax.swing.JButton;

/**
 * <p>
 * This class represents clear button.
 * </p>
 * When is clicked this button clears calculator screen.
 * 
 * @author Stipan Mikulić
 * @version 1.0
 */
public class Clear extends JButton {
	private static final long serialVersionUID = 1L;
	/**
	 * Button name
	 */
	private static final String NAME = "clr";

	/**
	 * Creates new {@link Clear} button.
	 * 
	 * @param calcUtil
	 *            calculator utility
	 */
	public Clear(CalcUtility calcUtil) {
		setText(NAME);
		setName(NAME);
		this.addActionListener((ActionEvent e) -> {
			calcUtil.getScreen().setText("");
		});
	}
}

package hr.fer.zemris.java.gui.calc;

import hr.fer.zemris.java.gui.calc.buttons.Clear;
import hr.fer.zemris.java.gui.calc.buttons.Dot;
import hr.fer.zemris.java.gui.calc.buttons.Equal;
import hr.fer.zemris.java.gui.calc.buttons.Pop;
import hr.fer.zemris.java.gui.calc.buttons.Push;
import hr.fer.zemris.java.gui.calc.buttons.Reset;
import hr.fer.zemris.java.gui.calc.buttons.Sign;
import hr.fer.zemris.java.gui.calc.buttons.digits.Digit;
import hr.fer.zemris.java.gui.calc.buttons.functions.ArcusCosine;
import hr.fer.zemris.java.gui.calc.buttons.functions.ArcusCotangent;
import hr.fer.zemris.java.gui.calc.buttons.functions.ArcusSine;
import hr.fer.zemris.java.gui.calc.buttons.functions.ArcusTangent;
import hr.fer.zemris.java.gui.calc.buttons.functions.Cosine;
import hr.fer.zemris.java.gui.calc.buttons.functions.Cotangent;
import hr.fer.zemris.java.gui.calc.buttons.functions.Exponent;
import hr.fer.zemris.java.gui.calc.buttons.functions.Ln;
import hr.fer.zemris.java.gui.calc.buttons.functions.Log;
import hr.fer.zemris.java.gui.calc.buttons.functions.PowerOf10;
import hr.fer.zemris.java.gui.calc.buttons.functions.Reciprocal;
import hr.fer.zemris.java.gui.calc.buttons.functions.Sine;
import hr.fer.zemris.java.gui.calc.buttons.functions.Tangent;
import hr.fer.zemris.java.gui.calc.buttons.operators.Addition;
import hr.fer.zemris.java.gui.calc.buttons.operators.Division;
import hr.fer.zemris.java.gui.calc.buttons.operators.Multiplication;
import hr.fer.zemris.java.gui.calc.buttons.operators.NthRoot;
import hr.fer.zemris.java.gui.calc.buttons.operators.Subtraction;
import hr.fer.zemris.java.gui.calc.buttons.operators.XtoPowerOfN;
import hr.fer.zemris.java.gui.layouts.CalcLayout;
import hr.fer.zemris.java.gui.layouts.RCPosition;

import java.awt.Checkbox;
import java.awt.Color;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

/**
 * <p>
 * This class represents simple calculator.
 * </p>
 * This calculator provides functions for calculating simple operations like
 * addition, subtraction, multiplication and division. It also provides
 * trigonometric, exponential and logarithmic functions. It allows user to use
 * stack with its appropriate functions.
 * 
 * Example:
 * 
 * <pre>
 * If user enters next input in given order:
 * <code>3, 2, *, 2, + 1, =</code>
 * On screen will be displayed next content:
 * <code>"3", "32", "32"</code>
 * Then calculator stores clicked operation ("*") and enters next input:
 * <code>"2"</code>
 * Then calculator stores clicked operation ("+") and enters next input:
 * <code>"1"</code>
 * Then equal sign is entered and result ("65") is displayed on calculator screen.
 * </pre>
 * 
 * @author Stipan Mikulić
 * @version 1.0
 */
public class Calculator extends JFrame {
	private static final long serialVersionUID = 1L;
	/**
	 * Calculator screen
	 */
	private JLabel screen;
	/**
	 * {@link Map} that contains calculator buttons
	 */
	private Map<String, JButton> buttons;
	/**
	 * Calculator digits
	 */
	private Digit digits;
	/**
	 * Calculator utility
	 */
	private final CalcUtility calcUtil;

	/**
	 * Creates new frame that contains needed component that forms simple
	 * calculator.
	 */
	public Calculator() {
		JPanel p = new JPanel(new CalcLayout(3));
		initScreen(p);
		calcUtil = new CalcUtility(screen, null, "", "", new Stack<>());
		buttons = new HashMap<>();
		digits = new Digit(calcUtil.getScreen());
		initButtonsMap();

		initButtons(p);
		showDefaultFunctions(p);
		addDigits(p);
		addOperators(p);
		initCheckBox(p);

		setSize(p.getPreferredSize());
		setMinimumSize(p.getMinimumSize());
		setTitle("Kalkulator");
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		add(p);
		pack();
	}

	/**
	 * This method fills {@link Map} with all buttons.
	 */
	private void initButtonsMap() {
		buttons.put("sin", new Sine(calcUtil));
		buttons.put("cos", new Cosine(calcUtil));
		buttons.put("tan", new Tangent(calcUtil));
		buttons.put("ctg", new Cotangent(calcUtil));
		buttons.put("1/x", new Reciprocal(calcUtil));
		buttons.put("log", new Log(calcUtil));
		buttons.put("ln", new Ln(calcUtil));
		buttons.put("x^n", new XtoPowerOfN(calcUtil));
		buttons.put("asin", new ArcusSine(calcUtil));
		buttons.put("acos", new ArcusCosine(calcUtil));
		buttons.put("atan", new ArcusTangent(calcUtil));
		buttons.put("actg", new ArcusCotangent(calcUtil));
		buttons.put("10^x", new PowerOf10(calcUtil));
		buttons.put("e^x", new Exponent(calcUtil));
		buttons.put("n\u221Ax", new NthRoot(calcUtil));
		buttons.put("=", new Equal(calcUtil));
		buttons.put("clr", new Clear(calcUtil));
		buttons.put("res", new Reset(calcUtil));
		buttons.put("push", new Push(calcUtil));
		buttons.put("+/-", new Sign(calcUtil));
		buttons.put("pop", new Pop(calcUtil));
		buttons.put(".", new Dot(calcUtil));
		buttons.put("/", new Division(calcUtil));
		buttons.put("*", new Multiplication(calcUtil));
		buttons.put("+", new Addition(calcUtil));
		buttons.put("-", new Subtraction(calcUtil));
	}

	/**
	 * This method initialize {@link Checkbox} on main panel which makes
	 * transformation between default and inverse trigonometric and exponential
	 * functions.
	 * 
	 * @param p
	 *            {@link JPanel} that contains digit buttons
	 */
	private void initCheckBox(JPanel p) {
		JCheckBox inv = new JCheckBox("inv");
		p.add(inv, new RCPosition(5, 7));
		inv.addActionListener((e) -> {
			if (inv.isSelected()) {
				showInverseFunctions(p);
			} else {
				showDefaultFunctions(p);
			}
		});
	}

	/**
	 * This method adds specific buttons on main panel.
	 * 
	 * @param p
	 *            {@link JPanel} that contains buttons
	 */
	private void initButtons(JPanel p) {
		p.add(buttons.get("="), new RCPosition(1, 6));
		p.add(buttons.get("clr"), new RCPosition(1, 7));
		p.add(buttons.get("res"), new RCPosition(2, 7));
		p.add(buttons.get("push"), new RCPosition(3, 7));
		p.add(buttons.get("pop"), new RCPosition(4, 7));
		p.add(buttons.get("+/-"), new RCPosition(5, 4));
		p.add(buttons.get("."), new RCPosition(5, 5));
	}

	/**
	 * This method adds simple operators on main panel.
	 * 
	 * @param p
	 *            {@link JPanel} that contains operator buttons
	 */
	private void addOperators(JPanel p) {
		p.add(buttons.get("/"), new RCPosition(2, 6));
		p.add(buttons.get("*"), new RCPosition(3, 6));
		p.add(buttons.get("-"), new RCPosition(4, 6));
		p.add(buttons.get("+"), new RCPosition(5, 6));
	}

	/**
	 * This method adds digits on main panel.
	 * 
	 * @param p
	 *            {@link JPanel} that contains digit buttons
	 */
	private void addDigits(JPanel p) {
		int digitNumber = 0;
		p.add(digits.getDigit(digitNumber++), new RCPosition(5, 3));
		p.add(digits.getDigit(digitNumber++), new RCPosition(4, 3));
		p.add(digits.getDigit(digitNumber++), new RCPosition(4, 4));
		p.add(digits.getDigit(digitNumber++), new RCPosition(4, 5));
		p.add(digits.getDigit(digitNumber++), new RCPosition(3, 3));
		p.add(digits.getDigit(digitNumber++), new RCPosition(3, 4));
		p.add(digits.getDigit(digitNumber++), new RCPosition(3, 5));
		p.add(digits.getDigit(digitNumber++), new RCPosition(2, 3));
		p.add(digits.getDigit(digitNumber++), new RCPosition(2, 4));
		p.add(digits.getDigit(digitNumber++), new RCPosition(2, 5));
	}

	/**
	 * This method adds default trigonometric and exponential functions on main
	 * panel
	 * 
	 * @param p
	 *            {@link JPanel} that contains buttons
	 */
	private void showDefaultFunctions(JPanel p) {
		removeInverseFunctions(p);
		p.add(buttons.get("1/x"), new RCPosition(2, 1));
		p.add(buttons.get("sin"), new RCPosition(2, 2));
		p.add(buttons.get("log"), new RCPosition(3, 1));
		p.add(buttons.get("cos"), new RCPosition(3, 2));
		p.add(buttons.get("ln"), new RCPosition(4, 1));
		p.add(buttons.get("tan"), new RCPosition(4, 2));
		p.add(buttons.get("x^n"), new RCPosition(5, 1));
		p.add(buttons.get("ctg"), new RCPosition(5, 2));
		p.repaint();
		pack();
	}

	/**
	 * Removes inverse trigonometric and exponential functions from main panel.
	 * 
	 * @param p
	 *            {@link JPanel} that contains buttons
	 */
	private void removeInverseFunctions(JPanel p) {
		p.remove(buttons.get("1/x"));
		p.remove(buttons.get("asin"));
		p.remove(buttons.get("10^x"));
		p.remove(buttons.get("acos"));
		p.remove(buttons.get("e^x"));
		p.remove(buttons.get("atan"));
		p.remove(buttons.get("n\u221Ax"));
		p.remove(buttons.get("actg"));
	}

	/**
	 * This method adds inverse trigonometric and exponential functions on main
	 * panel
	 * 
	 * @param p
	 *            {@link JPanel} that contains buttons
	 */
	private void showInverseFunctions(JPanel p) {
		removeDefaultFunctions(p);
		p.add(buttons.get("1/x"), new RCPosition(2, 1));
		p.add(buttons.get("asin"), new RCPosition(2, 2));
		p.add(buttons.get("10^x"), new RCPosition(3, 1));
		p.add(buttons.get("acos"), new RCPosition(3, 2));
		p.add(buttons.get("e^x"), new RCPosition(4, 1));
		p.add(buttons.get("atan"), new RCPosition(4, 2));
		p.add(buttons.get("n\u221Ax"), new RCPosition(5, 1));
		p.add(buttons.get("actg"), new RCPosition(5, 2));
		p.repaint();
		pack();
	}

	/**
	 * Removes default trigonometric and exponential functions from main panel.
	 * 
	 * @param p
	 *            {@link JPanel} that contains buttons
	 */
	private void removeDefaultFunctions(JPanel p) {
		p.remove(buttons.get("1/x"));
		p.remove(buttons.get("sin"));
		p.remove(buttons.get("log"));
		p.remove(buttons.get("cos"));
		p.remove(buttons.get("ln"));
		p.remove(buttons.get("tan"));
		p.remove(buttons.get("x^n"));
		p.remove(buttons.get("ctg"));
	}

	/**
	 * This method initialize calculator screen and adds it to main panel.
	 * 
	 * @param p
	 *            {@link JPanel} to which screen is added
	 */
	private void initScreen(JPanel p) {
		screen = new JLabel();
		screen.setBackground(Color.ORANGE);
		screen.setOpaque(true);
		screen.setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY, 1));
		screen.setHorizontalAlignment(SwingConstants.RIGHT);
		screen.setVerticalAlignment(SwingConstants.CENTER);
		p.add(screen, new RCPosition(1, 1));
	}

	/**
	 * This method is called when program starts.
	 * 
	 * @param args
	 *            command line arguments
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(() -> {
			Calculator frame = new Calculator();
			frame.setVisible(true);
		});
	}

}

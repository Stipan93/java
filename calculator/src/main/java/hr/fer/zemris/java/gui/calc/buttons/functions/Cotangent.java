package hr.fer.zemris.java.gui.calc.buttons.functions;

import hr.fer.zemris.java.gui.calc.CalcUtility;

/**
 * <p>
 * This class represents cotangent function.
 * </p>
 * It makes contract with {@link IFunction} interface and implements needed
 * method for performing cotangent function.
 * 
 * @author Stipan Mikulić
 * @version 1.0
 * @see AbstractFunction
 */
public class Cotangent extends AbstractFunction {
	private static final long serialVersionUID = 1L;
	/**
	 * Function name
	 */
	private static final String NAME = "ctg";

	/**
	 * Creates new {@link Cotangent} function button.
	 * 
	 * @param calcUtil
	 *            calculator utility
	 */
	public Cotangent(CalcUtility calcUtil) {
		super(calcUtil);
		setText(NAME);
		setName(NAME);
	}

	@Override
	public double accept(double arg) {
		return 1.0 / Math.tan(arg);
	}

}

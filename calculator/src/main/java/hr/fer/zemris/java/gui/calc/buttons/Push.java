package hr.fer.zemris.java.gui.calc.buttons;

import java.awt.event.ActionEvent;

import hr.fer.zemris.java.gui.calc.CalcUtility;

import javax.swing.JButton;

/**
 * <p>
 * This class represents push button.
 * </p>
 * When is clicked this button pushes number from screen on stack.
 * 
 * @author Stipan Mikulić
 * @version 1.0
 */
public class Push extends JButton {
	private static final long serialVersionUID = 1L;
	/**
	 * Button name
	 */
	private static final String NAME = "push";

	/**
	 * Creates new {@link Push} button.
	 * 
	 * @param calcUtil
	 *            calculator utility
	 */
	public Push(CalcUtility calcUtil) {
		setText(NAME);
		setName(NAME);
		this.addActionListener((ActionEvent e) -> {
			calcUtil.getStack().push(calcUtil.getScreen().getText());
		});
	}
}

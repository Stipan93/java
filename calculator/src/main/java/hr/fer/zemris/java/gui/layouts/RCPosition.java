package hr.fer.zemris.java.gui.layouts;

/**
 * <p>
 * This class represents constraints for {@link CalcLayout} layout.
 * </p>
 * It contains two numbers that determines row and column in {@link CalcLayout}
 * layout table.
 * 
 * @author Stipan Mikulić
 * @version 1.0
 * @see CalcLayout
 */
public class RCPosition {
	/**
	 * Row number
	 */
	private final int row;
	/**
	 * Column number
	 */
	private final int column;

	/**
	 * Creates new {@link RCPosition} with given row and column number.
	 * 
	 * @param row
	 *            row number
	 * @param column
	 *            column number
	 */
	public RCPosition(int row, int column) {
		super();
		this.row = row;
		this.column = column;
	}

	/**
	 * @return row number
	 */
	public int getRow() {
		return row;
	}

	/**
	 * @return column number
	 */
	public int getColumn() {
		return column;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + column;
		result = prime * result + row;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RCPosition other = (RCPosition) obj;
		if (column != other.column)
			return false;
		if (row != other.row)
			return false;
		return true;
	}

}

package hr.fer.zemris.java.gui.calc.buttons.functions;

import hr.fer.zemris.java.gui.calc.CalcUtility;

/**
 * <p>
 * This class represents tangent function.
 * </p>
 * It makes contract with {@link IFunction} interface and implements needed
 * method for performing tangent function.
 * 
 * @author Stipan Mikulić
 * @version 1.0
 * @see AbstractFunction
 */
public class Tangent extends AbstractFunction {
	private static final long serialVersionUID = 1L;
	/**
	 * Function name
	 */
	private static final String NAME = "tan";

	/**
	 * Creates new {@link Tangent} function button.
	 * 
	 * @param calcUtil
	 *            calculator utility
	 */
	public Tangent(CalcUtility calcUtil) {
		super(calcUtil);
		setText(NAME);
		setName(NAME);
	}

	@Override
	public double accept(double arg) {
		return Math.tan(arg);
	}

}

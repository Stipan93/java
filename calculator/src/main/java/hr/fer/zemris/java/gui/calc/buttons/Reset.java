package hr.fer.zemris.java.gui.calc.buttons;

import hr.fer.zemris.java.gui.calc.CalcUtility;

import javax.swing.JButton;

/**
 * <p>
 * This class represents reset button.
 * </p>
 * When is clicked this button resets stored operator, screen and operands.
 * 
 * @author Stipan Mikulić
 * @version 1.0
 */
public class Reset extends JButton {
	private static final long serialVersionUID = 1L;
	/**
	 * Button name
	 */
	private static final String NAME = "res";

	/**
	 * Creates new {@link Reset} button.
	 * 
	 * @param calcUtil
	 *            calculator utility
	 */
	public Reset(CalcUtility calcUtil) {
		setText(NAME);
		setName(NAME);
		this.addActionListener((e) -> {
			calcUtil.getScreen().setText("");
			calcUtil.setOperator(null);
			calcUtil.setFirstOperand("");
			calcUtil.setSecondOperand("");
		});
	}
}

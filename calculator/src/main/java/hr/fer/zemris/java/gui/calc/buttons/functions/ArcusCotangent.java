package hr.fer.zemris.java.gui.calc.buttons.functions;

import hr.fer.zemris.java.gui.calc.CalcUtility;

/**
 * <p>
 * This class represents arcus cotangent function.
 * </p>
 * It makes contract with {@link IFunction} interface and implements needed
 * method for performing arcus cotangent function.
 * 
 * @author Stipan Mikulić
 * @version 1.0
 * @see AbstractFunction
 */
public class ArcusCotangent extends AbstractFunction {
	private static final long serialVersionUID = 1L;
	/**
	 * Function name
	 */
	private static final String NAME = "actg";

	/**
	 * Creates new {@link ArcusCotangent} function button.
	 * 
	 * @param calcUtil
	 *            calculator utility
	 */
	public ArcusCotangent(CalcUtility calcUtil) {
		super(calcUtil);
		setText(NAME);
		setName(NAME);
	}

	@Override
	public double accept(double arg) {
		return 1.0 / Math.atan(arg);
	}

}

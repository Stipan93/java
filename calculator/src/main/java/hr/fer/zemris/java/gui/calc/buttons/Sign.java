package hr.fer.zemris.java.gui.calc.buttons;

import hr.fer.zemris.java.gui.calc.CalcUtility;

import javax.swing.JButton;

/**
 * <p>
 * This class represents sign button. Negative numbers have minus sign and
 * positive numbers have no sign on screen.
 * </p>
 * This button changes sign for number on screen when is clicked.
 * 
 * @author Stipan Mikulić
 * @version 1.0
 */
public class Sign extends JButton {
	private static final long serialVersionUID = 1L;
	/**
	 * Button name
	 */
	private static final String NAME = "+/-";

	/**
	 * Creates new {@link Sign} button.
	 * 
	 * @param calcUtil
	 *            calculator utility
	 */
	public Sign(CalcUtility calcUtil) {
		setText(NAME);
		setName(NAME);
		this.addActionListener((e) -> {
			if (!calcUtil.getScreen().getText().isEmpty()) {
				if (Double.parseDouble(calcUtil.getScreen().getText()) > 0) {
					calcUtil.getScreen().setText(
							"-" + calcUtil.getScreen().getText());
				} else {
					calcUtil.getScreen().setText(
							calcUtil.getScreen().getText().replace("-", ""));
				}
			}
		});
	}
}

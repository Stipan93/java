package hr.fer.zemris.java.gui.calc.buttons.functions;

/**
 * This interface makes contract with classes which implements it and it
 * provides method which needs to implement algorithm for specific function.
 * Concrete strategy class needs to implement method {@link #accept(double)} in
 * its appropriate way.
 * 
 * @author Stipan Mikulić
 * @version 1.0
 */
public interface IFunction {
	/**
	 * This method calculates defined function on given argument.
	 * 
	 * @param arg
	 *            operand
	 * @return result of calculated function
	 */
	public double accept(double arg);
}

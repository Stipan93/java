package hr.fer.zemris.java.gui.calc.buttons.functions;

import hr.fer.zemris.java.gui.calc.CalcUtility;

/**
 * <p>
 * This class represents arcus cosine function.
 * </p>
 * It makes contract with {@link IFunction} interface and implements needed
 * method for performing arcus cosine function.
 * 
 * @author Stipan Mikulić
 * @version 1.0
 * @see AbstractFunction
 */
public class ArcusCosine extends AbstractFunction {
	private static final long serialVersionUID = 1L;
	/**
	 * Function name
	 */
	private static final String NAME = "acos";

	/**
	 * Creates new {@link ArcusCosine} function button.
	 * 
	 * @param calcUtil
	 *            calculator utility
	 */
	public ArcusCosine(CalcUtility calcUtil) {
		super(calcUtil);
		setText(NAME);
		setName(NAME);
	}

	@Override
	public double accept(double arg) {
		return Math.acos(arg);
	}
}

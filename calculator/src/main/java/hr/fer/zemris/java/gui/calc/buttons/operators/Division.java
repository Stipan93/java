package hr.fer.zemris.java.gui.calc.buttons.operators;

import hr.fer.zemris.java.gui.calc.CalcUtility;

/**
 * <p>
 * This class represent division operator.
 * </p>
 * It makes contract with {@link IOperator} interface and implements needed
 * method for performing division.
 * 
 * @author Stipan Mikulić
 * @version 1.0
 * @see AbstractOperator
 */
public class Division extends AbstractOperator {
	private static final long serialVersionUID = 1L;
	/**
	 * Operation name
	 */
	private static final String NAME = "/";

	/**
	 * Creates new {@link Division} button which provides division operation.
	 * 
	 * @param calcUtil
	 *            calculator utility
	 */
	public Division(CalcUtility calcUtil) {
		super(calcUtil);
		setText(NAME);
		setName(NAME);
	}

	@Override
	public double accept(double arg1, double arg2) {
		if (Math.abs(arg2) < Double.MIN_VALUE) {
			if (arg1 < 0 && arg2 < 0 || arg1 > 0 && arg2 > 0) {
				return Double.POSITIVE_INFINITY;
			} else {
				return Double.NEGATIVE_INFINITY;
			}
		}
		return arg1 / arg2;
	}
}
package hr.fer.zemris.java.gui.calc.buttons;

import hr.fer.zemris.java.gui.calc.CalcUtility;

import javax.swing.JButton;

/**
 * <p>
 * This class represents dot button.
 * </p>
 * When is clicked this button prints dot on calculator screen.
 * 
 * @author Stipan Mikulić
 * @version 1.0
 */
public class Dot extends JButton {
	private static final long serialVersionUID = 1L;
	/**
	 * Button name
	 */
	private static final String NAME = ".";

	/**
	 * Creates new {@link Dot} button.
	 * 
	 * @param calcUtil
	 *            calculator utility
	 */
	public Dot(CalcUtility calcUtil) {
		setText(NAME);
		setName(NAME);
		this.addActionListener((e) -> {
			if (!calcUtil.getScreen().getText().contains(".")) {
				calcUtil.getScreen().setText(
						calcUtil.getScreen().getText() + ".");
			}
		});
	}
}

package hr.fer.zemris.java.gui.calc.buttons.functions;

import hr.fer.zemris.java.gui.calc.CalcUtility;
import hr.fer.zemris.java.gui.calc.Calculator;

import java.awt.event.ActionEvent;

import javax.swing.JButton;

/**
 * <p>
 * This abstract class represents function which is displayed as a
 * {@link JButton} in {@link Calculator}. It provides algorithm for action
 * listener for every function.
 * </p>
 * Each function button on click calculates value of this function on value
 * stored in first operand.
 * 
 * @author Stipan Mikulić
 * @version 1.0
 * @see IFunction
 */
public abstract class AbstractFunction extends JButton implements IFunction {
	private static final long serialVersionUID = 1L;

	/**
	 * Creates new {@link AbstractFunction}.
	 * 
	 * @param calcUtil
	 *            calculator utility
	 */
	public AbstractFunction(CalcUtility calcUtil) {
		this.addActionListener((ActionEvent e) -> {
			if (!calcUtil.getScreen().getText().isEmpty()) {
				calcUtil.setFirstOperand(calcUtil.getScreen().getText());
				double result = accept(Double.parseDouble(calcUtil
						.getFirstOperand()));
				if ((new Double(result)).isInfinite()
						|| (new Double(result)).isNaN()) {
					calcUtil.getScreen().setText("Argument out of domain!");
				} else {
					calcUtil.getScreen().setText(String.valueOf(result));
				}
			}
		});
	}
}

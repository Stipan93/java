package hr.fer.zemris.java.gui.calc.buttons.functions;

import hr.fer.zemris.java.gui.calc.CalcUtility;

/**
 * <p>
 * This class represents power of ten function.
 * </p>
 * It makes contract with {@link IFunction} interface and implements needed
 * method for performing power of ten function.
 * 
 * @author Stipan Mikulić
 * @version 1.0
 * @see AbstractFunction
 */
public class PowerOf10 extends AbstractFunction {
	private static final long serialVersionUID = 1L;
	/**
	 * Base of power function
	 */
	private static final double BASE = 10;
	/**
	 * Function name
	 */
	private static final String NAME = "10^x";

	/**
	 * Creates new {@link PowerOf10} function button.
	 * 
	 * @param calcUtil
	 *            calculator utility
	 */
	public PowerOf10(CalcUtility calcUtil) {
		super(calcUtil);
		setText(NAME);
		setName(NAME);
	}

	@Override
	public double accept(double arg) {
		return Math.pow(BASE, arg);
	}

}

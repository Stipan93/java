package hr.fer.zemris.java.gui.calc.buttons.operators;

import hr.fer.zemris.java.gui.calc.CalcUtility;

/**
 * <p>
 * This class represent n-th root operator.
 * </p>
 * It makes contract with {@link IOperator} interface and implements needed
 * method for performing n-th root operation.
 * 
 * @author Stipan Mikulić
 * @version 1.0
 * @see AbstractOperator
 */
public class NthRoot extends AbstractOperator {
	private static final long serialVersionUID = 1L;
	/**
	 * Operation name
	 */
	private static final String NAME = "n\u221Ax";

	/**
	 * Creates new {@link NthRoot} button which provides n-th root operation.
	 * 
	 * @param calcUtil
	 *            calculator utility
	 */
	public NthRoot(CalcUtility calcUtil) {
		super(calcUtil);
		setText(NAME);
		setName(NAME);
	}

	@Override
	public double accept(double arg1, double arg2) {
		return Math.pow(arg1, 1.0 / arg2);
	}
}

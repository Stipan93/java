package hr.fer.zemris.java.gui.calc.buttons.functions;

import hr.fer.zemris.java.gui.calc.CalcUtility;

/**
 * <p>
 * This class represents arcus sine function.
 * </p>
 * It makes contract with {@link IFunction} interface and implements needed
 * method for performing arcus sine function.
 * 
 * @author Stipan Mikulić
 * @version 1.0
 * @see AbstractFunction
 */
public class ArcusSine extends AbstractFunction {
	private static final long serialVersionUID = 1L;
	/**
	 * Function name
	 */
	private static final String NAME = "asin";

	/**
	 * Creates new {@link ArcusSine} function button.
	 * 
	 * @param calcUtil
	 *            calculator utility
	 */
	public ArcusSine(CalcUtility calcUtil) {
		super(calcUtil);
		setText(NAME);
		setName(NAME);
	}

	@Override
	public double accept(double arg) {
		return Math.asin(arg);
	}
}

package hr.fer.zemris.java.gui.calc.buttons.digits;

import hr.fer.zemris.java.gui.calc.Calculator;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;

/**
 * <p>
 * This class represents digits for {@link Calculator}.
 * </p>
 * It contains digits (0-9) and they are store as {@link JButton}.
 * 
 * @author Stipan Mikulić
 * @version 1.0
 * @see Calculator
 */
public class Digit {
	/**
	 * Number of digits
	 */
	public static final int NUMBER_OF_DIGITS = 10;
	/**
	 * {@link List} where {@link JButton} digits are stored
	 */
	List<JButton> digits;
	/**
	 * Calculator screen
	 */
	JLabel screen;

	/**
	 * Creates new instance of {@link Digit} class.
	 * 
	 * @param screen
	 *            calculator screen
	 */
	public Digit(JLabel screen) {
		this.screen = screen;
		digits = new ArrayList<JButton>();
		for (int i = 0; i < NUMBER_OF_DIGITS; i++) {
			JButton digit = new JButton(String.valueOf(i));
			digit.addActionListener((e) -> {
				try {
					Double.parseDouble(screen.getText());
				} catch (Exception ex) {
					screen.setText("");
				}
				screen.setText(screen.getText() + digit.getText());
			});
			digits.add(digit);
		}
	}

	/**
	 * This method returns {@link JButton} that represents digit that is
	 * required.
	 * 
	 * @param digit
	 *            digit that is required
	 * @return {@link JButton} that represents digit
	 */
	public JButton getDigit(int digit) {
		return digits.get(digit);
	}
}

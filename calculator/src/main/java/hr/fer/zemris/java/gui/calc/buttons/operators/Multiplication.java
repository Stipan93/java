package hr.fer.zemris.java.gui.calc.buttons.operators;

import hr.fer.zemris.java.gui.calc.CalcUtility;

/**
 * <p>
 * This class represent multiplication operator.
 * </p>
 * It makes contract with {@link IOperator} interface and implements needed
 * method for performing multiplication.
 * 
 * @author Stipan Mikulić
 * @version 1.0
 * @see AbstractOperator
 */
public class Multiplication extends AbstractOperator {
	private static final long serialVersionUID = 1L;
	/**
	 * Operation name
	 */
	private static final String NAME = "*";

	/**
	 * Creates new {@link Multiplication} button which provides multiplication
	 * operation.
	 * 
	 * @param calcUtil
	 *            calculator utility
	 */
	public Multiplication(CalcUtility calcUtil) {
		super(calcUtil);
		setText(NAME);
		setName(NAME);
	}

	@Override
	public double accept(double arg1, double arg2) {
		return arg1 * arg2;
	}

}

package hr.fer.zemris.java.hw10.jnotepadpp.i18n;

/**
 * <p>
 * This class represents decorator for some other {@link ILocalizationProvider}.
 * </p>
 * This class provides methods for connecting to other
 * {@link ILocalizationProvider}. That allows garbage collector to release
 * memory of disposed frames because when frame is disposed localization
 * provider disconnects from it.
 * 
 * @author Stipan Mikulić
 * @version 1.0
 */
public class LocalizationProviderBridge extends AbstractLocalizationProvider {
	/**
	 * Flag that determines connection status
	 */
	private boolean connected;
	/**
	 * Listener that allows this class to be notified for language changes
	 */
	private ILocalizationListener listener;
	/**
	 * {@link ILocalizationProvider} that is decorated by this class
	 */
	private ILocalizationProvider parent;

	/**
	 * Creates new {@link LocalizationProviderBridge} that decorates
	 * {@link ILocalizationProvider} given as argument.
	 * 
	 * @param parent
	 *            {@link ILocalizationProvider} that is decorated
	 */
	public LocalizationProviderBridge(ILocalizationProvider parent) {
		connected = false;
		listener = () -> {
			fire();
		};
		this.parent = parent;
	}

	/**
	 * Disconnects this class from decorated {@link ILocalizationProvider}.
	 * (i.e. to its parent object)
	 */
	public void disconnect() {
		if (connected) {
			connected = false;
			parent.removeLocalizationListener(listener);
		}
	}

	/**
	 * Connects this class to decorated {@link ILocalizationProvider}. (i.e. to
	 * its parent object)
	 */
	public void connect() {
		if (!connected) {
			connected = true;
			parent.addLocalizationListener(listener);
		}
	}

	@Override
	public String getString(String key) {
		return parent.getString(key);
	}

}

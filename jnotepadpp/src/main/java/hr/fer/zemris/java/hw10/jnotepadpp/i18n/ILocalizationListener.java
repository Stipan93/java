package hr.fer.zemris.java.hw10.jnotepadpp.i18n;

/**
 * <p>
 * This interface represents localization listeners.
 * </p>
 * It provides method {@link #localizationChanged()} which will be called by the
 * Subject (Observer pattern) when the selected language changes.
 * 
 * Each listener implements its specific job that needs to be done when language
 * is changed.
 * 
 * @author Stipan Mikulić
 * @version 1.0
 */
public interface ILocalizationListener {
	/**
	 * This method is called by the Subject when language is changed.
	 */
	public void localizationChanged();
}

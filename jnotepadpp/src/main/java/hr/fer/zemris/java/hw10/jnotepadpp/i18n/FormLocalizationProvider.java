package hr.fer.zemris.java.hw10.jnotepadpp.i18n;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;

/**
 * <p>
 * This class represents listener for {@link JFrame}.
 * </p>
 * 
 * This class registers itself as a WindowListener to its JFrame; when frame is
 * opened, it connects to given {@link ILocalizationProvider} and when frame is
 * closed, it disconnects itself.
 * 
 * @author Stipan Mikulić
 * @version 1.0
 */
public class FormLocalizationProvider extends LocalizationProviderBridge {

	/**
	 * Creates new {@link FormLocalizationProvider} that registers itself to
	 * given frame.
	 * 
	 * @param parent
	 *            {@link ILocalizationProvider} that provides localization
	 * @param frame
	 *            frame on listeners are added
	 */
	public FormLocalizationProvider(ILocalizationProvider parent, JFrame frame) {
		super(parent);
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowOpened(WindowEvent e) {
				super.windowOpened(e);
				connect();
			}

			@Override
			public void windowClosed(WindowEvent e) {
				super.windowClosed(e);
				disconnect();
			}
		});
	}

}

package hr.fer.zemris.java.hw10.jnotepadpp;

import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 * <p>
 * This class represents tab which extends {@link JScrollPane}.
 * </p>
 * This {@link Tab} contains {@link JTextArea} where files are displayed
 * contents of opened files.
 * 
 * @author Stipan Mikulić
 * @version 1.0
 */
public class Tab extends JScrollPane {
	private static final long serialVersionUID = 1L;
	/**
	 * {@link JTextArea} that represents editor
	 */
	private JTextArea editor;

	/**
	 * Creates new {@link Tab} with given {@link JTextArea}.
	 * 
	 * @param editor
	 *            {@link JTextArea} where opened file is displayed
	 */
	public Tab(JTextArea editor) {
		super(editor);
		this.editor = editor;
	}

	/**
	 * @return {@link JTextArea} that this {@link Tab} contains
	 */
	public JTextArea getEditor() {
		return editor;
	}

}

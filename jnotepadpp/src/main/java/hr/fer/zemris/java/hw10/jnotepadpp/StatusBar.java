package hr.fer.zemris.java.hw10.jnotepadpp;

import hr.fer.zemris.java.hw10.jnotepadpp.i18n.LJLabel;

import java.awt.Color;
import java.awt.Component;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.Timer;
import javax.swing.text.Document;

/**
 * <p>
 * This class represents status bar in {@link JNotepadPP}.
 * </p>
 * {@link StatusBar} contains informations about length of currently opened file
 * and about caret in file. It also contains clock.
 * 
 * @author Stipan Mikulić
 * @version 1.0
 */
public class StatusBar extends JPanel {
	private static final long serialVersionUID = 1L;
	/**
	 * Length label key
	 */
	private static final String LENGTH_KEY = "length";
	/**
	 * Default text for caret info label
	 */
	private static final String DEFAULT_CARET_INFO = "Ln: " + "  Col: "
			+ "  Sel: ";
	/**
	 * Length info label in status bar
	 */
	private JLabel lengthInfo;
	/**
	 * Caret info label in status bar
	 */
	private JLabel caretInfo = new JLabel(DEFAULT_CARET_INFO);
	/**
	 * Clock label in status bar
	 */
	private JLabel clock = new JLabel();
	/**
	 * {@link Timer} used to run clock in status bar
	 */
	private Timer timer;
	/**
	 * Reference to {@link JNotepadPP}, application frame
	 */
	private JNotepadPP notepadPP;

	/**
	 * Creates new {@link StatusBar} with real time clock and length and caret
	 * info set to default value.
	 * 
	 * @param jNotepadPP
	 *            reference to {@link JNotepadPP}, application frame
	 */
	public StatusBar(JNotepadPP jNotepadPP) {
		this.notepadPP = jNotepadPP;
		lengthInfo = new LJLabel(LENGTH_KEY, JNotepadPP.getFlp());
		setLayout(new GridLayout(1, 3));
		setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY, 1));
		add(lengthInfo);
		add(caretInfo);
		add(clock);

		final DateFormat timeFormat = new SimpleDateFormat(
				"yyyy/MM/dd  HH:mm:ss");
		ActionListener timerListener = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String time = timeFormat.format(new Date());
				clock.setText(time);
			}
		};
		timer = new Timer(1000, timerListener);
		timer.setInitialDelay(0);
		timer.start();
	}

	/**
	 * @return length info label
	 */
	public JLabel getLengthInfo() {
		return lengthInfo;
	}

	/**
	 * @return caret info label
	 */
	public JLabel getCaretInfo() {
		return caretInfo;
	}

	/**
	 * @return clock label
	 */
	public JLabel getClock() {
		return clock;
	}

	/**
	 * @return gets {@link Timer} which runs clock
	 */
	public Timer getTimer() {
		return timer;
	}

	/**
	 * This method sets caret info label to its default value
	 */
	public void setDefaultCaretInfo() {
		caretInfo.setText(DEFAULT_CARET_INFO);
	}

	/**
	 * This method sets length info label to its default value
	 */
	public void setDefaultLengthInfo() {
		lengthInfo.setText(JNotepadPP.getFlp().getString(LENGTH_KEY));
	}

	/**
	 * This method gets caret data from current editor and updates caret info
	 * label
	 */
	public void upadateCaret() {
		Component comp = notepadPP.getTabbedEditorPane().getSelectedComponent();
		if (comp == null) {
			setDefaultCaretInfo();
			return;
		}
		JTextArea editor = ((Tab) comp).getEditor();
		int caretPos = editor.getCaretPosition();
		int lineNum = 0;
		int columnPos = 0;
		int selection = 0;
		try {
			lineNum = editor.getLineOfOffset(caretPos);
			columnPos = caretPos - editor.getLineStartOffset(lineNum);
			int selectionStart = Math.min(
					editor.getCaret().getDot(),
					editor.getCaret().getMark());
			selection = Math.max(editor.getCaret().getDot(), editor.getCaret()
					.getMark())
					- selectionStart;
			if (selection > 0) {
				notepadPP.getNotepadMenuBar().getCaseMenu().setEnabled(true);
				notepadPP.getNotepadMenuBar().getSortMenu().setEnabled(true);
			} else {
				notepadPP.getNotepadMenuBar().getCaseMenu().setEnabled(false);
				notepadPP.getNotepadMenuBar().getSortMenu().setEnabled(false);
			}
		} catch (Exception ingorable) {
		}
		getCaretInfo().setText(
				"Ln: " + lineNum + "  Col: " + columnPos + "  Sel: "
						+ selection);
	}

	/**
	 * This method gets length of file opened in current editor and updates
	 * length info label
	 */
	public void updateLength() {
		Component comp = notepadPP.getTabbedEditorPane().getSelectedComponent();
		if (comp == null) {
			setDefaultLengthInfo();
			return;
		}
		JTextArea editor = ((Tab) comp).getEditor();
		Document doc = editor.getDocument();
		int length = doc.getLength();
		getLengthInfo().setText(
				JNotepadPP.getFlp().getString(LENGTH_KEY) + ": " + length);
	}

}

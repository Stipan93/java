package hr.fer.zemris.java.hw10.jnotepadpp;

import hr.fer.zemris.java.hw10.jnotepadpp.i18n.LocalizableAction;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;

import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;

/**
 * <p>
 * This class provides all needed actions for {@link JNotepadPP}.
 * </p>
 * 
 * @author Stipan Mikulić
 * @version 1.0
 */
public class Actions {
	/**
	 * Default locale properties
	 */
	private static final String DEFAUL_LOCALE_NAME = "en";
	/**
	 * Reference to {@link JNotepadPP}, application frame
	 */
	private JNotepadPP notepadPP;
	/**
	 * Storage for copied or cut text
	 */
	private String copiedText;
	/**
	 * {@link DocumentListener} that defines methods that listen for changes in
	 * document
	 */
	private DocumentListener modificationListener = new DocumentListener() {
		@Override
		public void removeUpdate(DocumentEvent e) {
			setRedFloppyIcon();
			notepadPP.getStatusBar().updateLength();
		}

		@Override
		public void insertUpdate(DocumentEvent e) {
			setRedFloppyIcon();
			notepadPP.getStatusBar().updateLength();
		}

		@Override
		public void changedUpdate(DocumentEvent e) {
			setRedFloppyIcon();
			notepadPP.getStatusBar().updateLength();
		}

		/**
		 * Set tab floppy icon to red color
		 */
		private void setRedFloppyIcon() {
			int index = notepadPP.getTabbedEditorPane().getSelectedIndex();
			notepadPP.getTabbedEditorPane().setIconAt(index, RedFloppyIcon);
		}
	};

	/**
	 * Red floppy icon
	 */
	ImageIcon RedFloppyIcon = new ImageIcon(
			"resources/icons/RedModifyIcon.png", "Red");

	/**
	 * Green floppy icon
	 */
	ImageIcon GreenFloppyIcon = new ImageIcon(
			"resources/icons/GreenModifyIcon.png", "Green");

	/**
	 * Creates new actions for all supported operations in {@link JNotepadPP}.
	 * 
	 * @param jNotepadPP
	 *            reference to {@link JNotepadPP}, application frame
	 */
	Actions(JNotepadPP jNotepadPP) {
		notepadPP = jNotepadPP;
		initializeAccelerators();
		initializeMnemonics();
	}

	/**
	 * This method initialize mnemonics.
	 */
	private void initializeMnemonics() {
		newDocumentAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_N);
		openDocumentAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_O);
		saveDocumentAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_S);
		saveAsDocumentAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_A);
		exitAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_E);
		closeTabAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_C);
		copySelectedPartAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_C);
		cutSelectedPartAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_U);
		pasteSelectedPartAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_P);
		statisticalInfoAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_S);
		deleteSelectedPartAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_D);
		toggleCaseAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_T);
		lowerCaseAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_L);
		upperCaseAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_U);
		ascendingSortAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_A);
		descendingSortAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_D);
		uniqueAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_R);
	}

	/**
	 * This method initialize accelerators
	 */
	private void initializeAccelerators() {
		newDocumentAction.putValue(
				Action.ACCELERATOR_KEY,
				KeyStroke.getKeyStroke("control N"));
		openDocumentAction.putValue(
				Action.ACCELERATOR_KEY,
				KeyStroke.getKeyStroke("control O"));
		saveDocumentAction.putValue(
				Action.ACCELERATOR_KEY,
				KeyStroke.getKeyStroke("control S"));
		saveAsDocumentAction.putValue(
				Action.ACCELERATOR_KEY,
				KeyStroke.getKeyStroke("control shift S"));
		copySelectedPartAction.putValue(
				Action.ACCELERATOR_KEY,
				KeyStroke.getKeyStroke("control C"));
		cutSelectedPartAction.putValue(
				Action.ACCELERATOR_KEY,
				KeyStroke.getKeyStroke("control X"));
		pasteSelectedPartAction.putValue(
				Action.ACCELERATOR_KEY,
				KeyStroke.getKeyStroke("control V"));
		statisticalInfoAction.putValue(
				Action.ACCELERATOR_KEY,
				KeyStroke.getKeyStroke("control I"));
		exitAction.putValue(
				Action.ACCELERATOR_KEY,
				KeyStroke.getKeyStroke("control Q"));
		closeTabAction.putValue(
				Action.ACCELERATOR_KEY,
				KeyStroke.getKeyStroke("control W"));
		deleteSelectedPartAction.putValue(
				Action.ACCELERATOR_KEY,
				KeyStroke.getKeyStroke("delete"));
		toggleCaseAction.putValue(
				Action.ACCELERATOR_KEY,
				KeyStroke.getKeyStroke("control T"));
		lowerCaseAction.putValue(
				Action.ACCELERATOR_KEY,
				KeyStroke.getKeyStroke("control L"));
		upperCaseAction.putValue(
				Action.ACCELERATOR_KEY,
				KeyStroke.getKeyStroke("control U"));
	}

	/**
	 * Action for creating new empty document.
	 */
	Action newDocumentAction = new LocalizableAction("new", true,
			JNotepadPP.getFlp()) {

		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			createNewTab("", null);
		}
	};

	/**
	 * Action for opening existing document.
	 */
	Action openDocumentAction = new LocalizableAction("open", true,
			JNotepadPP.getFlp()) {
		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {

			JFileChooser fc = new JFileChooser();
			fc.setDialogTitle("Open file");
			if (fc.showOpenDialog(notepadPP) != JFileChooser.APPROVE_OPTION) {
				return;
			}
			Path file = fc.getSelectedFile().toPath();
			if (!Files.isReadable(file)) {
				JOptionPane.showMessageDialog(
						notepadPP,
						"Selected file (" + file + ") is not readable!",
						"Error",
						JOptionPane.ERROR_MESSAGE);
				return;
			}
			if (isAlreadyOpen(file)) {
				return;
			}
			try {
				byte[] okteti = Files.readAllBytes(file);
				createNewTab(new String(okteti, StandardCharsets.UTF_8), file);
			} catch (IOException e1) {
				JOptionPane.showMessageDialog(
						notepadPP,
						"Error while reading file " + file + " :"
								+ e1.getMessage(),
						"Error",
						JOptionPane.ERROR_MESSAGE);
				return;
			}

		}

		/**
		 * This method checks is file selected in {@link JFileChooser} already
		 * opened in {@link JNotepadPP}.
		 * 
		 * @param file
		 *            selected file
		 * @return <code>true</code> if file is already opened, otherwise
		 *         <code>false</code>
		 */
		private boolean isAlreadyOpen(Path file) {
			for (int i = 0; i < notepadPP.getTabbedEditorPane().getTabCount(); i++) {
				JTabbedPane pane = notepadPP.getTabbedEditorPane();
				Component comp = pane.getComponentAt(i);
				if (comp == null) {
					return true;
				}
				if (file.toString().equals(pane.getToolTipTextAt(i))) {
					JOptionPane.showMessageDialog(
							notepadPP,
							"Selected file (" + file + ") is already open!",
							"Message",
							JOptionPane.ERROR_MESSAGE);
					pane.setSelectedIndex(i);
					return true;
				}
			}
			return false;
		}
	};

	/**
	 * This method creates new {@link Tab} in {@link JTabbedPane} of
	 * {@link JNotepadPP} with the given content.
	 * 
	 * @param tabContent
	 *            content of {@link Tab} that is created
	 * @param filePath
	 *            {@link Path} of file that is opened
	 */
	private void createNewTab(String tabContent, Path filePath) {
		JTextArea editor = new JTextArea();
		editor.addCaretListener((l) -> {
			notepadPP.getStatusBar().upadateCaret();
		});
		JTabbedPane pane = notepadPP.getTabbedEditorPane();
		Tab newTab = new Tab(editor);
		editor.setText(tabContent);
		editor.getDocument().addDocumentListener(modificationListener);
		if (filePath == null) {
			pane.addTab(null, newTab);
			pane.setSelectedComponent(newTab);
			pane.setIconAt(pane.getSelectedIndex(), RedFloppyIcon);
			pane.setToolTipTextAt(pane.getSelectedIndex(), null);
		} else {
			pane.addTab(filePath.toFile().getName(), newTab);
			pane.setSelectedComponent(newTab);
			pane.setIconAt(pane.getSelectedIndex(), GreenFloppyIcon);
			pane.setToolTipTextAt(pane.getSelectedIndex(), filePath.toString());
		}
		notepadPP.getStatusBar().updateLength();
	}

	/**
	 * Action for saving opened document. If document does not already exists
	 * then user needs to set name and location where it should be saved.
	 */
	Action saveDocumentAction = new LocalizableAction("save", true,
			JNotepadPP.getFlp()) {
		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			JTabbedPane pane = notepadPP.getTabbedEditorPane();
			Component comp = pane.getSelectedComponent();
			if (comp == null) {
				return;
			}
			JTextArea editor = ((Tab) comp).getEditor();
			Path file;
			if (pane.getTitleAt(pane.getSelectedIndex()).equals("")) {
				JFileChooser fc = new JFileChooser();
				fc.setDialogTitle("Save document");
				if (fc.showSaveDialog(notepadPP) != JFileChooser.APPROVE_OPTION) {
					nothingWasSavedDialog();
					return;
				}
				file = fc.getSelectedFile().toPath();
				if (Files.exists(file)) {
					int rez = openOverwriteDialog(file);
					if (rez != JOptionPane.YES_OPTION) {
						return;
					}
				}
				pane.setTitleAt(pane.getSelectedIndex(), file.toFile()
						.getName());
				pane.setToolTipTextAt(pane.getSelectedIndex(), file.toString());
			} else {
				file = Paths.get(pane.getToolTipTextAt(pane.getSelectedIndex()));
			}
			try {
				Files.write(
						file,
						editor.getText().getBytes(StandardCharsets.UTF_8));
				setGreenFloppyIcon();
			} catch (IOException e1) {
				errorWhileWriteingToFile(e1);
				return;
			}
		}

	};

	/**
	 * Action for saving document under other name. User selects location where
	 * the file should be stored.
	 */
	Action saveAsDocumentAction = new LocalizableAction("saveAs", true,
			JNotepadPP.getFlp()) {
		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			JTabbedPane pane = notepadPP.getTabbedEditorPane();
			Component comp = pane.getSelectedComponent();
			if (comp == null) {
				return;
			}
			JTextArea editor = ((Tab) comp).getEditor();
			JFileChooser fc = new JFileChooser();
			fc.setDialogTitle("Save document as");
			if (fc.showSaveDialog(notepadPP) != JFileChooser.APPROVE_OPTION) {
				nothingWasSavedDialog();
				return;
			}
			Path file = fc.getSelectedFile().toPath();
			if (Files.exists(file)) {
				int rez = openOverwriteDialog(file);
				if (rez != JOptionPane.YES_OPTION) {
					return;
				}
			}
			try {
				Files.write(
						file,
						editor.getText().getBytes(StandardCharsets.UTF_8));
				setGreenFloppyIcon();
			} catch (IOException e1) {
				errorWhileWriteingToFile(e1);
				return;
			}
		}
	};

	/**
	 * This method shows dialog which informs user that nothing was stored.
	 */
	private void nothingWasSavedDialog() {
		JOptionPane.showMessageDialog(
				notepadPP,
				"Nothing was saved.",
				"Message",
				JOptionPane.INFORMATION_MESSAGE);
	}

	/**
	 * This method sets tab floppy icon to green.
	 */
	private void setGreenFloppyIcon() {
		notepadPP.getTabbedEditorPane().setIconAt(
				notepadPP.getTabbedEditorPane().getSelectedIndex(),
				GreenFloppyIcon);
	}

	/**
	 * This method show confirm dialog which ask user to overwrite existing
	 * file.
	 * 
	 * @param file
	 *            file to overwrite
	 * @return users choice, yes or no option from confirm dialog
	 */
	private int openOverwriteDialog(Path file) {
		return JOptionPane.showConfirmDialog(
				notepadPP,
				"Selected file (" + file
						+ ") already exists. Do you want to overwrite it?",
				"Warning",
				JOptionPane.YES_NO_OPTION,
				JOptionPane.WARNING_MESSAGE);

	}

	/**
	 * This method shows message dialog when error while writing to a file
	 * occurred.
	 * 
	 * @param e1
	 *            {@link Exception} for which message is printed
	 */
	private void errorWhileWriteingToFile(IOException e1) {
		JOptionPane.showMessageDialog(
				notepadPP,
				"Error while writing to file "
						+ notepadPP.getTabbedEditorPane().getToolTipTextAt(
								notepadPP.getTabbedEditorPane()
										.getSelectedIndex()) + " :"
						+ e1.getMessage(),
				"Error",
				JOptionPane.ERROR_MESSAGE);
	}

	/**
	 * Action for exiting application (i.e. {@link JNotepadPP})
	 */
	Action exitAction = new LocalizableAction("exit", true, JNotepadPP.getFlp()) {
		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			if (checkTabs()) {
				notepadPP.getStatusBar().getTimer().stop();
				notepadPP.dispose();
			}
		}
	};

	/**
	 * Action for closing current {@link Tab}.
	 */
	Action closeTabAction = new LocalizableAction("close", true,
			JNotepadPP.getFlp()) {
		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			JTabbedPane pane = notepadPP.getTabbedEditorPane();
			int index = pane.getSelectedIndex();
			Component comp = pane.getSelectedComponent();
			if (index == -1) {
				return;
			}
			if (checkTabAt(index)) {
				if (pane.getTabCount() == 1) {
					pane.removeAll();
				} else {
					pane.remove(comp);
				}
			}
		}

	};

	/**
	 * This method checks is document in {@link Tab} at given index saved. If
	 * document is not saved then user decides will it be saved.
	 * 
	 * @param index
	 *            index of {@link Tab}
	 * @return <code>true</code> if next tab could be checked, otherwise
	 *         <code>false</code>
	 */
	private boolean checkTabAt(int index) {
		JTabbedPane pane = notepadPP.getTabbedEditorPane();
		if (((ImageIcon) pane.getIconAt(index)).getDescription().equals("Red")) {
			int rez = JOptionPane.showConfirmDialog(
					notepadPP,
					"Do you want to save file: " + pane.getTitleAt(index),
					"Warning",
					JOptionPane.YES_NO_CANCEL_OPTION,
					JOptionPane.WARNING_MESSAGE);
			if (rez == JOptionPane.YES_OPTION) {
				saveDocumentAction.actionPerformed(null);
				return true;
			} else if (rez == JOptionPane.NO_OPTION) {
				return true;
			} else {
				return false;
			}
		} else {
			return true;
		}
	}

	/**
	 * This method checks are opened documents in {@link JTabbedPane} saved. If
	 * documents are not saved then user decides will they be saved.
	 * 
	 * @return <code>true</code> if all tabs are saved, otherwise
	 *         <code>false</code>
	 */
	private boolean checkTabs() {
		JTabbedPane pane = notepadPP.getTabbedEditorPane();
		int len = pane.getTabCount();
		for (int i = 0; i < len; i++) {
			if (checkTabAt(i)) {
				continue;
			} else {
				return false;
			}
		}
		return true;
	}

	/**
	 * Action for showing in message dialog statistical information about
	 * selected file.
	 */
	Action statisticalInfoAction = new LocalizableAction("stats", true,
			JNotepadPP.getFlp()) {
		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			Component comp = notepadPP.getTabbedEditorPane()
					.getSelectedComponent();
			if (comp == null) {
				return;
			}
			JTextArea editor = ((Tab) comp).getEditor();
			Document doc = editor.getDocument();
			int numberOfCharacters = doc.getLength();
			int numberOfNonBlankCharacters = 0;
			int numberOfLines = 0;
			try {
				numberOfNonBlankCharacters = doc.getText(0, numberOfCharacters)
						.replaceAll("\\s+", "")
						.length();
				numberOfLines = doc.getText(0, numberOfCharacters)
						.replaceAll("[^\n]", "")
						.length() + 1;
			} catch (BadLocationException ignorable) {
			}
			JOptionPane.showMessageDialog(
					notepadPP,
					"Your document has " + numberOfCharacters + " characters, "
							+ numberOfNonBlankCharacters
							+ " non-blank characters and " + numberOfLines
							+ " lines.",
					"Statistical info",
					JOptionPane.INFORMATION_MESSAGE);
		}
	};

	/**
	 * Action for deleting selected text in document.
	 */
	Action deleteSelectedPartAction = new LocalizableAction("delete", true,
			JNotepadPP.getFlp()) {
		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			Component comp = notepadPP.getTabbedEditorPane()
					.getSelectedComponent();
			if (comp == null) {
				return;
			}
			JTextArea editor = ((Tab) comp).getEditor();
			Document doc = editor.getDocument();
			int selectionStart = getSelectionStart(editor);
			int selectionLength = getSelectionEnd(editor) - selectionStart;
			try {
				doc.remove(selectionStart, selectionLength);
			} catch (BadLocationException ignorable) {
			}

		}
	};

	/**
	 * Action for coping selected text from document.
	 */
	Action copySelectedPartAction = new LocalizableAction("copy", true,
			JNotepadPP.getFlp()) {
		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			Component comp = notepadPP.getTabbedEditorPane()
					.getSelectedComponent();
			if (comp == null) {
				return;
			}
			JTextArea editor = ((Tab) comp).getEditor();
			Document doc = editor.getDocument();
			int selectionStart = getSelectionStart(editor);
			int selectionLength = getSelectionEnd(editor) - selectionStart;
			try {
				copiedText = doc.getText(selectionStart, selectionLength);
			} catch (BadLocationException ignorable) {
			}

		}
	};

	/**
	 * Action for cutting selected text from document.
	 */
	Action cutSelectedPartAction = new LocalizableAction("cut", true,
			JNotepadPP.getFlp()) {
		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			Component comp = notepadPP.getTabbedEditorPane()
					.getSelectedComponent();
			if (comp == null) {
				return;
			}
			JTextArea editor = ((Tab) comp).getEditor();
			Document doc = editor.getDocument();
			int selectionStart = getSelectionStart(editor);
			int selectionLength = getSelectionEnd(editor) - selectionStart;
			try {
				copiedText = doc.getText(selectionStart, selectionLength);
				doc.remove(selectionStart, selectionLength);
			} catch (BadLocationException ignorable) {
			}

		}
	};

	/**
	 * Action for pasting copied or cut text to selected position in document.
	 */
	Action pasteSelectedPartAction = new LocalizableAction("paste", true,
			JNotepadPP.getFlp()) {
		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			Component comp = notepadPP.getTabbedEditorPane()
					.getSelectedComponent();
			if (comp == null) {
				return;
			}
			JTextArea editor = ((Tab) comp).getEditor();
			Document doc = editor.getDocument();
			int selectionStart = getSelectionStart(editor);
			int selectionLength = getSelectionEnd(editor) - selectionStart;
			try {
				doc.remove(selectionStart, selectionLength);
				doc.insertString(selectionStart, copiedText, null);
			} catch (BadLocationException ignorable) {
			}

		}
	};

	/**
	 * Action for inverting cases in selected text in document.
	 */
	Action toggleCaseAction = new LocalizableAction("toogle", true,
			JNotepadPP.getFlp()) {
		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			Component comp = notepadPP.getTabbedEditorPane()
					.getSelectedComponent();
			if (comp == null) {
				return;
			}
			JTextArea editor = ((Tab) comp).getEditor();
			Document doc = editor.getDocument();
			int selectionStart = getSelectionStart(editor);
			int selectionLength = getSelectionEnd(editor) - selectionStart;
			try {
				String text = doc.getText(selectionStart, selectionLength);
				text = toglleCase(text, true, true);
				doc.remove(selectionStart, selectionLength);
				doc.insertString(selectionStart, text, null);
			} catch (BadLocationException ignorable) {
			}

		}
	};

	/**
	 * Action to change the letters in the selected text to lowercase..
	 */
	Action lowerCaseAction = new LocalizableAction("toLower", true,
			JNotepadPP.getFlp()) {
		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			Component comp = notepadPP.getTabbedEditorPane()
					.getSelectedComponent();
			if (comp == null) {
				return;
			}
			JTextArea editor = ((Tab) comp).getEditor();
			Document doc = editor.getDocument();
			int selectionStart = getSelectionStart(editor);
			int selectionLength = getSelectionEnd(editor) - selectionStart;
			try {
				String text = doc.getText(selectionStart, selectionLength);
				text = toglleCase(text, true, false);
				doc.remove(selectionStart, selectionLength);
				doc.insertString(selectionStart, text, null);
			} catch (BadLocationException ignorable) {
			}

		}
	};

	/**
	 * Action to change the letters in the selected text to uppercase.
	 */
	Action upperCaseAction = new LocalizableAction("toUpper", true,
			JNotepadPP.getFlp()) {
		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			Component comp = notepadPP.getTabbedEditorPane()
					.getSelectedComponent();
			if (comp == null) {
				return;
			}
			JTextArea editor = ((Tab) comp).getEditor();
			Document doc = editor.getDocument();
			int selectionStart = getSelectionStart(editor);
			int selectionLength = getSelectionEnd(editor) - selectionStart;
			try {
				String text = doc.getText(selectionStart, selectionLength);
				text = toglleCase(text, false, true);
				doc.remove(selectionStart, selectionLength);
				doc.insertString(selectionStart, text, null);
			} catch (BadLocationException ignorable) {
			}
		}
	};

	/**
	 * This method changes letters from selected text.
	 * 
	 * @param text
	 *            {@link String} that needs to be edited
	 * @param toLower
	 *            change uppercase to lowercase
	 * @param toUpper
	 *            change lowercase to uppercase
	 * @return edited {@link String}
	 */
	private String toglleCase(String text, boolean toLower, boolean toUpper) {
		char[] znakovi = text.toCharArray();
		for (int i = 0; i < znakovi.length; i++) {
			Character c = znakovi[i];
			if (Character.isLowerCase(c) && toUpper) {
				znakovi[i] = Character.toUpperCase(c);
			} else if (Character.isUpperCase(c) && toLower) {
				znakovi[i] = Character.toLowerCase(c);
			}
		}
		return new String(znakovi);
	}

	/**
	 * Action for sorting selected text in ascending order.
	 */
	Action ascendingSortAction = new LocalizableAction("ascending", true,
			JNotepadPP.getFlp()) {
		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			sort(true);
		}

	};
	/**
	 * Action for sorting selected text in descending order.
	 */
	Action descendingSortAction = new LocalizableAction("descending", true,
			JNotepadPP.getFlp()) {
		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			sort(false);
		}
	};

	/**
	 * This method determines selected lines and sorts them in ascending or
	 * descending order depending on flag given as argument.
	 * 
	 * @param asc
	 *            if <code>true</code> ascending sort is selected, if
	 *            <code>false</code> descending sort is selected
	 */
	private void sort(boolean asc) {
		Component comp = notepadPP.getTabbedEditorPane().getSelectedComponent();
		if (comp == null) {
			return;
		}
		JTextArea editor = ((Tab) comp).getEditor();
		int selectionStart = getSelectionStart(editor);
		int selectionEnd = getSelectionEnd(editor);
		List<String> lines = getLines(editor);
		try {
			int lineStart = editor.getLineStartOffset(editor.getLineOfOffset(selectionStart));
			int length = editor.getLineEndOffset(editor.getLineOfOffset(selectionEnd))
					- lineStart;
			editor.getDocument().remove(lineStart, length);
			editor.insert(sortLines(asc, lines), lineStart);
		} catch (BadLocationException ignorable) {
		}
	}

	/**
	 * This method sorts selected text in ascending or descending order
	 * depending on flag given as argument.
	 * 
	 * @param asc
	 *            flag that determines order of sort
	 * @param lines
	 *            {@link List} of strings that need to be sorted
	 * @return {@link String} that contains sorted lines
	 */
	private String sortLines(boolean asc, List<String> lines) {
		Collator collator = Collator.getInstance(new Locale(getLanguage()));
		int sign = asc == true ? 1 : -1;
		String[] linesArray = new String[lines.size()];
		lines.toArray(linesArray);
		StringBuilder sortedLines = new StringBuilder("");
		for (int i = 0; i < linesArray.length; i++) {
			int minOrMaxIndex = i;
			for (int j = i + 1; j < linesArray.length; j++) {
				int result = sign
						* collator.compare(
								linesArray[minOrMaxIndex],
								linesArray[j]);
				if (result > 0) {
					minOrMaxIndex = j;
				}
			}
			if (minOrMaxIndex != i) {
				String pom = linesArray[minOrMaxIndex];
				linesArray[minOrMaxIndex] = linesArray[i];
				linesArray[i] = pom;
			}
			sortedLines.append(linesArray[i]);
		}
		return sortedLines.toString();
	}

	/**
	 * This method gets language acronym from selected language
	 * 
	 * @return selected acronym or {@value #DEFAUL_LOCALE_NAME} if no language
	 *         is selecte
	 */
	private String getLanguage() {
		for (Enumeration<AbstractButton> buttons = notepadPP.getNotepadMenuBar()
				.getLanguagesGroup()
				.getElements(); buttons.hasMoreElements();) {
			AbstractButton button = buttons.nextElement();
			if (button.isSelected()) {
				return button.getName();
			}
		}
		return DEFAUL_LOCALE_NAME;
	}

	/**
	 * Action for removing duplicated lines from selected text.
	 */
	Action uniqueAction = new LocalizableAction("unique", true,
			JNotepadPP.getFlp()) {
		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			Component comp = notepadPP.getTabbedEditorPane()
					.getSelectedComponent();
			if (comp == null) {
				return;
			}
			JTextArea editor = ((Tab) comp).getEditor();
			int selectionStart = getSelectionStart(editor);
			int selectionEnd = getSelectionEnd(editor);
			List<String> lines = getLines(editor);
			try {
				int lineStart = editor.getLineStartOffset(editor.getLineOfOffset(selectionStart));
				int length = editor.getLineEndOffset(editor.getLineOfOffset(selectionEnd))
						- lineStart;
				editor.getDocument().remove(lineStart, length);
				addEditedLines(editor, lines, lineStart);
			} catch (BadLocationException ignorable) {
			}
		}

		/**
		 * This method removes duplicated lines in given {@link List}. First
		 * occurrence is note removed.
		 * 
		 * @param editor
		 *            {@link JTextArea} where document is stored
		 * @param lines
		 *            {@link List} where duplicates need to be removed
		 * @param lineStart
		 *            position where edited text needs to be inserted
		 * @throws BadLocationException
		 *             if error in editing {@link Document} has occurred
		 */
		private void addEditedLines(
				JTextArea editor,
				List<String> lines,
				int lineStart) throws BadLocationException {
			Collator collator = Collator.getInstance(new Locale(getLanguage()));
			StringBuilder retainedLines = new StringBuilder("");
			int arrayLength = lines.size();
			for (int i = 0; i < arrayLength; i++) {
				for (int j = i + 1; j < arrayLength; j++) {
					if (collator.compare(lines.get(i), lines.get(j)) == 0) {
						lines.remove(j);
						j--;
						arrayLength = lines.size();
					}
				}
				retainedLines.append(lines.get(i));
			}
			editor.insert(retainedLines.toString(), lineStart);
		}
	};

	/**
	 * This method separate lines in selected text.
	 * 
	 * @param editor
	 *            {@link JTextArea} where document is stored
	 * @return {@link List} of strings that contains separated lines lines
	 */
	private List<String> getLines(JTextArea editor) {
		int selectionStart = getSelectionStart(editor);
		int selectionEnd = getSelectionEnd(editor);

		List<String> lines = new ArrayList<String>();
		for (int i = selectionStart; i <= selectionEnd;) {
			try {
				int line = editor.getLineOfOffset(i);
				int start = editor.getLineStartOffset(line);
				int length = editor.getLineEndOffset(line) - start;
				lines.add(editor.getText(start, length));
				i = start + length + 1;
			} catch (BadLocationException ignorable) {
			}
		}
		return lines;
	}

	/**
	 * This method gets selection start in document.
	 * 
	 * @param editor
	 *            {@link JTextArea} where document is stored
	 * @return offset from beginning of document to selection start
	 */
	private int getSelectionStart(JTextArea editor) {
		return Math.min(editor.getCaret().getDot(), editor.getCaret().getMark());
	}

	/**
	 * This method gets selection end in document.
	 * 
	 * @param editor
	 *            {@link JTextArea} where document is stored
	 * @return offset from beginning of document to selection end
	 */
	private int getSelectionEnd(JTextArea editor) {
		return Math.max(editor.getCaret().getDot(), editor.getCaret().getMark());
	}
}

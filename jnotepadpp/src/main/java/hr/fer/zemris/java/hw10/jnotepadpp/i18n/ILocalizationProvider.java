package hr.fer.zemris.java.hw10.jnotepadpp.i18n;

/**
 * <p>
 * This interface provides localization.
 * </p>
 * Object which are instances of classes that implement this interface will be
 * able to give us the translations for given keys.
 * 
 * Also, each ILocalizationProvider is the Subject (Observer pattern) that will
 * notify all registered listeners when a selected language has changed. For
 * that purpose, the ILocalizationProvider interface also declares a method for
 * registration and a method for de-registration of listeners.
 * 
 * @author Stipan Mikulić
 * @version 1.0
 */
public interface ILocalizationProvider {
	/**
	 * This method adds the {@link ILocalizationListener} given as argument to
	 * registered listeners. (i.e. registers given listener)
	 * 
	 * @param listener
	 *            {@link ILocalizationListener} to register
	 */
	public void addLocalizationListener(ILocalizationListener listener);

	/**
	 * This method removes the {@link ILocalizationListener} given as argument
	 * from registered listeners. (i.e. de-registers given listener)
	 * 
	 * @param listener
	 *            {@link ILocalizationListener} to remove
	 */
	public void removeLocalizationListener(ILocalizationListener listener);

	/**
	 * This method takes a key and gives back the localization for the key
	 * 
	 * @param key
	 *            key that needs to be localized
	 * @return localized value for given key
	 */
	public String getString(String key);
}

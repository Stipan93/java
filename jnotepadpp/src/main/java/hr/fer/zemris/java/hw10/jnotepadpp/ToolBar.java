package hr.fer.zemris.java.hw10.jnotepadpp;

import javax.swing.JButton;
import javax.swing.JToolBar;

/**
 * <p>
 * This class represents tool bar.
 * </p>
 * It contains buttons that have actions which {@link JNotepadPP} applications
 * supports.
 * 
 * @author Stipan Mikulić
 * @version 1.0
 */
public class ToolBar extends JToolBar {
	private static final long serialVersionUID = 1L;
	/**
	 * Reference to {@link JNotepadPP}, application frame
	 */
	private JNotepadPP notepadPP;
	/**
	 * Reference to all actions for this tool bar
	 */
	private Actions actions;

	/**
	 * Creates new tool bar with supported actions.
	 * 
	 * @param jNotepadPP
	 *            reference to {@link JNotepadPP}, application frame
	 */
	public ToolBar(JNotepadPP jNotepadPP) {
		setName("Tools");
		notepadPP = jNotepadPP;
		actions = notepadPP.getActions();
		setFloatable(true);
		add(new JButton(actions.newDocumentAction));
		add(new JButton(actions.openDocumentAction));
		add(new JButton(actions.saveDocumentAction));
		add(new JButton(actions.saveAsDocumentAction));
		add(new JButton(actions.closeTabAction));
		add(new JButton(actions.exitAction));
		addSeparator();
		add(new JButton(actions.toggleCaseAction));
		add(new JButton(actions.deleteSelectedPartAction));
		add(new JButton(actions.statisticalInfoAction));
		add(new JButton(actions.copySelectedPartAction));
		add(new JButton(actions.cutSelectedPartAction));
		add(new JButton(actions.pasteSelectedPartAction));
	}
}

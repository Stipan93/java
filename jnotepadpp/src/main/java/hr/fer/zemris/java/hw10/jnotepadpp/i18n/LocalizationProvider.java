package hr.fer.zemris.java.hw10.jnotepadpp.i18n;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * <p>
 * This class provides localization for notepad application.
 * </p>
 * This class is singleton object which means that only one instance of this
 * class could be created. Instance can be retrieved by method
 * {@link #getInstance()}.
 * 
 * @author Stipan Mikulić
 * @version 1.0
 */
public class LocalizationProvider extends AbstractLocalizationProvider {
	/**
	 * Selected language
	 */
	private String language;
	/**
	 * {@link ResourceBundle} which provides methods for getting localization
	 */
	private ResourceBundle bundle;
	/**
	 * Single instance of {@link LocalizationProvider}
	 */
	private static LocalizationProvider instance = new LocalizationProvider();

	/**
	 * Creates new {@link LocalizationProvider}.
	 */
	private LocalizationProvider() {
		language = "en";
		Locale locale = Locale.forLanguageTag(language);
		File file = new File("resources");
		URL[] urls = new URL[1];
		try {
			urls[0] = file.toURI().toURL();
		} catch (MalformedURLException ignorable) {
		}
		ClassLoader loader = new URLClassLoader(urls);
		bundle = ResourceBundle.getBundle("prijevodi", locale, loader);
	}

	/**
	 * @return unique instance of this class
	 */
	public static LocalizationProvider getInstance() {
		return instance;
	}

	/**
	 * Sets language to value given as argument.
	 * 
	 * @param language
	 *            language to set
	 */
	public void setLanguage(String language) {
		this.language = language;
		Locale locale = Locale.forLanguageTag(language);
		File file = new File("resources");
		URL[] urls = new URL[1];
		try {
			urls[0] = file.toURI().toURL();
		} catch (MalformedURLException ignorable) {
		}
		ClassLoader loader = new URLClassLoader(urls);
		bundle = ResourceBundle.getBundle("prijevodi", locale, loader);
		fire();
	}

	@Override
	public String getString(String key) {
		return bundle.getString(key);
	}

}

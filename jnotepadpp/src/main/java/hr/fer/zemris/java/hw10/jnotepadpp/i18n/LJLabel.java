package hr.fer.zemris.java.hw10.jnotepadpp.i18n;

import javax.swing.JLabel;

/**
 * <p>
 * This class represents localized {@link JLabel}.
 * </p>
 * When language is changed this label is able to update its text. This class
 * gets notifications from its localization provider.
 * 
 * @author Stipan Mikulić
 * @version 1.0
 */
public class LJLabel extends JLabel {
	private static final long serialVersionUID = 1L;
	/**
	 * Key for localization of this label text
	 */
	protected String key;
	/**
	 * {@link ILocalizationProvider} that is decorated by this class
	 */
	private ILocalizationProvider provider;
	/**
	 * Listener that allows this class to be notified for language changes
	 */
	private ILocalizationListener listener;

	/**
	 * Creates new {@link LJLabel} with text that is fetched from selected
	 * language.
	 * 
	 * @param key
	 *            localization key
	 * @param lp
	 *            {@link ILocalizationProvider} for {@link LJLabel}
	 */
	public LJLabel(String key, ILocalizationProvider lp) {
		this.key = key;
		provider = lp;
		updateLabel();
		listener = () -> {
			updateLabel();
		};
		lp.addLocalizationListener(listener);
	}

	/**
	 * This method updates {@link LJLabel} text depending on selected language
	 */
	private void updateLabel() {
		setText(provider.getString(key));
	}

}

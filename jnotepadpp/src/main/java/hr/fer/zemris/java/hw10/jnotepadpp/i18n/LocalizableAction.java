package hr.fer.zemris.java.hw10.jnotepadpp.i18n;

import javax.swing.AbstractAction;
import javax.swing.Action;

/**
 * <p>
 * This class represents localized {@link LocalizableAction}.
 * </p>
 * When language is changed this action is able to update its name. This class
 * gets notifications from its localization provider.
 * 
 * @author Stipan Mikulić
 * @version 1.0
 */
public abstract class LocalizableAction extends AbstractAction {
	private static final long serialVersionUID = 1L;
	/**
	 * Key for localization of this actions name
	 */
	private String key;
	/**
	 * Flag that determines does this action have description
	 */
	private boolean hasDescription;
	/**
	 * {@link ILocalizationProvider} that is decorated by this class
	 */
	private ILocalizationProvider provider;
	/**
	 * Listener that allows this class to be notified for language changes
	 */
	private ILocalizationListener listener;

	/**
	 * Creates new {@link LocalizableAction} with name that is fetched from
	 * selected language.
	 * 
	 * @param key
	 *            localization key for actions name
	 * @param lp
	 *            {@link ILocalizationProvider} for {@link LocalizableAction}
	 */
	public LocalizableAction(String key, ILocalizationProvider lp) {
		this(key, false, lp);
	}

	/**
	 * Creates new {@link LocalizableAction} with name that is fetched from
	 * selected language.
	 * 
	 * @param key
	 *            localization key for actions name
	 * @param hasDescription
	 *            flag that determines does this action have description
	 * @param lp
	 *            {@link ILocalizationProvider} for {@link LocalizableAction}
	 */
	public LocalizableAction(
								String key,
								boolean hasDescription,
								ILocalizationProvider lp) {
		this.key = key;
		this.hasDescription = hasDescription;
		provider = lp;
		initAction();
		listener = () -> {
			initAction();
		};
		provider.addLocalizationListener(listener);
	}

	/**
	 * This method initializes actions name and description
	 */
	private void initAction() {
		putValue(Action.NAME, provider.getString(key));
		if (hasDescription) {
			putValue(Action.SHORT_DESCRIPTION, provider.getString(key + "Desc"));
		}

	}
}

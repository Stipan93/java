package hr.fer.zemris.java.hw10.jnotepadpp;

import hr.fer.zemris.java.hw10.jnotepadpp.i18n.FormLocalizationProvider;
import hr.fer.zemris.java.hw10.jnotepadpp.i18n.LocalizationProvider;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.SwingUtilities;

/**
 * <p>
 * This class represents simple text editor.
 * </p>
 * This {@link JNotepadPP} editor allow user to work with multiple documents at
 * the same time. It provides actions for file management (open, save, save as,
 * new ...) and editing files ( delete, copy, cut, paste, unique, sort, toggle
 * cases ... ). It supports three different languages: Croatian, English and
 * German. Default language is English.
 * 
 * @author Stipan Mikulić
 * @version 1.0
 */
public class JNotepadPP extends JFrame {
	private static final long serialVersionUID = 1L;
	/**
	 * Frame title for this {@link JNotepadPP}
	 */
	public static final String FRAME_NAME = "JNotepad++";
	/**
	 * {@link JTabbedPane} for this {@link JNotepadPP} where files are opened
	 */
	private JTabbedPane contentPane;
	/**
	 * Actions for this {@link JNotepadPP}
	 */
	private Actions actions;
	/**
	 * {@link StatusBar} for this {@link JNotepadPP}
	 */
	private StatusBar statusBar;
	/**
	 * {@link MenuBar} for this {@link JNotepadPP}
	 */
	private MenuBar menuBar;
	/**
	 * Localization provider for this {@link JNotepadPP}
	 */
	private static FormLocalizationProvider flp;

	/**
	 * Creates new {@link JNotepadPP}.
	 */
	public JNotepadPP() {
		flp = new FormLocalizationProvider(LocalizationProvider.getInstance(),
				this);
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		setTitle(FRAME_NAME);
		setLocation(300, 300);
		setMinimumSize(new Dimension(350, 150));
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				super.windowClosing(e);
				actions.exitAction.actionPerformed(null);
			}
		});
		actions = new Actions(this);
		initGUI();
		pack();
	}

	/**
	 * This method initialize {@link JNotepadPP} components.
	 */
	private void initGUI() {
		getContentPane().setLayout(new BorderLayout());
		JPanel mainPanel = new JPanel(new BorderLayout());
		contentPane = new JTabbedPane();
		contentPane.addChangeListener((e) -> {
			statusBar.updateLength();
			statusBar.upadateCaret();
			if (contentPane.getSelectedIndex() == -1) {
				menuBar.getCaseMenu().setEnabled(false);
				menuBar.getSortMenu().setEnabled(false);
				setTitle(JNotepadPP.FRAME_NAME);
			} else {
				setTitle(contentPane.getTitleAt(contentPane.getSelectedIndex())
						+ " - " + JNotepadPP.FRAME_NAME);
			}
		});
		menuBar = new MenuBar(this);
		statusBar = new StatusBar(this);
		setJMenuBar(menuBar);
		mainPanel.add(contentPane, BorderLayout.CENTER);
		mainPanel.add(statusBar, BorderLayout.PAGE_END);
		getContentPane().add(new ToolBar(this), BorderLayout.PAGE_START);
		getContentPane().add(mainPanel, BorderLayout.CENTER);
	}

	/**
	 * @return actions for this {@link JNotepadPP}
	 */
	public Actions getActions() {
		return actions;
	}

	/**
	 * @return {@link StatusBar} for this {@link JNotepadPP}
	 */
	public StatusBar getStatusBar() {
		return statusBar;
	}

	/**
	 * @return {@link JTabbedPane} for this {@link JNotepadPP}
	 */
	public JTabbedPane getTabbedEditorPane() {
		return contentPane;
	}

	/**
	 * @return {@link MenuBar} for this {@link JNotepadPP}
	 */
	public MenuBar getNotepadMenuBar() {
		return menuBar;
	}

	/**
	 * @return {@link FormLocalizationProvider} for this {@link JNotepadPP}
	 */
	public static FormLocalizationProvider getFlp() {
		return flp;
	}

	/**
	 * This method is called when program starts
	 * 
	 * @param args
	 *            command line arguments
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(() -> {
			new JNotepadPP().setVisible(true);
		});
	}
}

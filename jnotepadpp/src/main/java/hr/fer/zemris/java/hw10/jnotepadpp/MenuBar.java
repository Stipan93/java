package hr.fer.zemris.java.hw10.jnotepadpp;

import hr.fer.zemris.java.hw10.jnotepadpp.i18n.LocalizableAction;
import hr.fer.zemris.java.hw10.jnotepadpp.i18n.LocalizationProvider;

import java.awt.event.ActionEvent;

import javax.swing.ButtonGroup;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JRadioButtonMenuItem;

/**
 * <p>
 * This class represents menu bar in {@link JNotepadPP}.
 * </p>
 * {@link MenuBar} contains file, edit, language and tools menu.
 * 
 * @author Stipan Mikulić
 * @version 1.0
 */
public class MenuBar extends JMenuBar {
	private static final long serialVersionUID = 1L;
	/**
	 * Reference to {@link JNotepadPP}, application frame
	 */
	private JNotepadPP notepadPP;
	/**
	 * Reference to {@link JNotepadPP} actions
	 */
	private Actions actions;
	/**
	 * File menu in menu bar
	 */
	private JMenu fileMenu;
	/**
	 * Edit menu in menu bar
	 */
	private JMenu editMenu;
	/**
	 * Tools menu in menu bar
	 */
	private JMenu toolsMenu;
	/**
	 * Toggle case sub-menu in tools menu
	 */
	private JMenu toggleCaseMenu;
	/**
	 * Languages menu in menu bar
	 */
	private JMenu languagesMenu;
	/**
	 * Sort sub-menu in tools menu
	 */
	private JMenu sortMenu;
	/**
	 * Button group in languages menu
	 */
	private ButtonGroup languagesGroup;

	/**
	 * Creates new {@link MenuBar}.
	 * 
	 * @param jNotepadPP
	 *            reference to {@link JNotepadPP}, application frame
	 */
	public MenuBar(JNotepadPP jNotepadPP) {
		notepadPP = jNotepadPP;
		actions = notepadPP.getActions();
		initMenus();

	}

	/**
	 * This method initializes menus in menu bar.
	 */
	private void initMenus() {
		initFileMenu();
		initEditMenu();
		initLanguagesMenu();
		initToolsMenu();
	}

	/**
	 * This method initializes file menu items.
	 */
	private void initFileMenu() {
		fileMenu = new JMenu(createLocalizableAction("file", false));
		add(fileMenu);
		fileMenu.add(new JMenuItem(actions.newDocumentAction));
		fileMenu.add(new JMenuItem(actions.openDocumentAction));
		fileMenu.add(new JMenuItem(actions.saveDocumentAction));
		fileMenu.add(new JMenuItem(actions.saveAsDocumentAction));
		fileMenu.addSeparator();
		fileMenu.add(new JMenuItem(actions.exitAction));
		fileMenu.add(new JMenuItem(actions.closeTabAction));
	}

	/**
	 * This method initializes edit menu items.
	 */
	private void initEditMenu() {
		editMenu = new JMenu(createLocalizableAction("edit", false));
		add(editMenu);
		editMenu.add(new JMenuItem(actions.deleteSelectedPartAction));
		editMenu.add(new JMenuItem(actions.statisticalInfoAction));
		editMenu.addSeparator();
		editMenu.add(new JMenuItem(actions.copySelectedPartAction));
		editMenu.add(new JMenuItem(actions.cutSelectedPartAction));
		editMenu.add(new JMenuItem(actions.pasteSelectedPartAction));
	}

	/**
	 * This method initializes language menu items.
	 */
	private void initLanguagesMenu() {
		languagesMenu = new JMenu(createLocalizableAction("language", false));
		languagesGroup = new ButtonGroup();
		add(languagesMenu);
		setLanguageInMenu("croatian", "hr");
		setLanguageInMenu("english", "en");
		setLanguageInMenu("german", "de");
	}

	/**
	 * This method creates {@link JRadioButtonMenuItem} language in language
	 * menu.
	 * 
	 * @param language
	 *            {@link JRadioButtonMenuItem} name
	 * @param languageAcronym
	 *            language acronym for getting locale properties
	 */
	private void setLanguageInMenu(String language, String languageAcronym) {
		JRadioButtonMenuItem languageBtn = new JRadioButtonMenuItem(
				createLocalizableAction(language, true));
		languageBtn.setName(languageAcronym);
		languageBtn.addActionListener((l) -> {
			setLanguage(languageAcronym);
		});
		if (languageAcronym.equals("en")) {
			languageBtn.setSelected(true);
		}
		languagesGroup.add(languageBtn);
		languagesMenu.add(languageBtn);
	}

	/**
	 * This method sets language to value given as argument.
	 * 
	 * @param language
	 *            language to set
	 */
	private void setLanguage(String language) {
		LocalizationProvider.getInstance().setLanguage(language);
		notepadPP.getStatusBar().updateLength();
		notepadPP.pack();
	}

	/**
	 * This method initializes tools menu items.
	 */
	private void initToolsMenu() {
		toolsMenu = new JMenu(createLocalizableAction("tools", false));
		add(toolsMenu);
		toggleCaseMenu = new JMenu(createLocalizableAction("changeCase", false));
		toggleCaseMenu.setEnabled(false);
		toolsMenu.add(toggleCaseMenu);
		toggleCaseMenu.add(new JMenuItem(actions.toggleCaseAction));
		toggleCaseMenu.add(new JMenuItem(actions.lowerCaseAction));
		toggleCaseMenu.add(new JMenuItem(actions.upperCaseAction));

		sortMenu = new JMenu(createLocalizableAction("sort", false));
		sortMenu.setEnabled(false);
		toolsMenu.add(sortMenu);
		sortMenu.add(new JMenuItem(actions.ascendingSortAction));
		sortMenu.add(new JMenuItem(actions.descendingSortAction));
		toolsMenu.add(new JMenuItem(actions.uniqueAction));
	}

	/**
	 * This method creates new {@link LocalizableAction} for menus.
	 * 
	 * @param key
	 *            localization key
	 * @param descriptionKey
	 *            flag that determines does action has description
	 * @return created {@link LocalizableAction}
	 */
	private LocalizableAction createLocalizableAction(
			String key,
			boolean hasDescription) {
		return new LocalizableAction(key, hasDescription, JNotepadPP.getFlp()) {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {

			}
		};
	}

	/**
	 * @return buttons group from languages menu
	 */
	public ButtonGroup getLanguagesGroup() {
		return languagesGroup;
	}

	/**
	 * @return toggle case sub-menu from tools menu
	 */
	public JMenu getCaseMenu() {
		return toggleCaseMenu;
	}

	/**
	 * @return sort sub-menu from tools menu
	 */
	public JMenu getSortMenu() {
		return sortMenu;
	}

}

package hr.fer.zemris.java.hw10.jnotepadpp.i18n;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * This class implements {@link ILocalizationProvider} interface and provides
 * method for register and de-register localization listeners.
 * </p>
 * When language is changed, method {@link #fire()} is called and it notifies
 * all registered listeners about change.
 * 
 * @author Stipan Mikulić
 * @version 1.0
 */
public abstract class AbstractLocalizationProvider implements
		ILocalizationProvider {
	/**
	 * Storage for registered listeners
	 */
	private List<ILocalizationListener> listeners = new ArrayList<ILocalizationListener>();

	@Override
	public void addLocalizationListener(ILocalizationListener listener) {
		listeners.add(listener);
	}

	@Override
	public void removeLocalizationListener(ILocalizationListener listener) {
		listeners.remove(listener);
	}

	/**
	 * This method notifies all registered listeners that change is made.
	 */
	public void fire() {
		for (ILocalizationListener listener : listeners) {
			listener.localizationChanged();
		}
	}
}
